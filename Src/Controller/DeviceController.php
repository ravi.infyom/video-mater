<?php

namespace Src\Controller;

use Src\Models\Device;

class DeviceController
{

    private $db;
    private $requestMethod;

    private $device;

    public function __construct($db, $requestMethod)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;

        $this->device = new Device($this->db);
    }

    public function addDevice($id)
    {
        $result = $this->device->store($id);
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        return $response;
    }
    
}