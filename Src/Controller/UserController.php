<?php

namespace Src\Controller;

use Src\Models\Ad;
use Src\Models\User;

class UserController
{

    private $db;
    private $requestMethod;

    private $user;

    public function __construct($db, $requestMethod)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;

        $this->user = new User($this->db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':

                $response = $this->getSettings();
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        return $response;
    }

    private function getSettings()
    {
        $settings = [
            "success" => true,
            "message" => "ad perfectly work",
            "update" => [
                "status" => null,
                "version_code" => 0,
                "whats_new" => "",
            ],
            "ad_network" => [
                "fb_ad"=> null,
                "fb_intreastial"=> null,
                "fb_banner"=> null,
                "fb_native_banner"=> null,
                "admob_ad"=> null,
                "admob_intrestial"=> null,
                "admob_banner"=> null,
                "admob_native"=> null,
                "admob_rv"=> null,
                "page_click"=> null,
                "banner_view"=> null,
                "native_view"=> null
            ],
            "country_market" => [
                "status" => null,
                "maker_name" => "",
                "msg" => "",
                "flag_icon" => "",
            ],
            "custom_ad_status" => null,
            "custom_ad_click" => null,
            "custom_ad" => [],
        ];
        $result = $this->user->getSettings();
        if (array_key_exists('update', $result)) {
            $status=$result['update']['updateStatus'];
            $settings['update'] = [
                "status" => filter_var($status, FILTER_VALIDATE_BOOLEAN),
                "version_code" => $result['update']['versionCode'],
                "whats_new" => $result['update']['whatsNew'],
            ];
        }
        if (array_key_exists('network', $result)) {
            
            // $data['network']['fbAd'] = $fbAd;
            // $data['network']['fbIntreastial'] = $fbIntreastial;
            // $data['network']['fbBanner'] = $fbBanner;
            // $data['network']['fbNative'] = $fbNative;
            // $data['network']['fbNativeBanner'] = $fbNativeBanner;
            // $data['network']['adMobAd'] = $adMobAd;
            // $data['network']['adMobIntreastial'] = $adMobIntreastial;
            // $data['network']['adMobBanner'] = $adMobBanner;
            // $data['network']['adMobNative'] = $adMobNative;
            // $data['network']['adMobRv'] = $adMobRv;
            // $data['network']['pageClicks'] = $pageClicks;
            // $data['network']['bannerView'] = $bannerView;
            // $data['network']['nativeView'] = $nativeView;
            $settings['ad_network'] = [
                "fb_ad"=>filter_var($result['network']['fbAd'], FILTER_VALIDATE_BOOLEAN),
                "fb_intreastial"=>filter_var($result['network']['fbIntreastial'], FILTER_VALIDATE_BOOLEAN),
                "fb_banner"=>filter_var($result['network']['fbBanner'], FILTER_VALIDATE_BOOLEAN),
                "fb_native"=>filter_var($result['network']['fbNative'], FILTER_VALIDATE_BOOLEAN),
                "fb_native_banner"=>filter_var($result['network']['fbNativeBanner'], FILTER_VALIDATE_BOOLEAN),
                "admob_ad"=>filter_var($result['network']['adMobAd'], FILTER_VALIDATE_BOOLEAN),
                "admob_intrestial"=>filter_var($result['network']['adMobIntreastial'], FILTER_VALIDATE_BOOLEAN),
                "admob_banner"=>filter_var($result['network']['adMobBanner'], FILTER_VALIDATE_BOOLEAN),
                "admob_native"=>filter_var($result['network']['adMobNative'], FILTER_VALIDATE_BOOLEAN),
                "admob_rv"=>filter_var($result['network']['adMobRv'], FILTER_VALIDATE_BOOLEAN),
                "page_click" => (int)$result['network']['pageClicks'],
                "banner_view" => (int)$result['network']['bannerView'],
                "native_view" => (int)$result['network']['nativeView'],
            ];
        }
        if (array_key_exists('country', $result)) {
            $settings['country_market'] = [
                "status" => filter_var($result['country']['countryStatus'], FILTER_VALIDATE_BOOLEAN),
                "maker_name" => $result['country']['makerName'],
                "msg" => $result['country']['countryMessage'],
                "flag_icon" =>  $_ENV['APP_URL'] . "/" .$result['country']['icon'],
            ];
        }
        if (array_key_exists('ad', $result)) {
            $settings['custom_ad_click'] = (int)$result['ad']['numberOfClick'];
            $settings['custom_ad_status'] =filter_var( $result['ad']['showAd'], FILTER_VALIDATE_BOOLEAN);
            $ads = explode(",", $result['ad']['ads']);
            if (count($ads) > 0) {
                $adObject = new Ad($this->db);
                $adResponse = $adObject->getAdsById($ads);
                if (count($adResponse) > 0) {
                    $data = [];
                    foreach ($adResponse as $row) {
                        $temp = [
                            "ad_name" => $row['ad_name'],
                            "ad_slogan" => $row['ad_slogan'],
                            "company_name" => $row['company_name'],
                            "ad_type" => $row['ad_type'],
                            "package_name" => $row['package_name'],
                            "redirect_url" => $row['redirect_url'],
                        ];
                        $icon = $row['ad_icon'];
                        $pos = stripos($icon, "/storage/ad/icon");
                        if ($pos == 3) {
                            $icon = $_ENV['APP_URL'] . "/" . $icon;
                        }
                        $temp['ad_icon'] = $icon;
                        $data[] = $temp;

                    }
                    $settings['custom_ad'] = $data;
                }
            }

        }

        // print_r($settings);die;
        $response['data'] = $settings;
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        return $response;
    }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}
