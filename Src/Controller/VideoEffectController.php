<?php

namespace Src\Controller;

use Src\Models\VideoEffect;

class VideoEffectController
{

    private $db;
    private $requestMethod;
    private $payload;

    private $videoEffect;

    public function __construct($db, $requestMethod, $payload)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->payload = $payload;

        $this->videoEffect = new VideoEffect($this->db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                // if ($this->userId) {
                // $response = $this->getEffect($this->userId);
                // } else {
                $response = $this->getAllEffects($this->payload);
                // };
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        return $response;
    }

    private function getAllEffects($payload)
    {
        $page = $payload['page'] ?? 1;
        $limit = $payload['per_page'] ?? 15;
        $offSet = ($page - 1) * $limit;
        $orderBy = 'id';

        $result = $this->videoEffect->getVideoEffects($offSet, $limit, $orderBy);
        $response['effects'] =  $result;
        $response['effect_count'] =  $this->videoEffect->getVideoEffectCount();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        return $response;
    }

    // private function getEffect($id)
    // {
    //     $result = $this->videoEffect->find($id);
    //     if (!$result) {
    //         return $this->notFoundResponse();
    //     }
    //     $response['status_code_header'] = 'HTTP/1.1 200 OK';
    //     $response['body'] = json_encode($result);
    //     return $response;
    // }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}