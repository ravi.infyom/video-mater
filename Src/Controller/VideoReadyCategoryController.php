<?php

namespace Src\Controller;

use Src\Models\VideoReadyCategory;

class VideoReadyCategoryController
{

    private $db;
    private $requestMethod;
    private $payload;

    private $category;

    public function __construct($db, $requestMethod, $payload)
    {
        $this->db = $db;
        $this->requestMethod = $requestMethod;
        $this->payload = $payload;

        $this->category = new VideoReadyCategory($this->db);
    }

    public function processRequest()
    {
        switch ($this->requestMethod) {
            case 'GET':
                // if ($this->userId) {
                // $response = $this->getEffect($this->userId);
                // } else {
                $response = $this->getCategoryNames();
                // };
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        return $response;
    }

    private function getCategoryNames()
    {
        $categories = [];
        $result = $this->category->getCategoryNames();
        if (count($result) > 0) {
            foreach ($result as $row) {
                $categories[] = ['category_name' => $row['name']];
            }
        }
        $response['video_category'] =  $categories;
        $response['total_category'] =  $this->category->getcategoryCount();
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        return $response;
    }

    // private function getEffect($id)
    // {
    //     $result = $this->category->find($id);
    //     if (!$result) {
    //         return $this->notFoundResponse();
    //     }
    //     $response['status_code_header'] = 'HTTP/1.1 200 OK';
    //     $response['body'] = json_encode($result);
    //     return $response;
    // }

    private function notFoundResponse()
    {
        $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
        $response['body'] = null;
        return $response;
    }
}