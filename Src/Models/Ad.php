<?php

namespace Src\Models;

class Ad
{
    private $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getAdById($id)
    {
        $results = [];
        $stmt = $this->connection->query("SELECT * FROM `ad` where `id`='$id'")->fetch(\PDO::FETCH_ASSOC);
        return $stmt;
    }

    public function getAdCount()
    {
        $results = [];
        $stmt = $this->connection->query("SELECT count(*) as `count` FROM `ad`")->fetch(\PDO::FETCH_ASSOC);
        return $stmt['count'];
    }

    public function getAds()
    {
        $results = $this->connection->query("SELECT * FROM `ad`")->fetchAll(\PDO::FETCH_ASSOC);
        return $results;
    }

    public function getAdsById($ids=[])
    {
        $results=[];
        if(count($ids)>0)
        {
            $ids=implode(",",$ids);
            $results = $this->connection->query("SELECT * FROM `ad` where `id` in ($ids)")->fetchAll(\PDO::FETCH_ASSOC);

        }
        return $results;
    }

    public function getAdNames()
    {
        $data = [];
        $results = $this->connection->query("SELECT * FROM `ad`")->fetchAll(\PDO::FETCH_ASSOC);
        foreach ($results as $row) {
            $data[] = ['id' => $row['id'], 'name' => $row['ad_name']];
        }
        return $data;
    }

    public function store($payload)
    {
        $payload = [
            'ad_name' => $payload['adName'],
            ':ad_slogan' => $payload['adSlogan'],
            ':company_name' => $payload['companyName'],
            ':ad_type' => $payload['adType'],
            ':package_name' => $payload['packageName'],
            ':ad_icon' => $payload['adIcon'],
            ':redirect_url' => $payload['redirectUrl'],
        ];

        $stmt = $this->connection->prepare("INSERT INTO `ad` (ad_name,ad_slogan,company_name,ad_type,package_name,ad_icon,redirect_url) VALUES (  :ad_name,:ad_slogan,:company_name,:ad_type,:package_name,:ad_icon,:redirect_url)")->execute($payload);
        $id = $this->connection->lastInsertId();
        return $id;
    }

    public function update($payload)
    {
        $adId = $payload['id'];
        $adName = $payload['adName'];
        $adSlogan = $payload['adSlogan'];
        $companyName = $payload['companyName'];
        $adType = $payload['adType'];
        $packageName = $payload['packageName'];
        $adIcon = $payload['adIcon'];
        $redirectUrl = $payload['redirectUrl'];

        $sql = "UPDATE `ad` SET `ad_name`= '$adName' ,`ad_slogan`='$adSlogan',`company_name`='$companyName',`ad_type`='$adType',`package_name`='$packageName',`ad_icon`='$adIcon',`redirect_url`='$redirectUrl'  WHERE `id` = '$adId'";
        $affectedrows = $this->connection->exec($sql);
        return $affectedrows;
    }

    public function delete($id)
    {
        $stmt = $this->connection->prepare("DELETE FROM `ad` WHERE `id` = :id")->execute(array(':id' => $id));
        return $stmt;
    }

}
