<?php

namespace Src\models;


class Audio
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getAudioByCutomFilter($filters)
    {
        $results = [];
        $query = "";
        foreach ($filters as $filter) {
            list($name, $operator, $value) = $filter;
            $query .= " and `" . $name . "` " . $operator . " '" . $value . "'";
        }
        // echo $query;
        $sql = "SELECT * FROM `audio` where 1 " . $query;
        foreach ($this->connection->query($sql) as $row) {
            $data['id'] = $row['id'];
            $data['audio_cateory_id'] = $row['audio_cateory_id'];
            $data['name'] = $row['name'];
            $data['audio'] = $row['audio'];
            array_push($results, $data);
        }
        return $results;
    }

    public function getAudioById($id)
    {
        $results = [];
        $stmt =  $this->connection->query("SELECT * FROM `audio` where `id`='$id'")->fetch();
        return $stmt;
    }

    public function getAudioCount()
    {
        $results = [];
        $stmt =  $this->connection->query("SELECT count(*) as `count` FROM `audio`")->fetch();
        return $stmt['count'];
    }
    public function getAudiosWithPage($offSet = 0, $limit = 15, $orderBy = 'id')
    {

        $results = [];
        $sql = "SELECT `audio`.*,`audio_category`.`name` as `category_name`,`audio_category`.`priority` ,`audio_category`.`status`  FROM `audio` join `audio_category`on `audio_category`.`id` = `audio`.`audio_category_id`  WHERE `audio_category`.`status` = 'active' ORDER BY  CASE WHEN `audio_category`.`priority` is null then 1 else 0 end, `audio_category`.`priority`";
        //order by `audio`. `$orderBy` LIMIT $limit OFFSET $offSet";

        foreach ($this->connection->query($sql) as $row) {
            $record = [];
            $record['audio_category'] = $row['category_name'];
            $record['audio_name'] = $row['name'];
            $record['audio_url'] = $_ENV['APP_URL'] . '/' . $row['audio'];
            $record['created_at'] = $row['created_at'];
            $record['updated_at'] = $row['updated_at'];


            array_push($results, $record);
        }
        return $results;
    }

    public function getAudios()
    {
        $results = [];
        $sql = "SELECT * FROM `audio` ORDER BY RAND()";
        foreach ($this->connection->query($sql) as $row) {
            $data['id'] = $row['id'];
            $data['audio_cateory_id'] = $row['audio_cateory_id'];
            $data['name'] = $row['name'];
            $data['audio'] = $row['audio'];
            array_push($results, $data);
        }
        return $results;
    }

    public function store($payload)
    {
        $name = $payload['name'];
        $categoryId = $payload['categoryId'];
        $audio = $payload['audio'];
        $stmt =  $this->connection->
            prepare("INSERT INTO `audio` (name,audio_category_id,audio) VALUES (  :name,:audio_category_id,:audio)")
            ->execute(array(':name' => $name, ':audio_category_id' => $categoryId, ':audio' => $audio));
        return $stmt;
    }

    public function update($payload)
    {
        $audioId = $payload['id'];
        $categoryId = $payload['categoryId'];
        $name = $payload['name'];
        $audio = $payload['audio'];
        $updatedAt = date('Y-m-d H:i:s', time());
        $sql = "UPDATE `audio` SET `name`= '$name' ,`audio`='$audio',`audio_category_id`='$categoryId',`updated_at`='$updatedAt'  WHERE `id` = '$audioId'";
        $affectedrows  = $this->connection->exec($sql);
        return $affectedrows;
    }

    public function delete($audioId)
    {
        $stmt =  $this->connection->prepare("DELETE FROM `audio` WHERE `id` = :id")->execute(array(':id' => $audioId));
        return $stmt;
    }
}