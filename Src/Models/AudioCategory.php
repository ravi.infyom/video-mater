<?php

namespace Src\Models;

class AudioCategory
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getAudioCategoryByName($name)
    {
        $results = [];
        $sql = "SELECT * FROM `audio_category` where `name`='$name'";
        foreach ($this->connection->query($sql) as $row) {
            $data['id'] = $row['id'];
            $data['name'] = $row['name'];
            $data['icon'] = $row['icon'];
            $data['status'] = $row['status'];


            array_push($results, $data);
        }
        return $results;
    }

    public function getAudioCategoryByCutomFilter($filters)
    {
        $results = [];
        $query = "";
        foreach ($filters as $filter) {
            list($name, $operator, $value) = $filter;
            $query .= " and `" . $name . "` " . $operator . " '" . $value . "'";
        }
        // echo $query;
        $sql = "SELECT * FROM `audio_category` where 1" . $query;
        foreach ($this->connection->query($sql) as $row) {
            $data['id'] = $row['id'];
            $data['name'] = $row['name'];
            $data['icon'] = $row['icon'];
            $data['status'] = $row['status'];


            array_push($results, $data);
        }
        return $results;
    }

    public function getAudioCategoryById($id)
    {
        $results = [];
        $stmt =  $this->connection->query("SELECT * FROM `audio_category` where `id`='$id'")->fetch();
        return $stmt;
    }

    public function getAudioCategoryCount()
    {
        $results = [];
        $stmt =  $this->connection->query("SELECT count(*) as `count` FROM `audio_category`")->fetch();
        return $stmt['count'];
    }


    public function getAudioCategories()
    {
        $results = [];
        $sql = "SELECT * FROM `audio_category`";
        foreach ($this->connection->query($sql) as $row) {
            $data['id'] = $row['id'];
            $data['name'] = $row['name'];
            $data['icon'] = $row['icon'];
            $data['status'] = $row['status'];

            array_push($results, $data);
        }
        return $results;
    }

    public function getAudioCategoryNames($filters = [])
    {
        $results = [];
        $query = "";
        foreach ($filters as $filter) {
            list($name, $operator, $value) = $filter;
            $query .= " and `" . $name . "` " . $operator . " '" . $value . "'";
        }
        $sql = "SELECT `id`,`name`,`priority` FROM `audio_category` where `status` = 'active'" . $query ." ORDER BY CASE WHEN `priority` is null then 1 else 0 end, `priority`";
        foreach ($this->connection->query($sql) as $row) {
            $data['id'] = $row['id'];
            $data['name'] = $row['name'];

            array_push($results, $data);
        }
        return $results;
    }

    public function store($payload)
    {
        $name = $payload['name'];
        $icon = $payload['icon'];
        $status = $payload['status'];
        $stmt =  $this->connection->prepare("INSERT INTO `audio_category` (name,icon,status) VALUES (  :name,:icon,:status)")->execute(array(':name' => $name, ':icon' => $icon, ':status' => $status));
        return $stmt;
    }

    public function update($payload)
    {
        $categoryId = $payload['categoryId'];
        $name = $payload['name'];
        $icon = $payload['icon'];
        $status = $payload['status'];
        $updatedAt = date('Y-m-d H:i:s', time());

        $priority = $payload['priority'];
        if(!($priority == null || $priority == '0'))
        {
            $this->setCategoryPriority($categoryId,$priority);

        }
        
        $sql = "UPDATE `audio_category` SET `name`= '$name' ,`icon`='$icon',`updated_at`='$updatedAt',`status`='$status'  WHERE `id` = '$categoryId'";
        $affectedrows  = $this->connection->exec($sql);
        return $affectedrows;
    }

    public function getCategoryByPriority($priority)
    {
        $results = [];
        $stmt =  $this->connection->query("SELECT * FROM `audio_category` where `priority`='$priority'")->fetch(\PDO::FETCH_ASSOC);
        return $stmt;
    }

    public function setCategoryPriority($catId,$priority)
    {
        $sql = "UPDATE `audio_category` SET `priority`= null  WHERE `priority` = '$priority'";
        $this->connection->exec($sql);
      
        $sql2 = "UPDATE `audio_category` SET `priority`= '$priority'  WHERE `id` = '$catId'";
        $this->connection->exec($sql2);
       
    }

    public function delete($categoryId)
    {
        $stmt =  $this->connection->prepare("DELETE FROM `audio_category` WHERE `id` = :id")->execute(array(':id' => $categoryId));
        return $stmt;
    }

    public function isLinkedToOther($categoryId)
    {
        $res =  $this->connection->query("SELECT count(*) as `count` FROM `audio` where audio_category_id=$categoryId")->fetch();
        return $res['count'] > 0 ? false : true;
    }
}