<?php

namespace Src\Models;


class Category
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getCategoryByName($name)
    {
        $results = [];
        $sql = "SELECT * FROM `category` where `name`='$name'";
        foreach ($this->connection->query($sql) as $row) {
            $data['id'] = $row['id'];
            $data['name'] = $row['name'];
            $data['icon'] = $row['icon'];
            $data['status'] = $row['status'];


            array_push($results, $data);
        }
        return $results;
    }

    public function getCategoryByCutomFilter($filters)
    {
        $results = [];
        $query = "";
        foreach ($filters as $filter) {
            list($name, $operator, $value) = $filter;
            $query .= " and `" . $name . "` " . $operator . " '" . $value . "'";
        }
        // echo $query;
        $sql = "SELECT * FROM `category` where status = 'active' " . $query;
        foreach ($this->connection->query($sql) as $row) {
            $data['id'] = $row['id'];
            $data['name'] = $row['name'];
            $data['icon'] = $row['icon'];
            $data['status'] = $row['status'];


            array_push($results, $data);
        }
        return $results;
    }

    public function getCategoryById($id)
    {
        $results = [];
        $stmt =  $this->connection->query("SELECT * FROM `category` where `id`='$id'")->fetch();
        return $stmt;
    }

    public function getCategoryCount()
    {
        $results = [];
        $stmt =  $this->connection->query("SELECT count(*) as `count` FROM `category`")->fetch();
        return $stmt['count'];
    }

    public function getCategories()
    {
        $results = [];
        $sql = "SELECT * FROM `category`";
        foreach ($this->connection->query($sql) as $row) {
            $data['id'] = $row['id'];
            $data['name'] = $row['name'];
            $data['icon'] = $row['icon'];
            $data['status'] = $row['status'];

            array_push($results, $data);
        }
        return $results;
    }

    public function getCategoryNames($filters = [])
    {
        $results = [];
        $query = "";
        foreach ($filters as $filter) {
            list($name, $operator, $value) = $filter;
            $query .= " and `" . $name . "` " . $operator . " '" . $value . "'";
        }
        $sql = "SELECT `id`,`name`,`priority` FROM `category` where `status` = 'active'" . $query . " ORDER BY CASE WHEN `priority` is null then 1 else 0 end, `priority`";
        foreach ($this->connection->query($sql) as $row) {
            $data['id'] = $row['id'];
            $data['name'] = $row['name'];

            array_push($results, $data);
        }
        return $results;
    }

    public function store($payload)
    {
        $name = $payload['name'];
        $icon = $payload['icon'];
        $status = $payload['status'];
        $stmt =  $this->connection->prepare("INSERT INTO category (name,icon,status) VALUES (  :name,:icon,:status)")->execute(array(':name' => $name, ':icon' => $icon, ':status' => $status));
        return $stmt;
    }

    public function update($payload)
    {
        $categoryId = $payload['categoryId'];
        $name = $payload['name'];
        $icon = $payload['icon'];
        $status = $payload['status'];
        
        $priority = $payload['priority'];
        if(!($priority == null || $priority == '0'))
        {
            $this->setCategoryPriority($categoryId,$priority);

        }
        
        $updatedAt = date('Y-m-d H:i:s', time());
        $sql = "UPDATE `category` SET `name`= '$name' ,`icon`='$icon',`updated_at`='$updatedAt',`status`='$status'  WHERE `id` = '$categoryId'";
        $affectedrows  = $this->connection->exec($sql);
        return $affectedrows;
    }

    public function delete($categoryId)
    {
        $stmt =  $this->connection->prepare("DELETE FROM category WHERE `id` = :id")->execute(array(':id' => $categoryId));
        return $stmt;
    }

    public function getCategoryByPriority($priority)
    {
        $results = [];
        $stmt =  $this->connection->query("SELECT * FROM `category` where `priority`='$priority'")->fetch(\PDO::FETCH_ASSOC);
        return $stmt;
    }

    public function setCategoryPriority($catId,$priority)
    {
        $sql = "UPDATE `category` SET `priority`= null  WHERE `priority` = '$priority'";
        $this->connection->exec($sql);
        
        $sql2 = "UPDATE `category` SET `priority`= '$priority'  WHERE `id` = '$catId'";
        $this->connection->exec($sql2);
       
    }

    public function isLinkedToOther($categoryId)
    {
        $vc =  $this->connection->query("SELECT count(*) as `count` FROM `video_effects` where category_id=$categoryId")->fetch();
        if ($vc['count'] > 0) {
            return false;
        } else {
            return true;
        }
    }
}