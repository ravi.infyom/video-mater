<?php

namespace Src\Models;

class Common
{

    public function getFilePathTableMapping($table=null)
    {
        $tables = [

            'ad' => [
                'ad_icon' => 'Src/storage/ad/icon',
            ],
            'audio' => [
                'audio' => 'Src/storage/audio/audio',
            ],
            'audio_category' => [
                'icon' => 'Src/storage/audio_category/icon',
            ],
            'category' => [
                'icon' => 'Src/storage/category/icon',
            ],
            'notification' => [
                'banner_image' => 'Src/storage/notification/banner_image',
            ],
            'users' => [
                'avatar' => 'Src/storage/user/avatar',
                // 'data'=>[
                //     'country'=>[
                //         'icon'=>'Src/storage/users/country'
                //     ]
                // ]
            ],

            'video_effects' => [
                'cover_image' => 'Src/storage/video_effect/cover_image',
                'video' => 'Src/storage/video_effect/video',
                'zip' => 'Src/storage/video_effect/zip',
            ],
            'video_ready' => [
                'cover_image' => 'Src/storage/video_ready/cover_image',
                'video' => 'Src/storage/video_ready/video',
            ],
            'video_ready_category' => [
                'icon' => 'Src/storage/video_ready_category/icon',
            ]

        ];

        if($table)
        {
            if(array_key_exists($table,$tables))
            {
                return $tables[$table];
            }
        }
        else
        {
            return $tables;
        }

        return null;
    }

}
