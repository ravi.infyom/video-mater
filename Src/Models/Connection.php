<?php

namespace Src\Models;

use PDO;
use PDOException;

class Connection
{
    private  $server = "mysql:host=localhost;dbname=status_app";
    private  $user = "root";
    private  $pass = "";
    private $options  = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,);
    private $con;

    public function __construct()
    {
        try {
            $this->con = new PDO($this->server, $this->user, $this->pass, $this->options);
            // return $this->con;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    public function openConnection()
    {
        try {
            $this->con = new PDO($this->server, $this->user, $this->pass, $this->options);
            // return $this->con;
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }
    public function getConnection()
    {
        return $this->con;
    }
    public function closeConnection()
    {
        $this->con = null;
    }
}