<?php

namespace Src\Models;

use Exception;

class DeleteFile
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function canDelete($file, $subject, $subjectName)
    {
        $stmt =  $this->connection->query("SELECT count(*) as `count` FROM `$subject` where `$subjectName`='$file'")->fetch();
        return $stmt['count'] > 0 ? false : true;
    }

    public function getAllFiles()
    {
        try {

            $results = [];
            $stmt =  $this->connection->query("SELECT * FROM `delete_files`")->fetchAll(\PDO::FETCH_ASSOC);
            return $stmt;
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    /**
     * @param String $file file path
     * @param String $table table name
     * @param String $subjectName table field name
     */
    public function addFile($file, $table, $subjectName)
    {
        $stmt =  $this->connection->prepare("INSERT INTO delete_files (file_path,subject,subject_name) VALUES (  :file_path,:subject,:subject_name)")->execute(array(':file_path' => $file, ':subject' => $table, ':subject_name' => $subjectName));

        return $stmt;
    }

    public function deleteFiles($files)
    {
        $files = implode(",", $files);
        $stmt =  $this->connection->prepare("DELETE FROM delete_files WHERE `id` in  (:id)")->execute(array(':id' => $files));
        return $stmt;
    }
}