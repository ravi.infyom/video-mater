<?php

namespace Src\Models;


class Device
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getDeviceByDeviceId($id)
    {
        $stmt =  $this->connection->query("SELECT * FROM `devices` where `device_id`='$id'")->fetch(\PDO::FETCH_ASSOC);
        return $stmt ?? [];
    }


    public function store($deviceId)
    {
        if(!$this->getDeviceByDeviceId($deviceId))
        {
            $stmt =  $this->connection->prepare("INSERT INTO `devices` (device_id) VALUES (  :device_id)")->execute(array(':device_id' => $deviceId));
        }
        return true;
       
    }

    public function getAllDeviceId()
    {
        $result=[];
        $stmt =  $this->connection->query("SELECT `device_id` FROM `devices`")->fetchAll(\PDO::FETCH_ASSOC);

        foreach($stmt as $row)
        {
            array_push($result,$row['device_id']);
        }
        return $result;
    }

}