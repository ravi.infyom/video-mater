<?php

namespace Src\Models;


class Language
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getLanguageByName($name)
    {
        $results = [];
        $sql = "SELECT * FROM `languages` where `language`='$name'";
        foreach ($this->connection->query($sql) as $row) {
            $data['id'] = $row['id'];
            $data['language'] = $row['language'];


            array_push($results, $data);
        }
        return $results;
    }

    public function getLanguageById($id)
    {
        $results = [];
        $stmt =  $this->connection->query("SELECT * FROM `languages` where `id`='$id'")->fetch();
        return $stmt;
    }

    public function getLanguageCount()
    {
        $stmt =  $this->connection->query("SELECT count(*) as `count` FROM `languages`")->fetch();
        return $stmt['count'];
    }

    public function getLanguages()
    {
        $results = [];
        $sql = "SELECT * FROM `languages`";
        foreach ($this->connection->query($sql) as $row) {
            $data['id'] = $row['id'];
            $data['language'] = $row['language'];


            array_push($results, $data);
        }
        return $results;
    }

    public function store($language)
    {
        $stmt =  $this->connection->prepare("INSERT INTO languages (language) VALUES (  :language)")->execute(array(':language' => $language));
        return $stmt;
    }

    public function update($payload)
    {
        $languageId = $payload['languageId'];
        $language = $payload['language'];
        $sql = "UPDATE `languages` SET `language`= '$language'  WHERE `id` = '$languageId'";
        $affectedrows  = $this->connection->exec($sql);
        return $affectedrows;
    }

    public function delete($languageId)
    {
        $stmt =  $this->connection->prepare("DELETE FROM languages WHERE `id` = :id")->execute(array(':id' => $languageId));
        return $stmt;
    }
}