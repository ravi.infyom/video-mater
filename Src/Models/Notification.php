<?php

namespace Src\Models;

use Src\Service\Firebase;

class Notification
{
    private $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getNotificationById($id)
    {
        $results = [];
        $stmt = $this->connection->query("SELECT * FROM `notification` where `id`='$id'")->fetch(\PDO::FETCH_ASSOC);
        return $stmt;
    }

    public function getNotificationCount()
    {
        $results = [];
        $stmt = $this->connection->query("SELECT count(*) as `count` FROM `notification`")->fetch(\PDO::FETCH_ASSOC);
        return $stmt['count'];
    }

    public function getNotifications()
    {
        $results = $this->connection->query("SELECT * FROM `notification`")->fetchAll(\PDO::FETCH_ASSOC);
        return $results;
    }

    public function store($payload)
    {
        $payload = [
            ':title' => $payload['title'],
            ':message' => $payload['message'],
            ':banner_image' => $payload['banner_image'],
            ':banner_size' => $payload['banner_size'],
            ':target_url' => $payload['target_url'],
            ':open_app' => filter_var($payload['open_app'], FILTER_VALIDATE_BOOLEAN) == true ? 1 : 0,
            ':status' => $payload['status'],
        ];

        $stmt = $this->connection->prepare("INSERT INTO `notification` (title,message,banner_image,banner_size,target_url,open_app,status) VALUES (  :title,:message,:banner_image,:banner_size,:target_url,:open_app,:status)")->execute($payload);
        $id = $this->connection->lastInsertId();
        return $id;
    }

    public function updateStatus($id, $status)
    {

        $sql = "UPDATE `notification` SET `status`= '$status'  WHERE `id` = '$id'";
        $affectedrows = $this->connection->exec($sql);
        return $affectedrows;
    }

    public function delete($id)
    {
        $stmt = $this->connection->prepare("DELETE FROM `notification` WHERE `id` = :id")->execute(array(':id' => $id));
        return $stmt;
    }

    public function sendNotiFication($id)
    {
        $payload = [];

        $notification = $this->getNotificationById($id);

        if ($notification) {

            $payload['noti_title'] = $notification['title'];
            $payload['noti_msg'] = $notification['message'];
            $payload['banner_image'] = $notification['banner_image'];
            $payload['banner_size'] = $notification['banner_size'];
            $payload['target_url'] = $notification['target_url'];
            $payload['open_app'] = $notification['open_app'];

            $firebase = new Firebase($this->connection);
            $res = $firebase->sendNotification($payload);
            return $res;
        }
        return false;
    }
}
