<?php

namespace Src\Models;

class User
{
    private $connection;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function login($email, $password)
    {

        $res = $this->connection->query("SELECT * FROM `users` where `email`='$email' and status='active' limit 1")->fetch(\PDO::FETCH_ASSOC);
        if ($res) {
            $hash = $res['password'];
            if (password_verify($password, $hash)) {
                return ['status' => 1];
            }
        }
        return ['status' => 0];
    }

    public function getUserById($id)
    {
        $user = [];
        $stmt = $this->connection->query("SELECT * FROM `users` where `id`='$id'")->fetch(\PDO::FETCH_ASSOC);
        if ($stmt) {
            $user = [
                'id' => $stmt['id'],
                'name' => $stmt['name'],
                'email' => $stmt['email'],
                'avatar' => $stmt['avatar'],
                'status' => $stmt['status'],
                'user_type' => $stmt['user_type'],
                'data' => json_decode($stmt['data'], 1),
            ];
        }

        return $user;
    }

    public function getServerToken($userId)
    {
        $user = $this->getUserById($userId);
        return $user && $user['data'] && $user['data']['token'] ? $user['data']['token'] : '';
    }

    public function getUserByEmail($email)
    {
        $user = [];
        $stmt = $this->connection->query("SELECT * FROM `users` where `email`='$email' and `user_type`='api_admin'")->fetch(\PDO::FETCH_ASSOC);
        if ($stmt) {
            $user = [
                'id' => $stmt['id'],
                'name' => $stmt['name'],
                'email' => $stmt['email'],
                'avatar' => $stmt['avatar'],
                'status' => $stmt['status'],
                'user_type' => $stmt['user_type'],
                'data' => json_decode($stmt['data'], 1),
            ];
        }

        return $user;
    }

    public function updateProfile($id, $name, $avatar, $email)
    {
        $updatedAt = date('Y-m-d H:i:s', time());
        $sql = "UPDATE `users` SET `name`='$name', `avatar`='$avatar', `email`='$email',  `updated_at`='$updatedAt'  WHERE `id` = '$id'";
        return $this->connection->exec($sql);
    }

    public function updateJsonData($id, $data)
    {
        $data = json_encode($data);
        $updatedAt = date('Y-m-d H:i:s', time());
        $sql = "UPDATE `users` SET `data`='$data', `updated_at`='$updatedAt'  WHERE `id` = '$id'";
        return $this->connection->exec($sql);
    }

    public function updatePassword($id, $password)
    {
        $password = password_hash($password, PASSWORD_DEFAULT);
        $updatedAt = date('Y-m-d H:i:s', time());
        $sql = "UPDATE `users` SET `password`='$password', `updated_at`='$updatedAt'  WHERE `id` = '$id'";
        return $this->connection->exec($sql);
    }

    public function addApiUser()
    {
        $user = $this->connection->query("SELECT * FROM `users` where `email`='apiuser@ithink.com' and `user_type`='api_admin' ")->fetch(\PDO::FETCH_ASSOC);
        if (!$user) {
            $payload = [
                ':name' => 'Api User',
                ':email' => 'djds4sdsds5d4s5ds@balma.com',
                ':password' => password_hash('2h..5*3dj6^d4d#5jh!j1', PASSWORD_DEFAULT), //$2y$10$5DxtXdrGGmStQTQW5sGYgOljY54W97b0fgTNBWnPwMTlb3c.3dHAq
                ':user_type' => 'api_admin',
                ':status' => 'active',
                ':avatar' => 'no',
                ':data' => '{}',
            ];
            $stmt = $this->connection->prepare("INSERT INTO `users` (name,email,password,user_type,status,avatar,data) VALUES (  :name,:email,:password,:user_type,:status,:avatar,:data)")->execute($payload);
            return $stmt;
        }

        return true;

    }

    public function getSettings()
    {
        $setting = [];
        $stmt = $this->connection->query("SELECT * FROM `users` where `user_type`='super_admin'")->fetch(\PDO::FETCH_ASSOC);
        if ($stmt) {
            $setting=json_decode($stmt['data'], 1);
        }
        return $setting;
    }


    public function addAdminUser()
    {
        $user = $this->connection->query("SELECT * FROM `users` where `email`='apiuser@ithink.com' and `user_type`='api_admin' ")->fetch(\PDO::FETCH_ASSOC);
        if (!$user) {
            $data = [
                "token" => "demo token",
                "update" => [
                    "updateStatus" => "true",
                    "versionCode" => "1.0.1",
                    "whatsNew" => "no updates",
                ],
                "ad" => [
                    "ads" => "0",
                    "numberOfClick" => "1",
                    "showAd" => "true",
                ],
                "country" => [
                    "makerName" => "Fake Name",
                    "countryMessage" => "default message",
                    "countryStatus" => "true",
                    "icon" => "Src/dist/img/avatar5.png",
                ],
                "network" => [
                    "fbAd" => "true",
                    "fbIntreastial" => "true",
                    "fbBanner" => "true",
                    "fbNative" => "true",
                    "fbNativeBanner" => "true",
                    "adMobAd" => "true",
                    "adMobIntreastial" => "true",
                    "adMobBanner" => "true",
                    "adMobNative" => "true",
                    "adMobRv" => "true",
                    "pageClicks" => "1",
                    "bannerView" => "1",
                    "nativeView" => "1",
                ],
            ];
            $payload = [
                ':name' => 'Admin User',
                ':email' => 'admin@gmail.com',
                ':password' => password_hash('admin', PASSWORD_DEFAULT),
                ':user_type' => 'super_admin',
                ':status' => 'active',
                ':avatar' => 'Src/dist/img/avatar5.png',
                ':data' => json_encode($data),
            ];
            $stmt = $this->connection->prepare("INSERT INTO `users` (name,email,password,user_type,status,avatar,data) VALUES (  :name,:email,:password,:user_type,:status,:avatar,:data)")->execute($payload);
            return $stmt;
        }

        return true;

    }


}
