<?php

namespace Src\Models;

class VideoEffect
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }


    public function getVideoEffectById($id)
    {
        $stmt =  $this->connection->query("SELECT * FROM `video_effects` where `id`='$id'")->fetch();
        return $stmt;
    }

    public function getVideoEffectCount()
    {
        $results = [];
        $stmt =  $this->connection->query("SELECT count(*) as `count` FROM `video_effects`")->fetch();
        return $stmt['count'];
    }

    public function getVideoEffects($offSet = 0, $limit = 15, $orderBy = 'id')
    {

        $results = [];
        $sql = "SELECT `video_effects`.*,`category`.`name` as `category_name` ,`category`.`priority` ,`category`.`status`  FROM `video_effects` join `category`on `category`.`id` = `video_effects`.`category_id` WHERE `category`.`status` = 'active' ORDER BY  CASE WHEN `category`.`priority` is null then 1 else 0 end, `category`.`priority`";
        //order by `video_effects`.`$orderBy` LIMIT $limit OFFSET $offSet

        foreach ($this->connection->query($sql) as $row) {
            $record = [];
            $record['effect_category'] = $row['category_name'];
            $record['effect_name'] = $row['title'];
            $record['effect_thumb'] = $_ENV['APP_URL'] . '/' . $row['cover_image'];
            $record['effect_video_thumb'] = $_ENV['APP_URL'] . '/' . $row['video'];
            $record['effect_file_zip'] = $_ENV['APP_URL'] . '/' . $row['zip'];
            $record['created_at'] = $row['created_at'];
            $record['updated_at'] = $row['updated_at'];

            array_push($results, $record);
        }
        return $results;
    }

    public function store($payload)
    {
        $categoryId = $payload['categoryId'];
        $title = $payload['title'];
        $video = $payload['video'];
        $coverImage = $payload['coverImage'];
        $zip = $payload['zip'];
        $stmt =  $this->connection->prepare("INSERT INTO video_effects (category_id,title,cover_image,video,zip) VALUES ( :category_id, :title, :cover_image, :video, :zip )")->execute([':category_id' => $categoryId, ':cover_image' => $coverImage, ':title' => $title, ':video' => $video, ':zip' => $zip]);
        return $stmt;
    }

    public function update($payload)
    {
        $id = $payload['id'];
        $categoryId = $payload['categoryId'];
        $title = $payload['title'];
        $video = $payload['video'];
        $coverImage = $payload['coverImage'];
        $zip = $payload['zip'];
        $updatedAt = date('Y-m-d H:i:s', time());
        $sql = "UPDATE `video_effects` SET `category_id`='$categoryId', `title`='$title',`cover_image`='$coverImage', `video`='$video', `zip`='$zip',  `updated_at`='$updatedAt'  WHERE `id` = '$id'";
        $affectedrows  = $this->connection->exec($sql);
        return $affectedrows;
    }

    public function delete($id)
    {
        $stmt =  $this->connection->prepare("DELETE FROM video_effects WHERE `id` = :id")->execute(array(':id' => $id));
        return $stmt;
    }
}