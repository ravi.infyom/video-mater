<?php

namespace Src\Models;

class VideoReady
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getVideoReadyById($id)
    {
        $stmt =  $this->connection->query("SELECT * FROM `video_ready` where `id`='$id'")->fetch();
        return $stmt;
    }

    public function getVideoReadyCount()
    {
        $results = [];
        $stmt =  $this->connection->query("SELECT count(*) as `count` FROM `video_ready`")->fetch();
        return $stmt['count'];
    }

    public function getVideoReady()
    {
        $results = [];
        $sql = "SELECT `video_ready`.*,`video_ready_category`.`name` as `category_name`,`video_ready_category`.`priority` ,`video_ready_category`.`status`  FROM `video_ready` join `video_ready_category`on `video_ready_category`.`id` = `video_ready`.`category_id`  WHERE `video_ready_category`.`status` = 'active' ORDER BY  CASE WHEN `video_ready_category`.`priority` is null then 1 else 0 end, `video_ready_category`.`priority`";
        foreach ($this->connection->query($sql) as $row) {
            $data['video_category'] = $row['category_name'];
            $data['video_name'] = $row['title'];
            $data['video_thumb'] =  $_ENV['APP_URL'] . '/' . $row['cover_image'];
            $data['video'] =  $_ENV['APP_URL'] . '/' . $row['video'];
            $record['created_at'] = $row['created_at'];
            $record['updated_at'] = $row['updated_at'];

            array_push($results, $data);
        }
        return $results;
    }

    public function store($payload)
    {
        $categoryId = $payload['categoryId'];
        $title = $payload['title'];
        $video = $payload['video'];
        $coverImage = $payload['coverImage'];
        $stmt =  $this->connection->prepare("INSERT INTO video_ready (category_id,title,cover_image,video) VALUES ( :category_id, :title, :cover_image, :video )")->execute([':category_id' => $categoryId, ':cover_image' => $coverImage, ':title' => $title, ':video' => $video]);
        return $stmt;
    }

    public function update($payload)
    {
        $id = $payload['id'];
        $categoryId = $payload['categoryId'];
        $title = $payload['title'];
        $video = $payload['video'];
        $coverImage = $payload['coverImage'];
        $updatedAt = date('Y-m-d H:i:s', time());
        $sql = "UPDATE `video_ready` SET `category_id`='$categoryId', `title`='$title', `cover_image`='$coverImage', `video`='$video',  `updated_at`='$updatedAt'  WHERE `id` = '$id'";
        $affectedrows  = $this->connection->exec($sql);
        return $affectedrows;
    }

    public function delete($id)
    {
        $stmt =  $this->connection->prepare("DELETE FROM video_ready WHERE `id` = :id")->execute(array(':id' => $id));
        return $stmt;
    }
}