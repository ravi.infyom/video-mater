<?php

namespace Src\Models;


class VideoSubCategory
{
    private $connection;

    function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function getCategoryByName($name)
    {
        $results = [];
        $sql = "SELECT * FROM `video_sub_category` where `name`='$name'";
        foreach ($this->connection->query($sql) as $row) {
            $data['id'] = $row['id'];
            $data['name'] = $row['name'];
            $data['icon'] = $row['icon'];
            $data['category_id'] = $row['category_id'];


            array_push($results, $data);
        }
        return $results;
    }

    public function getCategoryByCutomFilter($filters)
    {
        $results = [];
        $query = "";
        foreach ($filters as $filter) {
            list($name, $operator, $value) = $filter;
            $query .= " and `" . $name . "` " . $operator . " '" . $value . "'";
        }
        // echo $query;
        $sql = "SELECT * FROM `video_sub_category` " . $query;
        foreach ($this->connection->query($sql) as $row) {
            $data['id'] = $row['id'];
            $data['name'] = $row['name'];
            $data['icon'] = $row['icon'];
            $data['category_id'] = $row['category_id'];

            array_push($results, $data);
        }
        return $results;
    }

    public function getCategoryById($id)
    {
        $results = [];
        $stmt =  $this->connection->query("SELECT * FROM `video_sub_category` where `id`='$id'")->fetch();
        return $stmt;
    }

    public function getSubCategoryCount()
    {
        $results = [];
        $stmt =  $this->connection->query("SELECT count(*) as `count` FROM `video_sub_category`")->fetch();
        return $stmt['count'];
    }

    public function getSubCategories()
    {
        $results = [];
        $sql = "SELECT * FROM `video_sub_category`";
        foreach ($this->connection->query($sql) as $row) {
            $data['id'] = $row['id'];
            $data['name'] = $row['name'];
            $data['icon'] = $row['icon'];
            $data['status'] = $row['status'];

            array_push($results, $data);
        }
        return $results;
    }

   
    public function store($payload)
    {
        $name = $payload['name'];
        $icon = $payload['icon'];
        $category_id = $payload['category_id'];
        $stmt =  $this->connection->prepare("INSERT INTO `video_sub_category` (name,icon,category_id) VALUES (  :name,:icon,:category_id)")->execute(array(':name' => $name, ':icon' => $icon, ':category_id' => $category_id));
        return $stmt;
    }

    public function update($payload)
    {
        $categoryId = $payload['categoryId'];
        $name = $payload['name'];
        $icon = $payload['icon'];
        $category_id = $payload['category_id'];
        $updatedAt = date('Y-m-d H:i:s', time());

    
        
        $sql = "UPDATE `video_sub_category` SET `name`= '$name' ,`icon`='$icon',`updated_at`='$updatedAt',`category_id`='$category_id'  WHERE `id` = '$categoryId'";
        $affectedrows  = $this->connection->exec($sql);
        return $affectedrows;
    }


    

    public function delete($categoryId)
    {
        $stmt =  $this->connection->prepare("DELETE FROM `video_sub_category` WHERE `id` = :id")->execute(array(':id' => $categoryId));
        return $stmt;
    }

    public function isLinkedToOther($categoryId)
    {
        $vr = $this->connection->query("SELECT count(*) as `count` FROM `video_ready` where category_id=$categoryId")->fetch();
        if ($vr['count'] > 0) {
            return false;
        } else {
            return true;
        }
    }
}