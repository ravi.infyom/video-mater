<?php

namespace Src\Service;

use Src\Models\Device;
use Src\Models\User;

class Firebase
{

    private $connection;
    private $token;

    public function __construct($connection)
    {

        if (!session_id()) {
            session_start();
        }
        $sessionName = $_ENV['SESSION_NAME'];

        if (isset($_SESSION[$sessionName]['user']['id'])) {

            $id = $_SESSION[$sessionName]['user']['id'];
            $user = new User($connection);
            $this->token = $user->getServerToken($id);
        }
        $this->connection = $connection;
    }

    public function sendNotification($payload)
    {
        if (!empty($this->token)) {
            $device = new Device($this->connection);
            $ids = $device->getAllDeviceId();
            if (is_array($ids) && count($ids) > 0) {
                $fields = array(
                    'registration_ids' => $ids,
                    'data' => $payload,
                );

                $result = $this->sendPushNotification($fields, $this->token);
                if($result)
                {
                    $data=json_decode($result,1);
                   if(is_array($data) && isset($data['success']))
                   {
                       return $data['success'] >0;
                   }
                }
                
            }
        }
        return false;

    }

    public function sendPushNotification($payload, $token)
    {

        //firebase server url to send the curl request
        $url = 'https://fcm.googleapis.com/fcm/send';

        //building headers for the request
        $headers = array(
            'Authorization: key=' . $token,
            'Content-Type: application/json',
        );

        //Initializing curl to open a connection
        $ch = curl_init();

        //Setting the curl url
        curl_setopt($ch, CURLOPT_URL, $url);

        //setting the method as post
        curl_setopt($ch, CURLOPT_POST, true);

        //adding headers
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        //disabling ssl support
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //adding the fields in json format
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($payload));

        //finally executing the curl request
        $result = curl_exec($ch);
        if ($result === false) {
            die('Curl failed: ' . curl_error($ch));
        }

        //Now close the connection
        curl_close($ch);

        //and return the result
        return $result;
    }
}
