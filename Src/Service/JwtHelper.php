<?php

namespace Src\Service;

use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;
use Src\Models\User;

class JwtHelper
{

    private $connection;
    private $token;

    public function __construct($connection)
    {
        $this->connection = $connection;
    }

    public function authenticate($email, $password)
    {
        $userObject = new User($this->connection);
        $authenticated = $userObject->login($email, $password);
        $status = array_key_exists('status', $authenticated) ? $authenticated['status'] : 0;
        if ($status) {
            $time = time();
            $xxx = base64_encode(str_shuffle($time . random_int(100000, 999999) . $time));
            $token_payload = [
                'iss' => $_ENV['APP_URL'],
                'email' => $email,
                'xxx' => $xxx,
                'yyy' => $this->encryptTime($time, 5),
            ];
            $this->token = JWT::encode($token_payload, base64_encode($_ENV['JWT_SECRET']), 'HS256');
        }
    }

    public function encryptTime($time, $minute)
    {
        $time = $time +($minute * 60);

        $time *= 6;
        $time -= 1998;
        $time *= 2;
        $time /= 4;

        return $time;
    }

    public function decryptTime($time)
    {
        $time *= 4;
        $time /= 2;
        $time += 1998;
        $time /= 6;

        return $time;
    }

    public function verify_token($token)
    {
        $verified = false;
        try {
            $decoded = (array) JWT::decode($token, base64_encode($_ENV['JWT_SECRET']), ['HS256']);
            if (array_key_exists('email', $decoded)) {
                $userObject = new User($this->connection);
                $user = $userObject->getUserByEmail($decoded['email']);
                $expire = $this->decryptTime($decoded['yyy']);
                $time = time();

                if ($user && $expire > $time) {
                    $verified = true;
                }
            }

        } catch (SignatureInvalidException $ex) {
            $verified = false;
        } catch (\Exception $e) {
            $verified = false;
        }
        return $verified;

    }

    public function getToken()
    {
        return $this->token;
    }
}
