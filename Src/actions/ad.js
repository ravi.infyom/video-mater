$(document).ready(function(e) {
    var app_url = null;
    $.getJSON("../env.json", function(json) {
        app_url = json.APP_URL
    });


    table = $('#ad_table').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "pageLength": 10,
        "searching": true,
        "draw": false,
        "autoWidth": false,
        "responsive": true,
        "ajax": {
            url: '../Src/ajax/data_tables/load_ads.php', // json
            type: "post", // type of method
            error: function() {}
        },
        'columns': [{
                data: 'id',
                name: "id",
                orderable: true,
                searchable: true
            },
            {
                name: "Ad Name",
                data: 'ad_name',
                orderable: true,
                searchable: true
            },
            {
                name: "Ad Slogan",
                data: 'ad_slogan',
                orderable: true,
                searchable: true
            },
            {
                name: "Company Name",
                data: 'company_name',
                orderable: true,
                searchable: true
            },
            {
                name: "Ad Type",
                data: 'ad_type',
                orderable: true,
                searchable: false
            },
            {
                name: "Package Name",
                data: 'package_name',
                orderable: true,
                searchable: true
            },
            {
                name: "Icon",
                data: 'ad_icon',
                orderable: false,
                searchable: false,
                render: function(data) {
                    return "<img src='" + data + "' width='50px'/>"
                }
            },
            {
                name: "Redirect Url",
                data: 'redirect_url',
                orderable: true,
                searchable: true
            },
            {
                name: "action",
                data: "action",
                orderable: false,
                searchable: false,
                render: function(data, type, row) {

                    // return 'Comming Soon'
                    let buttons = "<div class='btn-group' role='group' aria-label='Action'><button  class='view_ad btn btn-sm btn-outline-primary' title='Edit' id='view_ad' value='" + row.id + "'><i class='fas fa-edit'></i></button><button  class='delete_ad btn btn-sm btn-outline-danger' title='Delete' id='delete_ad' value='" + row.id + "'><i class='fas fa-trash'></i></button>"

                    return buttons + "</div>"
                }
            }
        ],
        initComplete: function() {
            var input = $('.dataTables_filter input').unbind(),
                self = this.api(),
                $searchButton = $('<button>')
                .html("<i class='fa fa-search'></i>")
                .addClass('btn btn-outline-primary btn-sm mx-2')
                .click(function() {
                    self.search(input.val()).draw();
                }),
                $clearButton = $('<button>')
                .html("<i class='fa fa-eraser'></i>")
                .addClass('btn btn-outline-danger btn-sm')
                .click(function() {
                    input.val('');
                    $searchButton.click();
                })
            $('.dataTables_filter').append($searchButton, $clearButton);
        }
    })

    function reload_table(){
        table.ajax.reload(null,false); //reload datatable ajax
    }


    $('#frminsert').on("submit",function (e){
        e.preventDefault();
        console.log('aaa');
        var formdata = new FormData(this);
        $.ajax({
            url: '../Src/ajax/ad_data.php', // json
            method:'POST',
            data:formdata,
            processData: false,
            contentType: false,
            success:function (data){
                console.log(data);
                $('#adName').val('');
                $('#adSlogan').val('');
                $('#companyName').val('');
                $('#adType').val('');
                $('#packageName').val('');
                $('#redirectUrl').val('');
                $('#adIcon').val('');
                $('#insertmyModal').modal("show");
                setTimeout(function (){
                    $('#insertmyModal').modal("hide");
                },2000);
                reload_table();
            }
        });
    });



    $(document).on("click", "#view_ad", function(e) {
        e.preventDefault();
        console.log('aaa');
        let id = $(this).attr('value');
        $('#edit_hidden_update_id').val(id);
        $.ajax({
            url:'../Src/ajax/ad_data.php',
            method:'POST',
            data:{id:id},
            dataType:"json",
            success:function(result){
                console.log(result);
                $('#editAdName').val(result.ad_name);
                $('#editAdSlogan').val(result.ad_slogan);
                $('#editCompanyName').val(result.company_name);
                $('#editAdType').val(result.ad_type);
                $('#editPackageName').val(result.package_name);
                $('#editRedirectUrl').val(result.redirect_url);
                // $('#editAdIcon').val(result.ad_icon);
                $('#edit_hidden_update_id').val(result.id);
                $('#myModal').modal("show");
            }});
    });

    $('#frmUpdate').on("submit",function (e){
        e.preventDefault();
        var formdata = new FormData(this);
        $.ajax({
            url: '../Src/ajax/ad_data.php', // json
            method:'POST',
            data:formdata,
            processData: false,
            contentType: false,
            success:function (data){
                console.log(data);
                $('#editAdName').val('');
                $('#editAdSlogan').val('');
                $('#editCompanyName').val('');
                $('#editAdType').val('');
                $('#editPackageName').val('');
                $('#editRedirectUrl').val('');
                $('#edit_hidden_update_id').val('');
                $('#editAdIcon').val('');
                $('#myModal').modal("hide");
                reload_table();
            }
        });
    });

    // $(document).on('click', '#view_ad', function(e) {
    //     e.preventDefault();
    //     let adId = $(this).attr('value');
    //     location.replace(app_url + '/Views/manage_ad.php?key=' + adId);
    //
    // });

    $(document).on('click', '#delete_ad', function(event) {
        event.preventDefault();
        let adId = $(this).val()
        if (adId) {
            Swal.fire({
                icon: 'error',
                title: 'Are you sure?',
                text: 'Deleting ad cannot be undone',
                showCancelButton: true,
                confirmButtonText: 'Delete',
                cancelButtonText: 'No',
                showLoaderOnConfirm: true,
                backdrop: false,
                preConfirm: () => {
                    return $.ajax({
                        url: '../Src/ajax/ad_data.php',
                        type: "POST",
                        data: { 'adId': adId, 'delete': true },
                        dataType: 'json',
                        success: function(json) {
                            if (json.status) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Ad deleted',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    onClose: () => {
                                        location.replace(app_url + '/Views/ad.php');
                                    }
                                })

                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Something went wrong',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    onClose: () => {
                                        location.replace(app_url + '/Views/ad.php');
                                    }
                                })

                            }
                        }
                    })
                },
                allowOutsideClick: () => !Swal.isLoading(),
            })
        }

    });

});