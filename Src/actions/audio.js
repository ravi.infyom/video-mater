$(document).ready(function(e) {

    var app_url = null;
    $.getJSON("../env.json", function(json) {
        app_url = json.APP_URL
    });

    function showSpinner() {
        $('#spinner').addClass('show')
        $('#main-content').addClass('disableAll')
    }

    function hideSpinner() {
        setTimeout(function() {
            $('#spinner').removeClass('show')
            $('#main-content').removeClass('disableAll')
        }, 500)
    }

    // showSpinner()

    table = $('#audio_table').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "pageLength": 10,
        "searching": true,
        "draw": false,
        "order": [
            [0, "desc"]
        ],
        "ajax": {
            url: '../Src/ajax/data_tables/load_audios.php', // json
            type: "post", // type of method
            error: function() {}
        },
        'columns': [{
                data: 'id',
                name: "Id",
                orderable: true,
                searchable: true
            },
            {
                name: "Audio Category",
                data: 'audio_category_name',
                orderable: true,
                searchable: true
            },
            {
                name: "Name",
                data: 'name',
                orderable: true,
                searchable: true
            },
            {
                name: "Audio",
                data: 'audio',
                orderable: false,
                searchable: false,
                render: function(data) {
                    return "<audio controls src=../" + data + " style='height:30px'>Your browser does not support the <code>audio</code> element.</audio>"
                }
            },
            {
                data: "Action",
                orderable: false,
                searchable: false,
                render: function(data, type, row) {
                    return "<button  class='btn_view btn btn-sm btn-primary' id='btn_view' value='" + row.id + "'><i class='fas fa-edit'></i></button><button  class='btn_delete btn btn-sm btn-danger' id='delete_audio' value='" + row.id + "'><i class='fas fa-trash'></i></button>"
                }
            }
        ],
        initComplete: function() {
            var input = $('.dataTables_filter input').unbind(),
                self = this.api(),
                $searchButton = $('<button>')
                .text('search')
                .addClass('btn btn-success btn-sm mx-2')
                .click(function() {
                    self.search(input.val()).draw();
                }),
                $clearButton = $('<button>')
                .text('clear')
                .addClass('btn btn-danger btn-sm')
                .click(function() {
                    input.val('');
                    $searchButton.click();
                })
            $('.dataTables_filter').append($searchButton, $clearButton);
            // hideSpinner()

        }
    })

    function reload_table(){
        table.ajax.reload(null,false); //reload datatable ajax
    }

    $('#frminsert').on("submit",function (e){
        e.preventDefault();
        var formdata = new FormData(this);
        $.ajax({
            url: '../Src/ajax/audio_data.php', // json
            method:'POST',
            data:formdata,
            processData: false,
            contentType: false,
            success:function (data){
                console.log(data);
                $('#txt-name').val('');
                $('#txt-Image').val('');
                $('#status').val('');
                $('#insertmyModal').modal("show");
                setTimeout(function (){
                    $('#insertmyModal').modal("hide");
                },2000);
                reload_table();
            }
        });
    });



    $(document).on("click", "#btn_view", function(e) {
        e.preventDefault();
        console.log('aaa');
        let id = $(this).attr('value');
        $('#edit_hidden_update_id').val(id);
        $.ajax({
            url:'../Src/ajax/audio_data.php',
            method:'POST',
            data:{id:id},
            dataType:"json",
            success:function(result){
                console.log(result);
                $('#edit-txt-name').val(result.name);
                // $('#edit-txt-Image').val(result.audio);
                $('#edit_audio_category_id').val(result.audio_category_id).trigger('change');
                $('#edit_hidden_update_id').val(result.id);
                $('#myModal').modal("show");
            }});
    });

    $('#frmUpdate').on("submit",function (e){
        e.preventDefault();
        var formdata = new FormData(this);
        $.ajax({
            url: '../Src/ajax/audio_data.php', // json
            method:'POST',
            data:formdata,
            processData: false,
            contentType: false,
            success:function (data){
                console.log(data);
                $('#edit-txt-name').val('');
                $('#edit-txt-Image').val('');
                $('#edit_audio_category_id').val('');
                $('#edit_hidden_update_id').val('');
                $('#myModal').modal("hide");
                reload_table();
            }
        });
    });





    // $(document).on("click", "#btn_view", function(e) {
    //     e.preventDefault();
    //     let audioId = $(this).attr('value');
    //     location.replace(app_url + '/Views/manage_audio.php?key=' + audioId);
    //
    // });

    $(document).on('click', '#delete_audio', function(event) {
        event.preventDefault();
        let deleteId = $(this).val()
        $('#delete_id').val(deleteId)
        $('#deleteModal').modal('toggle')

    });


    $(document).on("click", "#deleteAudio", function(e) {

        e.preventDefault();
        let audioId = $('#delete_id').val()
        if (audioId) {
            $.ajax({
                type: "POST",
                url: "../Src/ajax/audio_data.php",
                data: { 'audioId': audioId, 'delete': true },
                dataType: 'json',
                success: function(data) {
                    if (data.status == true) {
                        location.replace(app_url + '/Views/audio.php');
                    }

                }
            });
        }

    });

});