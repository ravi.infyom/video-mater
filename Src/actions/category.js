$(document).ready(function(e) {
    var app_url = null;
    $.getJSON("../env.json", function(json) {
        app_url = json.APP_URL
    });
    table = $('#category_table').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "pageLength": 10,
        "searching": true,
        "draw": false,
        'responsive': true,
        "order": [
            [3, "asc"]
        ],
        "ajax": {
            url: '../Src/ajax/data_tables/load_categories.php', // json
            type: "post", // type of method
            error: function() {}
        },
        'columns': [{
                data: 'id',
                name: "Id",
                orderable: true,
                searchable: true
            },
            {
                name: "Name",
                data: 'name',
                orderable: true,
                searchable: true
            },
            {
                name: "Icon",
                data: 'icon',
                orderable: false,
                searchable: false,
                render: function(data) {

                    return data ? "<img src='../" + data + "' width='50px'/>" : "Unavailable";
                }
            },
            {
                name: "Priority",
                data: 'priority',
                orderable: true,
                searchable: true
            },
            {
                name: "Status",
                data: 'status',
                orderable: true,
                searchable: true
            },
            {
                data: "Action",
                orderable: false,
                searchable: false,
                render: function(data, type, row) {
                    return "<button type='button' data-toggle='modal' data-target='#myModal' class='btn_view btn btn-sm btn-primary' id='btn_view' data-id='" + row.id + "'><i class='fas fa-edit'></i></button>" +
                        "<button  class='btn_delete btn btn-sm btn-danger' id='delete_category'  value='" + row.id + "'><i class='fas fa-trash'></i></button>"
                }
            }
        ],
        initComplete: function() {
            var input = $('.dataTables_filter input').unbind(),
                self = this.api(),
                $searchButton = $('<button>')
                .text('search')
                .addClass('btn btn-success btn-sm mx-2')
                .click(function() {
                    self.search(input.val()).draw();
                }),
                $clearButton = $('<button>')
                .text('clear')
                .addClass('btn btn-danger btn-sm')
                .click(function() {
                    input.val('');
                    $searchButton.click();
                })
            $('.dataTables_filter').append($searchButton, $clearButton);
        }
    })

    function reload_table(){
        table.ajax.reload(null,false); //reload datatable ajax
    }


    $('#frminsert').on("submit",function (e){
        e.preventDefault();
        var formdata = new FormData(this);
        $.ajax({
            url: '../Src/ajax/category_data.php', // json
            method:'POST',
            data:formdata,
            processData: false,
            contentType: false,
            success:function (data){
                console.log(data);
                $('#txt-name').val('');
                $('#txt-Image').val('');
                $('#status').val('');
                $('#insertmyModal').modal("show");
                setTimeout(function (){
                    $('#insertmyModal').modal("hide");
                },2000);
                // setInterval(function(){
                //     $('#category_table').DataTable().ajax.reload(null, false);
                    // }, 3000);
                reload_table();
            }
        });
    });



    $(document).on("click", "#btn_view", function(e) {
        e.preventDefault();
        // let categoryId = $(this).attr('data-id');

        // let categoryId = $(this).attr('value');
        // location.replace(app_url + '/Views/manage_category.php?key=' + categoryId);

        let categoryId = $(this).attr('data-id');
        $('#edit_hidden_update_id').val(categoryId);
            $.ajax({
                url:'../Src/ajax/category_data.php',
                method:'POST',
                data:{updateId:categoryId},
                dataType:"json",
                success:function(result){
                    console.log(result.id);
                    $('#edit-txt-name').val(result.name);
                    $('#edit-txt-Image').val(result.icon);
                    $('#edit-status').val(result.status);
                    $('#edit_hidden_update_id').val(result.id);
                    $('#myModal').modal("show");

                }});
        });



    $('#frmUpdate').on("submit",function (e){
        e.preventDefault();
        var formdata = new FormData(this);
        $.ajax({
            url: '../Src/ajax/category_data.php', // json
            method:'POST',
            data:formdata,
            processData: false,
            contentType: false,
            success:function (data){
                console.log(data);
                $('#edit-txt-name').val('');
                $('#edit-txt-Image').val('');
                $('#edit-status').val('');
                $('#hidden_update_id').val('');
                $('#myModal').modal("hide");
                // $('#category_table').DataTable().ajax.reload(null, false);
                // console.log(app_url);
                // location.replace('http://localhost/Laravel/video-mater/Views/category');
                // location.reload();
                reload_table();

                // $( "#datatable" ).load(window.location.href + " #datatable" );
            }
        });
    });

    $(document).on('click', '#delete_category', function(event) {
        event.preventDefault();
        let deleteId = $(this).val()
        $('#delete_id').val(deleteId)
        $('#deleteModal').modal('toggle')

    });


    // $(document).on('click', '#delete_category', function(event) {
    //     event.preventDefault();
    //     let categoryId = $(this).attr('value');
    //     console.log(categoryId);
    //     if (categoryId) {
    //         Swal.fire({
    //             icon: 'error',
    //             title: 'Are you sure?',
    //             text: 'Deleting ad cannot be undone',
    //             showCancelButton: true,
    //             confirmButtonText: 'Delete',
    //             cancelButtonText: 'No',
    //             showLoaderOnConfirm: true,
    //             backdrop: false,
    //             preConfirm: () => {
    //                 return $.ajax({
    //                     url: '../Src/ajax/category_data.php',
    //                     type: "POST",
    //                     data: { 'categoryId': categoryId, 'delete': true },
    //                     dataType: 'json',
    //                     success: function(json) {
    //                         console.log(json);
    //                         if (json.status) {
    //                             Swal.fire({
    //                                 icon: 'success',
    //                                 title: 'Ad deleted',
    //                                 showConfirmButton: false,
    //                                 timer: 1500,
    //                                 onClose: () => {
    //                                     location.replace(app_url + '/Views/category.php');
    //                                 }
    //                             })

    //                         } else {
    //                             Swal.fire({
    //                                 icon: 'error',
    //                                 title: 'Something went wrong',
    //                                 showConfirmButton: false,
    //                                 timer: 1500,
    //                                 onClose: () => {
    //                                     location.replace(app_url + '/Views/category.php');
    //                                 }
    //                             })

    //                         }
    //                     }
    //                 })
    //             },
    //             allowOutsideClick: () => !Swal.isLoading(),
    //         })
    //     }

    // });


    $(document).on("click", "#deleteCategory", function(e) {

        e.preventDefault();
        let categoryId = $('#delete_id').val()
        if (categoryId) {
            $.ajax({
                type: "POST",
                url: "../Src/ajax/category_data.php",
                data: { 'categoryId': categoryId, 'delete': true },
                dataType: 'json',
                success: function(data) {
                    if (data.status == true) {
                        location.replace(app_url + '/Views/category.php');
                    } else {
                        Swal.fire({
                            title: 'Error!',
                            text: data.message ? data.message : 'Something went wrong',
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 6000,
                            customClass: {
                                container: 'bg-secondary',
                                title: 'text-danger',
                                content: 'text-danger'
                            }
                        })
                    }

                }
            });
        }

    });



});