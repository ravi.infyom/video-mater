$(document).ready(function(e) {

    function loadCounts() {
        var id = 'all';
        $.ajax({
            type: "get",
            url: "Src/ajax/dashboard_data.php",
            dataType: 'json',
            success: function(data) {
                $('#video_category_count').html(data.videoCategoryCount)
                $('#video_effect_count').html(data.videoEffectCount)
                $('#video_ready_count').html(data.videoReadyCount)
                $('#audio_category_count').html(data.audioCategoryCount)
                $('#audio_count').html(data.audioCount)
                $('#language_count').html(data.languageCount)
                $('#video_ready_category_count').html(data.videoReadyCategoryCount)
            }
        });
    }
    var api_url = null;
    $.getJSON("./env.json", function(json) {
        api_url = json.APP_URL
    });

    loadCounts();



});