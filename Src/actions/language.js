
$(document).ready(function () {

    table = $('#language_table').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "pageLength": 10,
        "searching": true,
        "draw": false,
        "ajax": {
            url: '../Src/ajax/data_tables/load_languages.php', // json
            type: "post", // type of method
            error: function () { }
        },
        'columns': [
            {
                data: 'id',
                name: "Id",
                orderable: true,
                searchable: true
            },
            {
                name: "Language Name",
                data: 'language',
                orderable: true,
                searchable: true
            },
            {
                data: "Action",
                orderable: false,
                searchable: false,
                render: function (data, type, row) {
                    return "<button  class='btn_view btn btn-sm btn-primary' id='btn_view' value='" + row.id + "'><i class='fas fa-edit'></i></button><button  class='btn_delete btn btn-sm btn-danger' id='btn_delete' value='" + row.id + "'><i class='fas fa-trash'></i></button>"
                }
            }
        ],
        initComplete: function () {
            var input = $('.dataTables_filter input').unbind(),
                self = this.api(),
                $searchButton = $('<button>')
                    .text('search')
                    .addClass('btn btn-success btn-sm mx-2')
                    .click(function () {
                        self.search(input.val()).draw();
                    }),
                $clearButton = $('<button>')
                    .text('clear')
                    .addClass('btn btn-danger btn-sm')
                    .click(function () {
                        input.val('');
                        $searchButton.click();
                    })
            $('.dataTables_filter').append($searchButton, $clearButton);
        }
    })


    //close button
    $(document).on("click", "#close_language", function () {

        $("#close_language").hide();
        $("#language_body").hide()
        $("#open_language").show();
        clearForm();

    });

    //Show Inward Entry Form
    $(document).on("click", "#open_language", function () {

        openForm();
    });

    function clearForm() {
        $('#ln').html();
        $("#language_name").val('');
        $("#submit_language").val('add');
    }

    function openForm() {
        $("#language_body").show();
        $("#open_language").hide();
        $("#close_language").show();
    }

    $(document).on("click", "#submit_language", function (e) {

        e.preventDefault();
        let language = $("#language_name").val();
        let action = $("#submit_language").val();
        if (language == '') {
            $('#ln').html('Please enter language!!');
        } else {
            if (action == 'add') {
                $.ajax({
                    type: "POST",
                    url: "../Src/ajax/language_data.php",
                    data: { 'language': language, 'insert': true },
                    success: function (data) {
                        var json = $.parseJSON(data);
                        if (json == 'duplicate') {
                            $('#ln').html('Language already exists!!');
                        }
                        else if (json == true) {
                            location.reload(true);
                        }

                    }
                });
            }
            else if (action == 'update') {

                let languageId = $("#masterId").val();
                $.ajax({
                    type: "POST",
                    url: "../Src/ajax/language_data.php",
                    data: { 'languageId': languageId, 'language': language, 'update': true },
                    success: function (data) {
                        var json = $.parseJSON(data);

                        if (json == 'duplicate') {
                            $('#ln').html('Language already exists!!');
                        }
                        else if (json == true) {
                            location.reload(true);
                        }


                    }
                });
            }

        }

    });

    $(document).on("click", "#btn_delete", function (e) {

        e.preventDefault();
        let languageId = $(this).attr('value');
        if (languageId) {
            $.ajax({
                type: "POST",
                url: "../Src/ajax/language_data.php",
                data: { 'languageId': languageId, 'delete': true },
                success: function (data) {
                    var json = $.parseJSON(data);
                    if (json == true) {
                        location.reload(true);
                    }

                }
            });
        }

    });

    $(document).on("click", "#btn_view", function (e) {

        e.preventDefault();
        let languageId = $(this).attr('value');
        if (languageId) {
            $.ajax({
                type: "POST",
                url: "../Src/ajax/language_data.php",
                data: { 'languageId': languageId, 'show': true },
                success: function (data) {
                    var json = $.parseJSON(data);
                    if (json.id && json.language) {
                        $("#language_name").val(json.language)
                        $("#submit_language").val('update')
                        $("#masterId").val(languageId)
                        openForm();
                    }

                }
            });
        }

    });




});