$(document).ready(function() {
    var app_url = null;
    $.getJSON("./env.json", function(json) {
        app_url = json.APP_URL
    });
    $(document).on("submit", "#signin", function(e) {
        e.preventDefault();
        var email = $("#email").val();
        var psw = $("#password").val();
        $("#login-error").html("");
        $('#spinner').removeClass('hideMe')
            // alert(email+psw);
        $.ajax({
            type: "post",
            url: "Src/ajax/auth.php",
            data: { 'email': email, 'psw': psw },
            success: function(data) {
                if (data == 1) {
                    $("#email").val('');
                    $("#password").val('');
                    location.replace(app_url + '/index.php');

                } else {
                    $('#spinner').addClass('hideMe')
                    Swal.fire({
                        position: 'top-end',
                        icon: 'error',
                        title: 'Invalid Email Id/Password',
                        showConfirmButton: false,
                        timer: 4000,
                        toast: true
                    })
                    $("#email").val('');
                    $("#password").val('');
                    $("#login-error").html("Invalid Email Id/Password");
                }
            }
        });

    });
});