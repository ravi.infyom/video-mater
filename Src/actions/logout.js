$(document).ready(function(){
	var app_url = null;
    $.getJSON("../env.json", function (json) {
        app_url = json.APP_URL
    });
	$(document).on("click","#signin",function(e){
		e.preventDefault();
		var email=$("#email").val();
		var psw=$("#password").val();
		
		// alert(email+psw);
		$.ajax({
			type:"post",
			url:"Src/ajax/auth.php",
			data:{'email':email,'psw':psw},
			success:function(data)
			{
				console.log(data);
				if(data==0)
				{
					$("#email").val('');
					$("#password").val('');
					$("#msg").html("Invalid Email Id/Password");
				}
				else if(data==1)
				{
					$("#email").val('');
					$("#password").val('');
					location.replace(app_url + '/index.php');
				}
			}
		});
		
	});
});