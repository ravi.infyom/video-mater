$(document).ready(function(e) {

    var app_url = null;
    $.getJSON("../env.json", function(json) {
        app_url = json.APP_URL
    });


    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };
    const key = getUrlParameter('key');

    if (key) {
        updateFormData()
    } else {
        $("#ad_header").html('Add Ad')
        $("#adForm").removeClass("disableForm");
    }


    function updateFormData() {
        $.ajax({
            type: "POST",
            url: "../Src/ajax/ad_data.php",
            data: {
                'adId': key,
                'show': true
            },
            dataType: 'json',
            success: function(json) {
                if (json.id) {

                    //ad_name,ad_slogan,company_name,ad_type,package_name,ad_icon,redirect_url

                    $("#ad_name").val(json.ad_name)
                    $("#ad_slogan").val(json.ad_slogan)
                    $("#company_name").val(json.company_name)
                    $("input[name=ad_type][value=" + json.ad_type + "]").prop('checked', true);
                    $("#package_name").val(json.package_name)
                    let icon = json.ad_icon
                    let type = 'url'
                    let pos = icon.indexOf('/storage/ad/icon');
                    if (pos == 3) {
                        type = 'upload'
                        $('#ad_icon_hidden').val(icon)
                        icon = app_url + '/' + icon

                        $('#icon_preview').attr('src', icon)
                        $('#upload_div').removeClass('hideMe')
                        $('#url_div').addClass('hideMe')
                    } else {
                        $('#url_div').removeClass('hideMe')
                        $('#upload_div').addClass('hideMe')
                    }
                    $('#ad_icon_url').val(icon)

                    $("input[name=ad_icon_type][value=" + type + "]").prop('checked', true);
                    $('#ad_icon').val(icon)
                    $("#redirect_url").val(json.redirect_url)
                    $("#submitAd").html('Update')
                    $("#submitAd").val('update')
                    $("#ad_header").html('Update Ad')
                    $("#masterId").val(key)
                    $("#adForm").removeClass("disableForm");
                } else {
                    $("#ad_form").removeClass("disableForm");
                    location.replace(app_url + '/Views/manage_ad.php');
                }

            }
        });
    }

    $(document).on('change', '#ad_name', function(event) {
        event.preventDefault();
        let ad_name = $('#ad_name').val()
        if (ad_name != '') {
            $('#ad_name-error').html('');
        }
    });

    $(document).on('change', '#redirect_url', function(event) {
        event.preventDefault();
        let redirect_url = $('#redirect_url').val()
        if (redirect_url != '') {
            $('#redirect_url-error').html('');
        }
    });

    $(document).on('change', '#ad_icon_hidden', function(event) {
        event.preventDefault();
        let ad_icon_hidden = $('#ad_icon_hidden').val()
        if (ad_icon_hidden != '') {
            $('#ad_icon_image-error').html('');
        }
    });

    $(document).on('change', '#ad_icon_url', function(event) {
        event.preventDefault();
        let ad_icon_url = $('#ad_icon_url').val()
        if (ad_icon_url != '') {
            $('#ad_icon_url-error').html('');
        }
    });


    $(document).on('click', '#icon_preview', function(e) {
        e.preventDefault()
        $('#ad_icon_image').trigger('click');
    });


    function validateForm(data) {
        let success = 1
            // return success
        if (!data.addName) {
            success = 0
            $('#ad_name-error').text("Please Ad Name")
        }

        if (!data.url) {
            success = 0
            if (data.ad_icon_type == 'url') {
                $('#ad_icon_url-error').text("Please enter ad icon url")
            } else {
                $('#ad_icon_image-error').text("Please select ad icon image")
            }
        }

        if (!data.redirectUrl) {
            success = 0
            $('#redirect_url-error').text("Please enter redirect_url")
        }

        return success

    }

    $(document).on('click', '#submitAd', function(event) {
        event.preventDefault()


        var _this = $(this);
        let addName = $("#ad_name").val()
        let adSlogan = $("#ad_slogan").val()
        let companyName = $("#company_name").val()
        let adType = $('input[name="ad_type"]:checked').val()
        let packageName = $("#package_name").val()
        let ad_icon_type = $('input[name="ad_icon_type"]:checked').val()
        let redirectUrl = $("#redirect_url").val()

        let url = ''
        if (ad_icon_type == 'url') {
            url = $('#ad_icon_url').val()
        } else {
            url = $('#ad_icon_hidden').val()
        }
        // let payload = { addName, url, ad_icon_type, redirectUrl, adSlogan, companyName, adType, packageName }
        let payload = { addName, url, ad_icon_type, redirectUrl }
        if (validateForm(payload)) {
            _this.prop('disabled', true).text('Processing...');
            var formData = new FormData();
            formData.append('adName', addName)
            formData.append('adSlogan', adSlogan)
            formData.append('companyName', companyName)
            formData.append('adType', adType)
            formData.append('packageName', packageName)
            formData.append('adIcon', url)
            formData.append('redirectUrl', redirectUrl)

            let masterId = $("#masterId").val()
            let action = $("#submitAd").val()
            if (action == 'update' && masterId) {
                formData.append('update', true)
                formData.append('adId', masterId);
                console.log('upadte');
                $.ajax({
                    url: '../Src/ajax/ad_data.php',
                    type: "POST",
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(json) {
                        _this.prop('disabled', false).text('Save');
                        let id = json.id
                        if (json.status) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Ad Updated',
                                showConfirmButton: false,
                                timer: 1500,
                                onClose: () => {
                                    location.replace(app_url + '/Views/ad.php');
                                }
                            })

                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Something went wrong',
                                showConfirmButton: false,
                                timer: 1500,
                                onClose: () => {
                                    location.replace(app_url + '/Views/ad.php');
                                }
                            })

                        }
                    },
                    error: function(jqxhr) {}
                });
            } else {
                formData.append('insert', true)
                console.log('insert')
                $.ajax({
                    url: '../Src/ajax/ad_data.php',
                    type: "POST",
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(json) {
                        _this.prop('disabled', false).text('Save');
                        let id = json.id
                        if (json.status) {
                            Swal.fire({
                                icon: 'success',
                                title: 'Ad Created',
                                showConfirmButton: false,
                                timer: 1500,
                                onClose: () => {
                                    location.replace(app_url + '/Views/ad.php');
                                }
                            })

                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Something went wrong',
                                showConfirmButton: false,
                                timer: 1500,
                                onClose: () => {
                                    location.replace(app_url + '/Views/ad.php');
                                }
                            })

                        }
                    },
                    error: function(jqxhr) {}
                });
            }
        }
    });


    $(document).on('change', 'input[name="ad_icon_type"]', function(e) {
        e.preventDefault()
        $('#banner_image-error').html('');
        $('#banner_url-error').html('');
        let ad_icon_type = $('input[name="ad_icon_type"]:checked').val()
        if (ad_icon_type == "url") {
            $('#url_div').removeClass('hideMe')
            $('#upload_div').addClass('hideMe')
        } else {
            $('#upload_div').removeClass('hideMe')
            $('#url_div').addClass('hideMe')
        }
    })

    $("#ad_icon_image").change(function(e) {

        $('#ad_icon_image-error').html('');
        if ((file = this.files[0])) {
            uploadAdIcon(file);
        }
    });

    function uploadAdIcon(file) {

        var fd = new FormData();
        fd.append('adIcon', file);
        fd.append('upload', true);
        $.ajax({
            url: '../Src/ajax/ad_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            dataType: 'json',
            data: fd,
            success: function(json, status) {
                if (json.status) {
                    icon = app_url + '/' + json.path
                    $('#ad_icon_hidden').val(json.path);
                    $('#icon_preview').attr('src', icon)
                } else {
                    alert('Cannot upload file.');
                }
            },
            error: function(jqxhr, status, msg) {
                //error code
            }
        });
    }

});