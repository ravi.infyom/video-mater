$(document).ready(function(e) {


    var app_url = null;
    $.getJSON("../env.json", function(json) {
        app_url = json.APP_URL
    });

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };
    const key = getUrlParameter('key');

    load_categories()

    function load_categories() {
        $.ajax({
            url: '../Src/ajax/audio_category_data.php',
            type: "POST",
            data: { 'audioCategoryNames': true },
            dataType: 'json',
            success: function(json) {
                if (json.status) {
                    var finalOptions = "<option value='' disabled selected>Select Audio Category</option>"
                    json.data.forEach(function(row) {
                        finalOptions = finalOptions + "<option value=" + row.id + ">" + row.name + "</option>"
                    });
                    $('#audio_category').html(finalOptions);
                }
            },
            error: function(jqxhr) {}
        })
    }

    if (key) {
        updateFormData()
    } else {
        $("#audio_header").html('Add Audio')
        $("#audio_form").removeClass("disableForm");
    }

    $(document).on('change', '#name', function(event) {
        event.preventDefault();
        $('#name-error').html('');
    });

    function updateFormData() {
        $.ajax({
            type: "POST",
            url: "../Src/ajax/audio_data.php",
            data: {
                'audioId': key,
                'show': true
            },
            dataType: 'json',
            success: function(json) {
                if (json.id) {

                    $("#name").val(json.name)
                    $("#audio_category").val(json.category_id).change();
                    //    while($("#audio_category").val() == null)
                    //    {
                    //     $("#audio_category").val(json.category_id).change();
                    //    }
                    $("#audio_image").attr('src', '../Src/dist/img/mp3-4.jpg')
                    $('#audio_preview').attr('src', "../" + json.audio)
                    $('#audio_preview').removeClass('hideMe')
                    $("#audio_hidden").val(json.audio)
                    $('.remove_audio').removeClass('hideMe')

                    $("#submitAudio").html('Update')
                    $("#submitAudio").val('update')
                    $("#audio_header").html('Update Audio')
                    $("#masterId").val(key)
                    $("#audio_form").removeClass("disableForm");
                } else {
                    $("#audio_form").removeClass("disableForm");
                    location.replace(app_url + '/Views/manage_audio.php');
                }

            }
        });
    }

    function validateForm(data) {
        let success = 1

        if (!data.name) {
            success = 0
            $('#name-error').text("Please enter name")
        }
        if (!data.audio_category) {
            success = 0
            $('#audio_category-error').text("Please select category")
        }
        if (!data.audio) {
            success = 0
            $('#audio-error').text("Please upload audio")
        }
        return success

    }

    $(document).on('click', '#submitAudio', function(event) {
        event.preventDefault()

        var _this = $(this);
        let name = $("#name").val()
        let audio_category = $("#audio_category").val()
        let audio = $("#audio_hidden").val()
        let payload = { name, audio, audio_category }
        if (validateForm(payload)) {
            _this.prop('disabled', true).text('Processing...');
            var formData = new FormData();
            formData.append('name', name)
            formData.append('audio_category_id', audio_category)
            formData.append('audio', audio)

            let masterId = $("#masterId").val()
            let action = $("#submitAudio").val()
            if (action == 'update' && masterId) {
                formData.append('update', true)
                formData.append('audioId', masterId);
                $.ajax({
                    url: '../Src/ajax/audio_data.php',
                    type: "POST",
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(json) {
                        _this.prop('disabled', false).text('Save');
                        if (json.status) {
                            location.replace(app_url + '/Views/audio.php');
                        }
                    },
                    error: function(jqxhr) {
                        console.log(jqxhr)
                    }
                });
            } else {
                formData.append('insert', true)
                $.ajax({
                    url: '../Src/ajax/audio_data.php',
                    type: "POST",
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(json) {
                        _this.prop('disabled', false).text('Save');
                        if (json.status) {
                            location.replace(app_url + '/Views/audio.php');
                        }
                    },
                    error: function(jqxhr) {}
                });
            }
        }
    });

    $(document).on('click', '#audio_image', function(event) {
        event.preventDefault();
        $('#audio').trigger('click');
    });

    //  on link click open ekkoLightBox
    $(document).on('click', '[data-toggle="lightboxVideo"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    $("#audio").change(function(e) {

        $('#audio-error').html('');
        console.log(this.files[0])
        if ((file = this.files[0])) {
            uploadAudio(file);
        }
    });

    $(document).on('change', '#audio_category', function(e) {
        e.preventDefault();
        if ($('#audio_category').val()) {
            $('#audio_category-error').html('');
        }
    });

    $(document).on('click', '#remove_audio', function(event) {
        event.preventDefault();
        $('#delete_file_id').val('audio')
        $('#deleteFileModal').modal('toggle')

    });

    var clearAudioComponents = function() {
        $('#audio_hidden').val('');
        $('#audio_preview').attr('src', '').addClass('hideMe')
        $('#audio_image').attr('src', '../Src/dist/img/mp3-4.jpg')
        $('.remove_audio').addClass('hideMe')
    }

    $(document).on('click', '#remove_icon', function(event) {
        event.preventDefault();
        $('#delete_file_id').val('icon')
        $('#deleteModal').modal('toggle')

    });
    $(document).on('click', '#delete_file', function(event) {
        event.preventDefault();
        let file = null
        let cb = function() {}
        let fileId = $('#delete_file_id').val()
        if (fileId == 'audio') {
            file = $("#audio_hidden").val()
            cb = clearAudioComponents
        }

        if (file != null) {
            deleteFile(file, 'audio', cb)
        }

    });

    var deleteFile = function(file, fieldName, cb) {

        var fd = new FormData();
        fd.append('filePath', file);
        fd.append('fieldName', fieldName);
        fd.append('deleteFile', true);
        var status = false
        $.ajax({
            url: '../Src/ajax/audio_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    cb()
                }
            },
            error: function(jqxhr, status, msg) {}
        });


        return status
    }

    function uploadAudio(file) {
        $('.audio-spinner').removeClass('hideMe')
        var fd = new FormData();
        fd.append('audio', file);
        fd.append('uploadAudio', true);
        $.ajax({
            url: '../Src/ajax/audio_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            dataType: 'json',
            data: fd,
            success: function(json, status) {
                if (json.status) {
                    $('#audio_hidden').val(json.path);
                    renderMedia("../" + json.path);
                    $('.remove_audio').removeClass('hideMe');
                    $('#submitAudio').prop('disabled', false);

                } else {
                    alert('Cannot upload file.');
                }
                $('.audio-spinner').addClass('hideMe')

            },
            error: function(jqxhr, status, msg) {
                //error code
                console.log(jqxhr)
            }
        });
    }

    function renderMedia(file) {
        $('#audio_preview').attr('src', file)
        $('#audio_preview').removeClass('hideMe')
        $("[data-s3='1']").on('load', function() {}).attr("src", "../Src/dist/img/edit-image.png");

    }
});