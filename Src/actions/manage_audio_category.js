$(document).ready(function(e) {

    var app_url = null;
    $.getJSON("../env.json", function(json) {
        app_url = json.APP_URL
    });

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };
    loadPriorities();

    function loadPriorities() {

        $.ajax({
            type: "POST",
            url: "../Src/ajax/audio_category_data.php",
            data: {
                'getCategoryCount': true
            },
            dataType: 'json',
            success: function(data) {
                let count = 20;
                if (data && data.count) {
                    count = data.count
                }

                let options = "<option value=''>-- Choose Priority --</option>"
                for (let i = 1; i <= count; i++) {
                    options += "<option value='" + i + "'>" + i + "</option>"
                }
                $("#priority").html(options)
            }
        });
    }
    const key = getUrlParameter('key');
    if (key) {
        $.ajax({
            type: "POST",
            url: "../Src/ajax/audio_category_data.php",
            data: {
                'audioCategoryId': key,
                'show': true
            },
            dataType: 'json',
            success: function(data) {
                if (data.id && data.name) {
                    $("#audio_category").val(data.name)
                    $("#status").val(data.status)
                    $("#addAudioCategory").val('update')
                    $("#addAudioCategory").html('update')
                    $("#audio_category_header").html('Update Category')
                    if (data.icon) {
                        $('#icon_preview').attr("src", "../" + data.icon);
                        $('#icon_hidden').val(data.icon);
                        $('.remove_icon').removeClass('hideMe')
                    }
                    $("#masterId").val(key)
                    $("#audio_category_form").removeClass("disableForm");
                    $("#priority").val(data.priority)
                    $("#priority").attr('disabled', false)
                } else {
                    $("#audio_category_form").removeClass("disableForm");
                    location.replace(app_url + '/Views/manage_audio_category.php');
                }
            }
        });
    } else {
        $("#audio_category_header").html('Add Category')
        $("#audio_category_form").removeClass("disableForm");
    }

    $(document).on('change', "#audio_category", function(e) {
        e.preventDefault()
        const val = $('#audio_category').val()
        if (val != '') {
            $('#audio_category-error').html('')
        }
    });

    $(document).on('click', "#icon_preview", function(e) {
        e.preventDefault()
        $('#icon').trigger('click')
    });

    $(document).on('change', "#status", function(e) {
        e.preventDefault()
        const val = $('#status').val()
        if (val != '') {
            $('#status-error').html('')
        }
    });

    var _URL = window.URL || window.webkitURL;
    var file, img;

    $("#icon").change(function(e) {


        if ((file = this.files[0])) {
            $('#icon-error').html('');

            uploadFile(file);
            $('#icon_preview').attr({
                src: _URL.createObjectURL(file)
            });
            img = new Image();
            // img.onload = function () {

            //     checkImageDImension(this.width, this.height, file);
            // };
            img.src = _URL.createObjectURL(file);

        }
    });

    function uploadFile(file) {

        var fd = new FormData();
        fd.append('icon', file);
        fd.append('uploadIcon', true);

        $.ajax({
            url: '../Src/ajax/audio_category_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $('#icon_hidden').val(data.path);
                    $('.remove_icon').removeClass('hideMe')
                } else {
                    $('#icon-error').html(data.message);
                }
            },
            error: function(jqxhr, status, msg) {
                //error code
            }
        });
    }

    var clearIconComponents = function() {
        $('#icon_hidden').val('');
        $('#icon').val('')
        $('#icon_preview').attr('src', '../Src/dist/img/add-plus-2.png')
        $('.remove_icon').addClass('hideMe')
    }

    $(document).on('click', '#remove_icon', function(event) {
        event.preventDefault();
        $('#delete_file_id').val('icon')
        $('#deleteModal').modal('toggle')

    });
    $(document).on('click', '#delete_file', function(event) {
        event.preventDefault();
        let file = null
        let cb = function() {}
        let fileId = $('#delete_file_id').val()
        if (fileId == 'icon') {
            file = $("#icon_hidden").val()
            cb = clearIconComponents
        }

        if (file != null) {
            deleteFile(file, 'icon', cb)
        }

    });

    var deleteFile = function(file, fieldName, cb) {

        var fd = new FormData();
        fd.append('filePath', file);
        fd.append('fieldName', fieldName);
        fd.append('deleteFile', true);
        var status = false
        $.ajax({
            url: '../Src/ajax/audio_category_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    cb()

                }
            },
            error: function(jqxhr, status, msg) {}
        });


        return status
    }

    function validateForm(data) {
        let success = 1
        if (data.category_name == '') {
            success = 0
            $('#audio_category-error').text("Please enter category name")
        }

        // if (data.icon == '') {
        //     success = 0
        //     $('#icon-error').text("Please upload icon")
        // }
        if (!data.status) {
            success = 0
            $('#status-error').text("Please select status")
        }
        return success

    }

    $(document).on('click', '#addAudioCategory', function(e) {
        e.preventDefault();

        let action = $("#addAudioCategory").val();
        var category_name = $('#audio_category').val();
        var icon = $('#icon_hidden').val();
        var status = $('#status').val();
        var payload = { category_name, icon, status }


        if (validateForm(payload)) {

            var formData = new FormData();
            formData.append('icon', icon);
            formData.append('audioCategoryName', category_name);
            formData.append('status', status);

            if (action == 'update') {
                id = $("#masterId").val()
                var priority = $('#priority').val();
                formData.append('priority', priority);
                formData.append('audioCategoryId', id);
                formData.append('update', true);
                $.ajax({
                    type: 'POST',
                    url: '../Src/ajax/audio_category_data.php',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        $('#audio_category_form').addClass("disableForm");
                    },
                    success: function(json) {
                        $('#audio_category_form').removeClass("disableForm");
                        if (json.status == 1) {
                            location.replace(app_url + '/Views/audio_category.php');
                        } else if (json.error.category) {
                            $('#audio_category-error').html(json.error.category)
                        } else {
                            $('#common-error').html(error)
                        }
                    },
                    error: function(error) {
                        console.log(error)
                    }
                });
            } else {
                formData.append('insert', true);
                $.ajax({
                    type: 'POST',
                    url: '../Src/ajax/audio_category_data.php',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        $('#audio_category_form').addClass("disableForm");
                    },
                    success: function(json) {
                        $('#audio_category_form').removeClass("disableForm");
                        if (json.status == 1) {
                            location.replace(app_url + '/Views/audio_category.php');
                        } else {
                            if (json.error.category) {
                                $('#audio_category-error').html(json.error.category)
                            }
                        }
                        $('#audioCategoryForm').css("opacity", "");
                        $("#addAudioCategory").removeAttr("disabled");
                    },
                    error: function(error) {
                        console.log(error)
                    }
                });

            }

        }

    });

});