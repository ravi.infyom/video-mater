$(document).ready(function(e) {

    var app_url = null;
    $.getJSON("../env.json", function(json) {
        app_url = json.APP_URL
    });


    $(document).on('change', '#title', function(event) {
        event.preventDefault();
        let title = $('#title').val()
        if (title != '') {
            $('#title-error').html('');
        }
    });

    $(document).on('change', '#message', function(event) {
        event.preventDefault();
        let message = $('#message').val()
        if (message != '') {
            $('#message-error').html('');
        }
    });

    $(document).on('change', '#banner_url', function(event) {
        event.preventDefault();
        let banner_url = $('#banner_url').val()
        if (banner_url != '') {
            $('#banner_url-error').html('');
        }
    });

    $(document).on('change', '#banner_hidden', function(event) {
        event.preventDefault();
        let banner_hidden = $('#banner_hidden').val()
        if (banner_hidden != '') {
            $('#banner_image-error').html('');
        }
    });

    $(document).on('change', '#target_url', function(event) {
        event.preventDefault();
        let target_url = $('#target_url').val()
        if (target_url != '') {
            $('#target_url-error').html('');
        }
    });

    function validateForm(data) {
        let success = 1
            // return success
        if (!data.title) {
            success = 0
            $('#title-error').text("Please enter title")
        }
        if (!data.message) {
            success = 0
            $('#message-error').text("Please enter message")
        }
        // if (!data.url) {
        //     success = 0
        //     if (data.banner_type == 'url') {
        //         $('#banner_url-error').text("Please enter banner url")
        //     } else {
        //         $('#banner_image-error').text("Please select banner image")
        //     }
        // }

        // if (!data.target_url) {
        //     success = 0
        //     $('#target_url-error').text("Please enter target_url")
        // }

        return success

    }

    $(document).on('click', '#submitNotification', function(event) {
        event.preventDefault()

        var _this = $(this);
        let title = $("#title").val()
        let message = $("#message").val()
        let target_url = $("#target_url").val()
        let open_app = $("#open_app").prop('checked')
        let banner_size = $('input[name="banner_size"]:checked').val()
        let banner_type = $('input[name="banner_type"]:checked').val()
        let url = ''
        if (banner_type == 'url') {
            url = $('#banner_url').val()
        } else {
            url = $('#banner_hidden').val()
        }
        let payload = { title, message, url, target_url, banner_type }
            // validateForm(payload)
        if (validateForm(payload)) {
            _this.prop('disabled', true).text('Processing...');
            var formData = new FormData();
            formData.append('title', title)
            formData.append('message', message)
            formData.append('banner_image', url)
            formData.append('target_url', target_url)
            formData.append('open_app', open_app)
            formData.append('banner_size', banner_size)
            formData.append('insert', true)


            $.ajax({
                url: '../Src/ajax/notification_data.php',
                type: "POST",
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function(json) {
                    _this.prop('disabled', false).text('Save');
                    if (json.status) {
                        let id = json.id
                        Swal.fire({
                            title: 'Want to send notification ?',
                            showCancelButton: true,
                            confirmButtonText: 'Send',
                            cancelButtonText: 'No',
                            showLoaderOnConfirm: true,
                            preConfirm: () => {

                                return $.ajax({
                                    url: '../Src/ajax/notification_data.php',
                                    type: "POST",
                                    data: { 'notificationId': id, 'send': true },
                                    dataType: 'json',
                                    success: function(json) {
                                        if (json.status) {
                                            Swal.fire({
                                                icon: 'success',
                                                title: 'Notification sent',
                                                showConfirmButton: false,
                                                timer: 1500,
                                                onClose: () => {
                                                    location.replace(app_url + '/Views/notification.php');
                                                }
                                            })

                                        } else {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Something went wrong',
                                                showConfirmButton: false,
                                                timer: 1500,
                                                onClose: () => {
                                                    location.replace(app_url + '/Views/notification.php');
                                                }
                                            })

                                        }
                                    }
                                })
                            },
                            allowOutsideClick: () => !Swal.isLoading(),
                            onClose: () => {
                                location.replace(app_url + '/Views/notification.php');
                            }
                        })
                    }
                },
                error: function(jqxhr) {}
            });
        }
    });

    $(document).on('change', 'input[name="banner_type"]', function(e) {
        e.preventDefault()
        $('#banner_image-error').html('');
        $('#banner_url-error').html('');
        let banner_type = $('input[name="banner_type"]:checked').val()
        if (banner_type == "url") {
            $('#url_div').removeClass('hideMe')
            $('#upload_div').addClass('hideMe')
        } else {
            $('#upload_div').removeClass('hideMe')
            $('#url_div').addClass('hideMe')
        }
    })

    var clearAudioComponents = function() {

        $('#audio_hidden').val('');
        $('#audio_preview').attr('src', '').addClass('hideMe')
        $('#audio_image').attr('src', '../Src/dist/img/mp3-4.jpg')
        $('.remove_audio').addClass('hideMe')
    }

    $("#banner_image").change(function(e) {

        $('#banner_image-error').html('');
        if ((file = this.files[0])) {
            uploadNotification(file);
        }
    });

    function uploadNotification(file) {

        var fd = new FormData();
        fd.append('bannerImage', file);
        fd.append('upload', true);
        $.ajax({
            url: '../Src/ajax/notification_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            dataType: 'json',
            data: fd,
            success: function(json, status) {
                if (json.status) {
                    $('#banner_hidden').val(app_url + "/" + json.path);
                } else {
                    alert('Cannot upload file.');
                }
            },
            error: function(jqxhr, status, msg) {
                //error code
            }
        });
    }

    function renderMedia(file) {
        $('#audio_preview').attr('src', file)
        $('#audio_preview').removeClass('hideMe')
        $("[data-s3='1']").on('load', function() {}).attr("src", "../Src/dist/img/edit-image.png");

    }
});