$(document).ready(function(e) {

    $(document).on('change', '#category', function(event) {
        event.preventDefault();
        $('#category-error').html('');
    });

    $(document).on('change', '#title', function(event) {
        event.preventDefault();
        $('#title-error').html('');
    });


    function validateForm(data) {
        let success = 1
        if (data.category == null || data.category == 0) {
            success = 0
            $('#category-error').text("Please select category")
        }
        if (data.title == '') {
            success = 0
            $('#title-error').text("Please enter title")
        }

        if (data.cover_image == '') {
            success = 0
            $('#cover_image-error').text("Please upload cover image")
        }
        if (data.video == '') {
            success = 0
            $('#video-error').text("Please upload video")
        }
        return success

    }
    var app_url = null;
    $.getJSON("../env.json", function(json) {
        app_url = json.APP_URL
    });

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };

    function load_categories() {
        return $.ajax({
            url: '../Src/ajax/video_ready_category_data.php',
            type: "POST",
            data: { 'categoryNames': true },
            async: true,
            dataType: 'json',
            success: function(json) {
                if (json.status) {
                    var finalOptions = "<option value='' disabled selected>Select Category</option>"
                    json.data.forEach(function(row) {
                        finalOptions = finalOptions + "<option value=" + row.id + ">" + row.name + "</option>"

                    });

                    $('#category').html(finalOptions);

                }

            },
            error: function(jqxhr) {}
        });
    }
    async function callAsync(functionName) {
        await functionName()
    }
    callAsync(load_categories);

    async function updateFormData() {
        $.ajax({
            type: "POST",
            url: "../Src/ajax/video_ready_data.php",
            data: {
                'videoReadyId': key,
                'show': true
            },
            dataType: 'json',
            success: function(json) {
                if (json.id) {

                    $("#category").val(json.category_id).change();
                    // $("#category_id").val(json.category_id).trigger('select')
                    $("#title").val(json.title)

                    $("#image_preview").attr('src', "../" + json.cover_image)
                    $("#cover_image_hidden").val(json.cover_image)
                    $(".remove_cover_image").removeClass('hideMe')

                    $("#video_image").attr('src', '../Src/dist/img/edit-image.png')
                    $(".video").attr("href", "../" + json.video).removeClass("hideMe");
                    $("#video_hidden").val(json.video)
                    $(".remove_video").removeClass('hideMe')


                    $("#submitVideoReady").html('Update')
                    $("#submitVideoReady").val('update')
                    $("#video_ready_header").html('Update Video Ready')
                    $("#masterId").val(key)
                    $("#video_ready_form").removeClass("disableForm");
                } else {
                    $("#video_ready_form").removeClass("disableForm");
                    location.replace(app_url + '/Views/manage_video_ready.php');
                }

            }
        });
    }
    const key = getUrlParameter('key');
    if (key) {
        callAsync(updateFormData);
    } else {
        $("#video_ready_header").html('Add Video Ready')
        $("#video_ready_form").removeClass("disableForm");
    }

    $(document).on('click', '#submitVideoReady', function(event) {
        event.preventDefault()

        var _this = $(this);
        let category = $("#category").val()
        let title = $("#title").val()
        let video = $("#video_hidden").val()
        let cover_image = $("#cover_image_hidden").val()
        let payload = { category, title, cover_image, video }
        if (validateForm(payload)) {
            console.log(payload)
            _this.prop('disabled', true).text('Processing...');
            var formData = new FormData();
            formData.append('category', category)
            formData.append('title', title)
            formData.append('video', video)
            formData.append('cover_image', cover_image)

            let masterId = $("#masterId").val()
            let action = $("#submitVideoReady").val()

            if (action == 'update' && masterId) {
                formData.append('update', true)
                formData.append('videoReadyId', masterId);
                $.ajax({
                    url: '../Src/ajax/video_ready_data.php',
                    type: "POST",
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(json) {
                        _this.prop('disabled', false).text('Save');
                        if (json.status) {
                            location.replace(app_url + '/Views/video_ready.php');
                        }
                    },
                    error: function(jqxhr) {
                        console.log(jqxhr)
                    }
                });
            } else {
                formData.append('insert', true)
                $.ajax({
                    url: '../Src/ajax/video_ready_data.php',
                    type: "POST",
                    data: formData,
                    cache: false,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(json) {
                        _this.prop('disabled', false).text('Save');
                        if (json.status) {
                            location.replace(app_url + '/Views/video_ready.php');
                        }
                    },
                    error: function(jqxhr) {
                        console.log(jqxhr)
                    }
                });
            }
        }
    });


});