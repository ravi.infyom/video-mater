$(document).ready(function(e) {

    var app_url = null;
    $.getJSON("../env.json", function(json) {
        app_url = json.APP_URL
    });

    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };
    async function callAsync(functionName) {
        await functionName()
    }
    callAsync(load_categories);

    function load_categories() {
        $.ajax({
            url: '../Src/ajax/category_data.php',
            type: "POST",
            data: { 'categoryNames': true },
            async: true,
            dataType: 'json',
            success: function(json) {
                if (json.status) {
                    var finalOptions = "<option value='' disabled selected>Select Category</option>"
                    json.data.forEach(function(row) {
                        finalOptions = finalOptions + "<option value=" + row.id + ">" + row.name + "</option>"

                    });
                    $('#category_id').html(finalOptions);
                }

            },
            error: function(jqxhr) {
                console.log(jqxhr)
            }
        });
    }
    const key = getUrlParameter('key');
    if (key) {
        load_categories();

        $.ajax({
            type: "POST",
            url: "../Src/ajax/video_sub_category_data.php",
            data: {
                'categoryId': key,
                'show': true
            },
            dataType: 'json',
            success: function(data) {
                if (data.id && data.name) {
                    $("#subcategory").val(data.name)
                    $("#addCategory").val('update')
                    $("#addCategory").html('update')
                    $("#category_header").html('Update Category')
                    if (data.icon) {
                        $('#icon_preview').attr("src", "../" + data.icon);
                        $('#icon_hidden').val(data.icon);
                        $('.remove_icon').removeClass('hideMe')
                    }
                    $("#masterId").val(key)
                    $("#category_form").removeClass("disableForm");
                    $("#category_id").val(data.category_id)

                } else {
                    $("#category_form").removeClass("disableForm");
                    location.replace(app_url + '/Views/video_sub_category.php');
                }
            }
        });
    } else {
        $("#category_header").html('Add Category')
        $("#category_form").removeClass("disableForm");
    }

    $(document).on('change', "#category", function(e) {
        e.preventDefault()
        const val = $('#category').val()
        if (val != '') {
            $('#category-error').html('')
        }
    });


    $(document).on('click', "#icon_preview", function(e) {
        e.preventDefault()
        $('#icon').trigger('click')
    });



    var _URL = window.URL || window.webkitURL;
    var file, img;

    $("#icon").change(function(e) {

        if ((file = this.files[0])) {
            $('#icon-error').html('');

            uploadFile(file);
            $('#icon_preview').attr({
                src: _URL.createObjectURL(file)
            });
            img = new Image();
            // img.onload = function () {

            //     checkImageDImension(this.width, this.height, file);
            // };
            img.src = _URL.createObjectURL(file);

        }
    });

    function uploadFile(file) {

        var fd = new FormData();
        fd.append('icon', file);
        fd.append('uploadIcon', true);

        $.ajax({
            url: '../Src/ajax/category_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $('#icon_hidden').val(data.path);
                    $('.remove_icon').removeClass('hideMe')
                } else {
                    $('#icon-error').html(data.message);
                }
            },
            error: function(jqxhr, status, msg) {
                //error code
            }
        });
    }

    var clearIconComponents = function() {
        $('#icon_hidden').val('');
        $('#icon').val('')
        $('#icon_preview').attr('src', '../Src/dist/img/add-plus-2.png')
        $('.remove_icon').addClass('hideMe')
    }

    $(document).on('click', '#remove_icon', function(event) {
        event.preventDefault();
        $('#delete_file_id').val('icon')
        $('#deleteModal').modal('toggle')

    });
    $(document).on('click', '#delete_file', function(event) {
        event.preventDefault();
        let file = null
        let cb = function() {}
        let fileId = $('#delete_file_id').val()
        if (fileId == 'icon') {
            file = $("#icon_hidden").val()
            cb = clearIconComponents
        }

        if (file != null) {
            deleteFile(file, 'icon', cb)

        }

    });

    var deleteFile = function(file, fieldName, cb) {

        var fd = new FormData();
        fd.append('filePath', file);
        fd.append('fieldName', fieldName);
        fd.append('deleteFile', true);
        var status = false
        $.ajax({
            url: '../Src/ajax/category_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    cb()

                }
            },
            error: function(jqxhr, status, msg) {}
        });


        return status
    }

    function validateForm(data) {
        let success = 1
        if (!data.sub_category_name) {
            success = 0
            $('#subcategory-error').text("Please enter sub category name")
        }
        if (!data.category_id) {
            success = 0
            $('#category-error').text("Please Select category name")
        }

        if (!data.icon) {
            success = 0
            $('#icon-error').text("Please upload icon")
        }


        return success

    }

    $(document).on('click', '#addCategory', function(e) {
        // console.log('a')
        e.preventDefault();

        let action = $("#addCategory").val();
        var sub_category_name = $('#subcategory').val();
        var icon = $('#icon_hidden').val();
        var category_id = $('#category_id').val();
        var payload = { sub_category_name, icon, category_id }

        if (validateForm(payload)) {

            var formData = new FormData();
            formData.append('icon', icon);
            formData.append('subcategoryName', sub_category_name);
            formData.append('category_id', category_id);

            if (action == 'update') {
                id = $("#masterId").val()
                formData.append('subcategoryId', id);
                formData.append('update', true);
                $.ajax({
                    type: 'POST',
                    url: '../Src/ajax/video_sub_category_data.php',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        $('#category_form').addClass("disableForm");
                    },
                    success: function(json) {
                        console.log(json);
                        $('#category_form').removeClass("disableForm");
                        if (json.status == 1) {
                            if (action == 'update'){
                                location.reload();
                            }
                            $('#myTab a:first').tab('show');
                            $('#category_table').DataTable().ajax.reload();
                        } else if (json) {
                            $('#category-error').html(json.error.category)
                        } else {
                            $('#common-error').html(error)
                        }
                    },
                    error: function(error) {
                        console.log(error)
                    }
                });
            } else {
                formData.append('insert', true);
                $.ajax({
                    type: 'POST',
                    url: '../Src/ajax/video_sub_category_data.php',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function() {
                        $('#category_form').addClass("disableForm");
                    },
                    success: function(json) {
                        $('#category_form').removeClass("disableForm");
                        if (json.status == 1) {
                            $('#myTab a:first').tab('show');
                            $('#subcategory_table').DataTable().ajax.reload();
                        } else {
                            if (json.error.category) {
                                $('#category-error').html(json.error.category)
                            }
                        }
                        $('#categoryForm').css("opacity", "");
                        $("#addCategory").removeAttr("disabled");
                    },
                    error: function(error) {
                        console.log(error)
                    }
                });

            }

        }

    });
});