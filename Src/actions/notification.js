$(document).ready(function(e) {
    var app_url = null;
    $.getJSON("../env.json", function(json) {
        app_url = json.APP_URL
    });


    table = $('#notification_table').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "pageLength": 10,
        "searching": true,
        "draw": false,
        "autoWidth": false,
        "responsive": true,
        "ajax": {
            url: '../Src/ajax/data_tables/load_notifications.php', // json
            type: "post", // type of method
            error: function() {}
        },
        'columns': [{
                data: 'id',
                name: "id",
                orderable: true,
                searchable: true
            },
            {
                name: "title",
                data: 'title',
                orderable: true,
                searchable: true
            },
            {
                name: "message",
                data: 'message',
                orderable: true,
                searchable: true
            },
            {
                name: "banner_size",
                data: 'banner_size',
                orderable: true,
                searchable: true
            },
            {
                name: "open_app",
                data: 'open_app',
                orderable: true,
                searchable: false,
                render: function(data) {
                    return data == true ? 'True' : 'False'
                }
            },
            {
                name: "status",
                data: 'status',
                orderable: true,
                searchable: true
            },
            {
                name: "action",
                data: "action",
                orderable: false,
                searchable: false,
                render: function(data, type, row) {

                    // return 'Comming Soon'
                    let buttons = "<div class='btn-group' role='group' aria-label='Action'>"

                    if (row.status == 'PENDING' || row.status == 'FAILED') {
                        buttons = buttons + "<button  class='btn_resend btn btn-sm btn-outline-primary' data-toggle='tooltip' title='Resend' id='resend_notification' value='" + row.id + "'><i class='fas fa fa-repeat'></i></button>";
                    }

                    buttons = buttons + "<button  class='btn_delete btn btn-sm btn-outline-danger' title='Delete' id='delete_notification' value='" + row.id + "'><i class='fas fa-trash'></i></button>"

                    return buttons + "</div>"
                }
            }
        ],
        initComplete: function() {
            var input = $('.dataTables_filter input').unbind(),
                self = this.api(),
                $searchButton = $('<button>')
                .html("<i class='fa fa-search'></i>")
                .addClass('btn btn-outline-primary btn-sm mx-2')
                .click(function() {
                    self.search(input.val()).draw();
                }),
                $clearButton = $('<button>')
                .html("<i class='fa fa-eraser'></i>")
                .addClass('btn btn-outline-danger btn-sm')
                .click(function() {
                    input.val('');
                    $searchButton.click();
                })
            $('.dataTables_filter').append($searchButton, $clearButton);
        }
    })



    $(document).on("click", "#view_notification", function(e) {
        e.preventDefault();
        let notificationId = $(this).attr('value');
        location.replace(app_url + '/Views/manage_notification.php?key=' + notificationId);

    });

    $(document).on('click', '#delete_notification', function(event) {
        event.preventDefault();
        let notificationId = $(this).val()
        if (notificationId) {
            Swal.fire({
                icon: 'error',
                title: 'Are you sure?',
                text: 'Deleting notification cannot be undone',
                showCancelButton: true,
                confirmButtonText: 'Delete',
                cancelButtonText: 'No',
                showLoaderOnConfirm: true,
                backdrop: false,
                preConfirm: () => {
                    return $.ajax({
                        url: '../Src/ajax/notification_data.php',
                        type: "POST",
                        data: { 'notificationId': notificationId, 'delete': true },
                        dataType: 'json',
                        success: function(json) {
                            if (json.status) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Notification deleted',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    onClose: () => {
                                        location.replace(app_url + '/Views/notification.php');
                                    }
                                })

                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Something went wrong',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    onClose: () => {
                                        location.replace(app_url + '/Views/notification.php');
                                    }
                                })

                            }
                        }
                    })
                },
                allowOutsideClick: () => !Swal.isLoading(),
            })
        }

    });

    $(document).on('click', '#resend_notification', function(event) {
        event.preventDefault();
        let notificationId = $(this).val()
        if (notificationId) {
            Swal.fire({
                title: 'Want to send notification ?',
                showCancelButton: true,
                confirmButtonText: 'Send',
                cancelButtonText: 'No',
                showLoaderOnConfirm: true,
                backdrop: false,
                preConfirm: () => {
                    return $.ajax({
                        url: '../Src/ajax/notification_data.php',
                        type: "POST",
                        data: { 'notificationId': notificationId, 'send': true },
                        dataType: 'json',
                        success: function(json) {
                            if (json.status) {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Notification sent',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    onClose: () => {
                                        location.replace(app_url + '/Views/notification.php');
                                    }
                                })

                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Something went wrong',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    onClose: () => {
                                        location.replace(app_url + '/Views/notification.php');
                                    }
                                })

                            }
                        }
                    })
                },
                allowOutsideClick: () => !Swal.isLoading(),
            })
        }
    });

});