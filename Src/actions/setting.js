$(document).ready(function(e) {

    var app_url = null;
    $.getJSON("../env.json", function(json) {
        app_url = json.APP_URL
    });

    $("input").each(function() {
        $(this).attr("autocomplete", "off");
    });

    $(document).on('click', '#avatar_preview', function(event) {
        event.preventDefault();
        $('#avatar').trigger('click');
    });

    $(document).on('click', '#icon_preview', function(event) {
        event.preventDefault();
        $('#icon').trigger('click');
    });

    var _URL = window.URL || window.webkitURL;
    var file, img;
    $("#avatar").change(function(e) {
        if ((file = this.files[0])) {
            $('#avatar-error').html('');

            uploadFile(file);
            $('#avatar_preview').attr({
                src: _URL.createObjectURL(file)
            });
            img = new Image();
            img.src = _URL.createObjectURL(file);

        }
    });

    var file2, img2;
    $("#icon").change(function(e) {
        if ((file2 = this.files[0])) {
            $('#icon-error').html('');

            uploadIcon(file2);
            $('#icon_preview').attr({
                src: _URL.createObjectURL(file2)
            });
            img2 = new Image();
            img2.src = _URL.createObjectURL(file2);

        }
    });


    function uploadFile(file) {

        var fd = new FormData();
        fd.append('avatar', file);
        fd.append('uploadFile', true);

        $.ajax({
            url: '../Src/ajax/setting_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $('#avatar_hidden').val(data.path);
                } else {
                    $('#avatar-error').html(data.message);
                }
            },
            error: function(jqxhr, status, msg) {
                //error code
            }
        });
    }

    function uploadIcon(file) {

        var fd = new FormData();
        fd.append('icon', file);
        fd.append('uploadIcon', true);

        $.ajax({
            url: '../Src/ajax/setting_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $('#icon_hidden').val(data.path);
                } else {
                    $('#icon-error').html(data.message);
                }
            },
            error: function(jqxhr, status, msg) {
                //error code
            }
        });
    }
    load_settings();

    function load_settings() {
        $.ajax({
            url: '../Src/ajax/ad_data.php',
            type: 'POST',
            data: { 'adNames': true },
            dataType: 'json',
            async: false,
            success: function(data) {
                if (data.status) {
                    let ads = data.data
                    let options = ''
                    if (ads.length > 0) {
                        ads.forEach(ad => {
                            options += "<option value=" + ad.id + ">" + ad.id + " | " + ad.name + "</option>"

                        });
                        $("#ads").html(options)
                    }

                } else {
                    // $('#avatar-error').html(data.message);
                }
            },
            error: function(jqxhr, status, msg) {
                //error code
            }
        });

        $.ajax({
            url: '../Src/ajax/setting_data.php',
            type: 'POST',
            data: { 'settings': true },
            dataType: 'json',
            async: false,
            success: function(data) {
                if (data.status) {
                    $('.disableControl').addClass('disableAll');
                    let response = data.data
                    let dataArray = response.data
                    $('#avatar_preview').attr('src', "../" + response.avatar)
                    $("#avatar_hidden").val(response.avatar)
                    $("#full_name").val(response.name)
                    $("#email").val(response.email)
                    $("#server_token").val(dataArray.token)

                    if (dataArray.network) {
                        let network = dataArray.network
                        if (network.fbAd == 'true') {
                            $("#fbAd").prop("checked", true);
                        }
                        if (network.fbIntreastial == 'true') {
                            $("#fbIntreastial").prop("checked", true);
                        }
                        if (network.fbNative == 'true') {
                            $("#fbNative").prop("checked", true);
                        }
                        if (network.fbBanner == 'true') {
                            $("#fbBanner").prop("checked", true);
                        }
                        if (network.fbNativeBanner == 'true') {
                            $("#fbNativeBanner").prop("checked", true);
                        }
                        if (network.adMobAd == 'true') {
                            $("#adMobAd").prop("checked", true);
                        }
                        if (network.adMobIntreastial == 'true') {
                            $("#adMobIntreastial").prop("checked", true);
                        }
                        if (network.adMobBanner == 'true') {
                            $("#adMobBanner").prop("checked", true);
                        }
                        if (network.adMobNative == 'true') {
                            $("#adMobNative").prop("checked", true);
                        }
                        if (network.adMobRv == 'true') {
                            $("#adMobRv").prop("checked", true);
                        }
                        if (network.pageClicks) {
                            $("#pageClicks").val(network.pageClicks);
                        }
                        if (network.bannerView) {
                            $("#bannerView").val(network.bannerView);
                        }
                        if (network.nativeView) {
                            $("#nativeView").val(network.nativeView);
                        }
                    }

                    if (dataArray.update) {
                        let update = dataArray.update
                        if (update.updateStatus == 'true') {
                            $("#update_status").prop('checked', true)
                        }
                        if (update.versionCode) {
                            $("#version_code").val(update.versionCode)
                        }
                        if (update.whatsNew) {
                            $("#whats_new").val(update.whatsNew)
                        }
                    }
                    if (dataArray.country) {
                        let country = dataArray.country
                        if (country.icon) {
                            $("#icon_preview").attr('src', "../" + country.icon)
                            $("#icon_hidden").val(country.icon)
                        }
                        if (country.countryStatus == 'true') {
                            $("#country_status").prop('checked', true)
                        }
                        if (country.makerName) {
                            $("#maker_name").val(country.makerName)
                        }
                        if (country.countryMessage) {
                            $("#country_msg").val(country.countryMessage)
                        }
                    }
                    if (dataArray.ad) {
                        let adArray = dataArray.ad
                        let ads = adArray.ads.split(",");
                        if (adArray.showAd == 'true') {
                            $("#show_ad").prop('checked', true)
                        }
                        $("#number_of_click").val(adArray.numberOfClick)
                        $("#ads").val(ads)
                        $('#ads').trigger('change');
                    }
                } else {
                    // $('#avatar-error').html(data.message);
                }
            },
            error: function(jqxhr, status, msg) {
                //error code
            }
        });


    }

    function IsEmail(email) {
        var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (!regex.test(email)) {
            return false;
        } else {
            return true;
        }
    }

    function validateProfile(fd) {
        let passed = true
        if (!fd.name) {
            passed = false
            $('#full_name-error').html('Please fill name field')
        }
        if (fd.email) {
            if (!IsEmail(fd.email)) {
                passed = false
                $('#email-error').html('Invalid email address')
            }
        } else {
            passed = false
            $('#email-error').html('Please fill email field')
        }
        if (!fd.avatar) {
            passed = false
            $('#avatar-error').html('Please select profile picture')
        }
        return passed;
    }

    function clearProfileForm() {
        $('#full_name-error').html('')
        $('#avatar-error').html('')
    }

    $(document).on('click', '#update_profile', function(e) {
        e.preventDefault();
        let name = $('#full_name').val();
        let avatar = $('#avatar_hidden').val()
        let email = $('#email').val()
        let fd = new FormData();
        fd.append('name', name);
        fd.append('avatar', avatar);
        fd.append('email', email);
        fd.append('updateProfile', true);
        if (validateProfile({ name, avatar, email })) {
            clearProfileForm()
                // showSpinner()

            Swal.fire({
                title: 'Are you sure?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, update it!',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '../Src/ajax/setting_data.php',
                        type: 'POST',
                        processData: false,
                        contentType: false,
                        data: fd,
                        dataType: 'json',
                        success: function(data) {
                            // hideSpinner()
                            Swal.fire({
                                title: 'Updated!',
                                timer: 1000,
                                icon: 'success',
                                timerProgressBar: true,
                                backdrop: true
                            }).then((result) => {
                                updateSession()
                            })

                        },
                        error: function(jqxhr, status, msg) {
                            //error code
                        }
                    });
                }
            })
        }

    });

    function logout() {
        // alert(email+psw);
        $.ajax({
            type: "post",
            url: '../Src/ajax/auth.php',
            data: {
                logout: true
            },
            success: function(data) {
                if (data == 'logout_success') {
                    location.replace(app_url + '/login.php');
                }
            }
        });

    }

    function updateSession() {
        let fd = new FormData()
        fd.append('updateSession', true)
        $.ajax({
            url: '../Src/ajax/setting_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd,
            dataType: 'json',
            success: function(data) {
                // if (data.status) {
                location.reload(true);
                // } else {
                //     logout();
                // }
            },
            error: function(jqxhr, status, msg) {
                //error code
            }
        });
    }
    $(document).on('click', '#update_password', function(e) {
        e.preventDefault();
        let password = $('#password').val();
        let confirm_password = $('#confirm_password').val()
        let fd = new FormData();
        fd.append('password', password);
        fd.append('updatePassword', true);
        if (password && password == confirm_password) {
            $('#confirm_password').removeClass('has-error')
            $('#password').removeClass('has-error')
            $('#confirm_password-error').html('')
            $('#password-error').html('')
            Swal.fire({
                title: 'Are you sure?',
                text: "You want to update password ?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, update it!',
                customClass: {
                    content: 'text-danger'
                },
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '../Src/ajax/setting_data.php',
                        type: 'POST',
                        processData: false,
                        contentType: false,
                        data: fd,
                        dataType: 'json',
                        success: function(data) {
                            Swal.fire({
                                title: 'Updated!',
                                timer: 1000,
                                icon: 'success',
                                timerProgressBar: true,
                                backdrop: true
                            })
                        },
                        error: function(jqxhr, status, msg) {
                            //error code
                        }
                    });
                }
            })
        } else {

            if (!password) {
                $('#confirm_password').removeClass('has-error')
                $('#confirm_password-error').html('')
                $('#password').addClass('has-error')
                $('#password-error').html("Password can't be empty")
            } else {
                $('#password').removeClass('has-error')
                $('#password-error').html("")
                $('#confirm_password').addClass('has-error')
                $('#confirm_password-error').html('Password not matching')
            }

        }

    });

    $(document).on('click', '#update_network', function(e) {
        e.preventDefault();
        let fbAd = $('#fbAd').prop('checked')
        let fbIntreastial = $('#fbIntreastial').prop('checked')
        let fbBanner = $('#fbBanner').prop('checked')
        let fbNative = $('#fbNative').prop('checked')
        let fbNativeBanner = $('#fbNativeBanner').prop('checked')
        let adMobAd = $('#adMobAd').prop('checked')
        let adMobIntreastial = $('#adMobIntreastial').prop('checked')
        let adMobBanner = $('#adMobBanner').prop('checked')
        let adMobNative = $('#adMobNative').prop('checked')
        let adMobRv = $('#adMobRv').prop('checked')
        let pageClicks = $('#pageClicks').val()
        let bannerView = $('#bannerView').val()
        let nativeView = $('#nativeView').val()


        let fd = new FormData();
        fd.append('fbAd', fbAd);
        fd.append('fbIntreastial', fbIntreastial);
        fd.append('fbBanner', fbBanner);
        fd.append('fbNative', fbNative);
        fd.append('fbNativeBanner', fbNativeBanner);
        fd.append('adMobAd', adMobAd);
        fd.append('adMobIntreastial', adMobIntreastial);
        fd.append('adMobBanner', adMobBanner);
        fd.append('adMobNative', adMobNative);
        fd.append('adMobRv', adMobRv);
        fd.append('pageClicks', pageClicks);
        fd.append('bannerView', bannerView);
        fd.append('nativeView', nativeView);
        fd.append('updateNetwork', true);

        Swal.fire({
            title: 'Are you sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, update it!',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '../Src/ajax/setting_data.php',
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: fd,
                    dataType: 'json',
                    success: function(data) {
                        load_settings()
                        Swal.fire({
                            title: 'Updated!',
                            icon: 'success',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    error: function(jqxhr, status, msg) {
                        //error code
                    }
                });
            }
        })


    });
    $(document).on('click', '#update_token', function(e) {
        e.preventDefault();

        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, update it!'
        }).then((result) => {
            if (result.value) {
                let server_token = $('#server_token').val()
                let fd = new FormData();
                fd.append('token', server_token);
                fd.append('updateToken', true);
                $.ajax({
                    url: '../Src/ajax/setting_data.php',
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: fd,
                    dataType: 'json',
                    success: function(data) {
                        load_settings()
                        Swal.fire({
                            title: 'Updated!',
                            text: 'Your token has been updated.',
                            icon: 'success',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    error: function(jqxhr, status, msg) {
                        //error code
                    }
                });


            }
        })



    });

    $(document).on('click', '#update_details', function(e) {
        e.preventDefault();
        let updateStatus = $('#update_status').prop('checked')
        let versionCode = $('#version_code').val()
        let whatsNew = $('#whats_new').val()

        let fd = new FormData();
        fd.append('updateStatus', updateStatus);
        fd.append('versionCode', versionCode);
        fd.append('whatsNew', whatsNew);
        fd.append('updateDetails', true);

        Swal.fire({
            title: 'Are you sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, update it!',
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    url: '../Src/ajax/setting_data.php',
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: fd,
                    dataType: 'json',
                    success: function(data) {
                        load_settings()
                        Swal.fire({
                            title: 'Updated!',
                            icon: 'success',
                            showConfirmButton: false,
                            timer: 1500
                        })
                    },
                    error: function(jqxhr, status, msg) {
                        //error code
                    }
                });

            }
        })



    });

    $(document).on('click', '#update_country', function(e) {
        e.preventDefault();
        let makerName = $('#maker_name').val();
        let icon = $('#icon_hidden').val()
        let countryStatus = $('#country_status').prop('checked')
        let countryMessage = $('#country_msg').val()
        let fd = new FormData();
        fd.append('makerName', makerName);
        fd.append('icon', icon);
        fd.append('countryMessage', countryMessage);
        fd.append('countryStatus', countryStatus);
        fd.append('updateCountry', true);
        // if (validateProfile({ name, avatar, email })) {
        // clearProfileForm()
        // showSpinner()

        Swal.fire({
                title: 'Are you sure?',
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, update it!',
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url: '../Src/ajax/setting_data.php',
                        type: 'POST',
                        processData: false,
                        contentType: false,
                        data: fd,
                        dataType: 'json',
                        success: function(data) {
                            // hideSpinner()
                            Swal.fire({
                                title: 'Updated!',
                                timer: 1000,
                                icon: 'success',
                                timerProgressBar: true,
                                backdrop: true
                            }).then((result) => {
                                load_settings()
                            })

                        },
                        error: function(jqxhr, status, msg) {
                            //error code
                        }
                    });
                }
            })
            // }

    });

    $(document).on('click', '#update_ad', function(e) {
        e.preventDefault();
        let ads = $('#ads').val();
        let showAd = $('#show_ad').prop('checked')
        let numberOfClick = $('#number_of_click').val()
        let fd = new FormData();
        fd.append('ads', ads);
        fd.append('showAd', showAd);
        fd.append('numberOfClick', numberOfClick);
        fd.append('updateAd', true);
        Swal.fire({
            title: 'Are you sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, update it!',
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: '../Src/ajax/setting_data.php',
                    type: 'POST',
                    processData: false,
                    contentType: false,
                    data: fd,
                    dataType: 'json',
                    success: function(data) {
                        // hideSpinner()
                        Swal.fire({
                            title: 'Updated!',
                            timer: 1000,
                            icon: 'success',
                            timerProgressBar: true,
                            backdrop: true
                        }).then((result) => {
                            load_settings()
                        })

                    },
                    error: function(jqxhr, status, msg) {
                        //error code
                    }
                });
            }
        })

    });
});