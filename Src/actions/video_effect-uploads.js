$(document).ready(function() {

    $(document).on('click', '#image_preview', function(event) {
        event.preventDefault();
        $('#cover_image').trigger('click');
    });


    $(document).on('click', '#remove_cover_image', function(event) {
        event.preventDefault();
        $('#delete_file_id').val('cover_image')
        $('#deleteFileModal').modal('toggle')

    });

    var clearCoverImageComponents = function() {
        $('#cover_image_hidden').val('');
        $('#cover_image').val('')
        $('#image_preview').attr('src', '../Src/dist/img/add-plus-2.png')
        $('.remove_cover_image').addClass('hideMe')
    }

    var _URL = window.URL || window.webkitURL;

    $("#cover_image").change(function(e) {

        var file, img
        $('#cover_image-error').html('');

        if (file = this.files[0]) {
            uploadFile(file);
            $('#image_preview').attr({
                src: _URL.createObjectURL(file)
            });
            img = new Image();

            img.src = _URL.createObjectURL(file);

        }
    });


    function uploadFile(file) {

        var fd = new FormData();
        fd.append('coverImage', file);
        fd.append('uploadCoverImage', true);

        $.ajax({
            url: '../Src/ajax/video_effect_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $('#cover_image_hidden').val(data.path);
                    $('.remove_cover_image').removeClass('hideMe')
                } else {
                    alert('Cannot upload file.');
                }
            },
            error: function(jqxhr, status, msg) {
                //error code
            }
        });
    }


    var deleteFile = function(file, fieldName, cb) {

        var fd = new FormData();
        fd.append('filePath', file);
        fd.append('fieldName', fieldName);
        fd.append('deleteFile', true);
        var status = false
        $.ajax({
            url: '../Src/ajax/video_effect_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    cb()

                }
            },
            error: function(jqxhr, status, msg) {}
        });


        return status
    }

    /** ------------------------------   Upload Video -----------------------------------------     */

    // on image click trigger video input click event
    $(document).on('click', '#video_image', function(event) {
        event.preventDefault();
        $('#video').trigger('click');
    });

    //  on link click open ekkoLightBox
    $(document).on('click', '[data-toggle="lightboxVideo"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    $(document).on('click', '#remove_video', function(event) {
        event.preventDefault();
        $('#delete_file_id').val('video')
        $('#deleteFileModal').modal('toggle')

    });

    var clearVideoComponents = function() {
        $('#video_hidden').val('');
        $('#video').val('')
        $('#video_image').attr('src', '../Src/dist/img/no-image-3.png')
        $('.remove_video').addClass('hideMe')
        $('.main_vid').attr('href', '')
        $('.main_vid').addClass('hideMe')
    }

    $("#video").change(function(e) {

        $('#video-error').html('');
        // $('#video_hidden').siblings('.error').remove();
        if (this.files[0]) {
            uploadvideo_one(this.files[0]);
        }
    });



    function uploadvideo_one(file) {

        var fd = new FormData();
        fd.append('video', file);
        fd.append('uploadVideo', true);
        $('.video-spinner').removeClass('hideMe')
        $.ajax({
            url: '../Src/ajax/video_effect_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            dataType: 'json',
            data: fd,
            success: function(json, status) {
                if (json.status) {
                    $('#video_hidden').val(json.path);
                    renderMedia("../" + json.path);
                    $('.remove_video').removeClass('hideMe');
                    $('#submitVideoEffect').prop('disabled', false);

                } else {
                    alert('Cannot upload file.');
                }

                $('.video-spinner').addClass('hideMe')
            },
            error: function(jqxhr, status, msg) {
                //error code
            }
        });
    }


    function renderMedia(file) {

        getFile(file, function(url) {
            //console.log('url', url); return false;
            frm = $('#videoEffectForm');

            // frm.find("#video_hidden").val(file);
            // frm.find(".main_vid").addClass("hideMe");    

            frm.find(".main_vid").attr("href", url).removeClass("hideMe");

            $("[data-s3='1']").on('load', function() {}).attr("src", "../Src/dist/img/edit-image.png");

        });
    }

    function getFile(filename, cb) {

        cb(filename);
    }


    /** ----------------------------------------- Upload viedo -end ----------------------------------------------------------------- */

    /** ---------------------------------------   Upload zip ---------------------------------------------------------------------- */

    $(document).on('click', '[data-toggle="lightboxZip"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

    $(document).on('click', '#zip_image', function(event) {
        event.preventDefault();
        $('#zip').trigger('click');
    });

    var clearZipComponents = function() {

        $('#zip_hidden').val('');
        $('#zip_success').html('');
        $('#zip').val('')
        $('#zip_image').attr('src', '../Src/dist/img/zip.png')
        $('.remove_zip').addClass('hideMe')

    }

    $(document).on('click', '#remove_zip', function(e) {
        e.preventDefault()
        $('#delete_file_id').val('zip')
        $('#deleteFileModal').modal('toggle')
    })

    $(document).on('click', '#delete_zip', function(event) {
        event.preventDefault();
        let filePath = fieldName = null
        let cb = function() {}
        let fileId = $('#delete_file_id').val()
        if (fileId == 'zip') {
            filePath = $("#zip_hidden").val()
            cb = clearZipComponents
            fieldName = 'zip';
        } else if (fileId == 'video') {
            filePath = $("#video_hidden").val()
            fieldName = 'video';
            cb = clearVideoComponents
        } else if (fileId == 'cover_image') {
            filePath = $("#cover_image_hidden").val()
            cb = clearCoverImageComponents
            fieldName = 'cover_image';
        }

        if (filePath != null) {
            deleteFile(filePath, fieldName, cb)
        }

    });

    $("#zip").change(function(e) {
        var file
        $('#zip-error').html('');
        if ((file = this.files[0])) {
            uploadZip(file);
            $('#zip-error').html('');

        }
    });

    function uploadZip(file) {

        var fd = new FormData();
        fd.append('zip', file);
        fd.append('uploadZip', true);
        $('.zip-spinner').removeClass('hideMe')
        $.ajax({
            url: '../Src/ajax/video_effect_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $('#zip_hidden').val(data.path);
                    $('#zip_success').html(file.name);
                    $('.remove_zip').removeClass('hideMe')
                } else {
                    alert('Cannot upload zip file.');
                }
                $('.zip-spinner').addClass('hideMe')
            },
            error: function(jqxhr, status, msg) {
                //error code
                console.log(jqxhr)
            }
        });
    }


});