$(document).ready(function(e) {

    var app_url = null;
    $.getJSON("../env.json", function(json) {
        app_url = json.APP_URL
    });



    table = $('#video_effects_table').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "pageLength": 10,
        "searching": true,
        "draw": false,
        "order": [
            [0, "desc"]
        ],
        "ajax": {
            url: '../Src/ajax/data_tables/load_video_effects.php', // json
            type: "post", // type of method
            error: function(error) {
                console.log(error)
            }
        },
        'columns': [{
                data: 'id',
                name: "Id",
                orderable: true,
                searchable: true
            },
            {
                name: "Category Name",
                data: 'category_name',
                orderable: true,
                searchable: true
            },
            {
                name: "Title",
                data: 'title',
                orderable: true,
                searchable: true
            },
            {
                name: "Cover Image",
                data: 'cover_image',
                orderable: false,
                searchable: false,
                render: function(data, type, row) {
                    return "<img src='../" + data + "' style='max-height:50px'>"
                }
            },
            {
                data: "Action",
                orderable: false,
                searchable: false,
                render: function(data, type, row) {
                    return "<button  class='btn_view btn btn-sm btn-primary' id='view_video_effect' value='" + row.id + "'><i class='fas fa-edit'></i></button><button  class='btn_delete btn btn-sm btn-danger' id='delete_video_effect' value='" + row.id + "'><i class='fas fa-trash'></i></button>"
                }
            }
        ],
        initComplete: function() {
            var input = $('.dataTables_filter input').unbind(),
                self = this.api(),
                $searchButton = $('<button>')
                .text('search')
                .addClass('btn btn-success btn-sm mx-2')
                .click(function() {
                    self.search(input.val()).draw();
                }),
                $clearButton = $('<button>')
                .text('clear')
                .addClass('btn btn-danger btn-sm')
                .click(function() {
                    input.val('');
                    $searchButton.click();
                })
            $('.dataTables_filter').append($searchButton, $clearButton);
        }
    })



        $('#submitVideoEffect').on("submit",function (e){
            e.preventDefault();
            console.log('aaa');
            var formdata = new FormData(this);
            $.ajax({
                url: '../Src/ajax/video_effect_data.php', // json
                method:'POST',
                data:formdata,
                processData: false,
                contentType: false,
                cache: false,
                dataType: 'json',
                success:function (data){
                    let category = $("#category").val();
                    let title = $("#title").val();
                    let video = $("#video").val();
                    let cover_image = $("#cover_image").val();
                    let zip = $("#zip").val();
                    $('#insertmyModal').modal("show");
                    setTimeout(function (){
                        $('#insertmyModal').modal("hide");
                    },2000);
                    // setInterval(function(){
                    // $('#category_table').DataTable().ajax.reload(null, false);
                    // }, 3000);
                }
            });
        });




    $(document).on("click", "#view_video_effect", function(e) {
        e.preventDefault();
        let videoEffectId = $(this).attr('value');
        location.replace(app_url + '/Views/manage_video_effect.php?key=' + videoEffectId);

    });

    $(document).on('click', '#delete_video_effect', function(event) {
        event.preventDefault();
        let deleteId = $(this).val()
        console.log(deleteId)
        $('#delete_id').val(deleteId)
        $('#deleteModal').modal('toggle')

    });


    $(document).on("click", "#deleteVideoEffect", function(e) {

        e.preventDefault();
        let videoEffectId = $('#delete_id').val()
        if (videoEffectId) {
            $.ajax({
                type: "POST",
                url: "../Src/ajax/video_effect_data.php",
                data: { 'videoEffectId': videoEffectId, 'delete': true },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        location.replace(app_url + '/Views/video_effect.php');
                    }

                }
            });
        }

    });
});