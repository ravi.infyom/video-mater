$(document).ready(function() {


    $(document).on('click', '#image_preview', function(event) {
        event.preventDefault();
        $('#cover_image').trigger('click');
    });


    $(document).on('click', '#remove_cover_image', function(event) {
        event.preventDefault();
        $('#delete_file_id').val('cover_image')
        $('#deleteFileModal').modal('toggle')

    });

    var clearCoverImageComponents = function() {
        $('#cover_image_hidden').val('');
        $('#cover_image').val('')
        $('#image_preview').attr('src', '../Src/dist/img/add-plus-2.png')
        $('.remove_cover_image').addClass('hideMe')
    }

    var _URL = window.URL || window.webkitURL;
    var file, img;

    $("#cover_image").change(function(e) {

        $('#cover_image-error').html('');

        if ((file = this.files[0])) {
            uploadFile(file);
            $('#image_preview').attr({
                src: _URL.createObjectURL(file)
            });
            img = new Image();

            img.src = _URL.createObjectURL(file);

        }
    });


    function uploadFile(file) {

        var fd = new FormData();
        fd.append('coverImage', file);
        fd.append('uploadCoverImage', true);

        $.ajax({
            url: '../Src/ajax/video_ready_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    $('#cover_image_hidden').val(data.path);
                    $('.remove_cover_image').removeClass('hideMe')
                } else {
                    alert('Cannot upload file.');
                }
            },
            error: function(jqxhr, status, msg) {
                //error code
            }
        });
    }

    var deleteFile = function(file, fieldName, cb) {
        var fd = new FormData();
        fd.append('fieldName', fieldName);
        fd.append('filePath', file);
        fd.append('deleteFile', true);
        var status = false
        $.ajax({
            url: '../Src/ajax/video_ready_data.php',
            type: 'POST',
            processData: false,
            contentType: false,
            data: fd,
            dataType: 'json',
            success: function(data) {
                if (data.status) {
                    cb()

                }
            },
            error: function(jqxhr, status, msg) {}
        });


        return status
    }


    $(document).on('click', '#delete_file', function(event) {
        event.preventDefault();
        let file = fieldName = null
        let cb = function() {}
        let fileId = $('#delete_file_id').val()
        console.log(fileId)
        if (fileId == 'video') {
            file = $("#video_hidden").val()
            cb = clearVideoComponents
            fieldName = 'video'
        } else if (fileId == 'cover_image') {
            file = $("#cover_image_hidden").val()
            cb = clearCoverImageComponents
            fieldName = 'cover_image'
        }

        if (file != null) {
            deleteFile(file, fieldName, cb)

        }

    });

});


/** ------------------------------   Upload Video -----------------------------------------     */

// on image click trigger video input click event
$(document).on('click', '#video_image', function(event) {
    event.preventDefault();
    $('#video').trigger('click');
});

//  on link click open ekkoLightBox
$(document).on('click', '[data-toggle="lightboxVideo"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});

$(document).on('click', '#remove_video', function(event) {
    event.preventDefault();
    $('#delete_file_id').val('video')
    $('#deleteFileModal').modal('toggle')

});

var clearVideoComponents = function() {
    $('#video_hidden').val('');
    $('#video').val('')
    $('#video_image').attr('src', '../Src/dist/img/no-image-3.png')
    $('.remove_video').addClass('hideMe')
    $('.progressVideo').addClass('hideMe')
    $('#progressVideo').html('0%')
    $('.video').attr('href', '')
    $('.video').addClass('hideMe')
}

$("#video").change(function(e) {

    $('#video-error').html('');
    // $('#video_hidden').siblings('.error').remove();
    if ((file = this.files[0])) {
        // $('.progressVideo').removeClass("hideMe");
        uploadvideo(file);
        // $('.progressVideo').removeClass("hideMe");
    }
});



function uploadvideo(file) {

    var fd = new FormData();
    fd.append('video', file);
    fd.append('uploadVideo', true);
    $('.video-spinner').removeClass('hideMe')
    $.ajax({
        url: '../Src/ajax/video_ready_data.php',
        type: 'POST',
        processData: false,
        contentType: false,
        dataType: 'json',
        data: fd,
        success: function(json, status) {
            if (json.status) {
                $('#video_hidden').val(json.path);
                renderMedia("../" + json.path);
                $('.remove_video').removeClass('hideMe');
                $('.video').removeClass('hideMe');
                $('#submitVideoReady').prop('disabled', false);
                $('.video-spinner').addClass('hideMe')

            } else {
                alert('Cannot upload file.');
            }


        },
        error: function(jqxhr, status, msg) {
            //error code
        }
    });
}


function renderMedia(file) {
    getFile(file, function(url) {
        frm = $('#videoReadyForm');
        frm.find(".video").attr("href", url).removeClass("hideMe");

        $("[data-s3='1']").on('load', function() {
            $('.progress').addClass("hideMe");
        }).attr("src", "../Src/dist/img/edit-image.png");

    });
}

function getFile(filename, cb) {

    cb(filename);
}


/** ----------------------------------------- Upload viedo -end ----------------------------------------------------------------- */