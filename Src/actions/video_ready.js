$(document).ready(function(e) {

    var app_url = null;
    $.getJSON("../env.json", function(json) {
        app_url = json.APP_URL
    });

    table = $('#video_ready_table').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "pageLength": 10,
        "searching": true,
        "draw": false,
        "order": [
            [0, "desc"]
        ],
        "ajax": {
            url: '../Src/ajax/data_tables/load_video_ready.php', // json
            type: "post", // type of method
            error: function(error) {
                console.log(error)
            }
        },
        'columns': [{
                data: 'id',
                name: "Id",
                orderable: true,
                searchable: true
            },
            {
                name: "Category Name",
                data: 'category_name',
                orderable: true,
                searchable: true
            },
            {
                name: "Title",
                data: 'title',
                orderable: true,
                searchable: true
            },
            {
                name: "Cover Image",
                data: 'cover_image',
                orderable: false,
                searchable: false,
                render: function(data, type, row) {
                    return "<img src='../" + data + "' style='max-height:50px'>"
                }
            },
            {
                data: "Action",
                orderable: false,
                searchable: false,
                render: function(data, type, row) {
                    return "<button  class='btn_view btn btn-sm btn-primary' id='view_video_ready' value='" + row.id + "'><i class='fas fa-edit'></i></button><button  class='btn_delete btn btn-sm btn-danger' id='delete_video_ready' value='" + row.id + "'><i class='fas fa-trash'></i></button>"
                }
            }
        ],
        initComplete: function() {
            var input = $('.dataTables_filter input').unbind(),
                self = this.api(),
                $searchButton = $('<button>')
                .text('search')
                .addClass('btn btn-success btn-sm mx-2')
                .click(function() {
                    self.search(input.val()).draw();
                }),
                $clearButton = $('<button>')
                .text('clear')
                .addClass('btn btn-danger btn-sm')
                .click(function() {
                    input.val('');
                    $searchButton.click();
                })
            $('.dataTables_filter').append($searchButton, $clearButton);
        }
    })


    $(document).on("click", "#view_video_ready", function(e) {
        e.preventDefault();
        let videoReadyId = $(this).attr('value');
        location.replace(app_url + '/Views/manage_video_ready.php?key=' + videoReadyId);

    });

    $(document).on('click', '#delete_video_ready', function(event) {
        event.preventDefault();
        let deleteId = $(this).val()
        console.log(deleteId)
        $('#delete_id').val(deleteId)
        $('#deleteModal').modal('toggle')

    });


    $(document).on("click", "#deleteVideoReady", function(e) {

        e.preventDefault();
        let videoReadyId = $('#delete_id').val()
        if (videoReadyId) {
            $.ajax({
                type: "POST",
                url: "../Src/ajax/video_ready_data.php",
                data: { 'videoReadyId': videoReadyId, 'delete': true },
                dataType: 'json',
                success: function(data) {
                    if (data.status == 1) {
                        location.replace(app_url + '/Views/video_ready.php');
                    }

                }
            });
        }

    });
});