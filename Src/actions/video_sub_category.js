$(document).ready(function(e) {
    var app_url = null;
    $.getJSON("../env.json", function(json) {
        app_url = json.APP_URL
    });

    table = $('#subcategory_table').DataTable({
        "bProcessing": true,
        "serverSide": true,
        "pageLength": 10,
        "searching": true,
        "draw": false,
        "order": [
            [2, "asc"]
        ],
        "ajax": {
            url: '../Src/ajax/data_tables/load_video_sub_categories.php', // json
            type: "post", // type of method
            error: function() {}
        },
        'columns': [{
                data: 'id',
                name: "Id",
                orderable: true,
                searchable: true
            },
            {
                name: "category_name",
                data: 'category_name',
                orderable: true,
                searchable: true
            },
            {
                name: "name",
                data: 'name',
                orderable: true,
                searchable: true
            },
            {
                name: "Icon",
                data: 'icon',
                orderable: false,
                searchable: false,
                render: function(data) {
                    return data ? "<img src='../" + data + "' width='50px'/>" : 'Unavailable';
                    return data ;
                }
            },
            {
                data: "Action",
                orderable: false,
                searchable: false,
                render: function(data, type, row) {
                    return "<button  class='btn_view btn btn-sm btn-primary' id='btn_view' value='" + row.id + "'><i class='fas fa-edit'></i></button><button  class='btn_delete btn btn-sm btn-danger' id='delete_category' value='" + row.id + "'><i class='fas fa-trash'></i></button>"
                }
            }
        ],
        initComplete: function() {
            var input = $('.dataTables_filter input').unbind(),
                self = this.api(),
                $searchButton = $('<button>')
                .text('search')
                .addClass('btn btn-success btn-sm mx-2')
                .click(function() {
                    self.search(input.val()).draw();
                }),
                $clearButton = $('<button>')
                .text('clear')
                .addClass('btn btn-danger btn-sm')
                .click(function() {
                    input.val('');
                    $searchButton.click();
                })
            $('.dataTables_filter').append($searchButton, $clearButton);
        }
    })



    $(document).on("click", "#btn_view", function(e) {
        e.preventDefault();
        let categoryId = $(this).attr('value');
        location.replace(app_url + '/Views/manage_sub_video_category.php?key=' + categoryId);

    });

    $(document).on('click', '#delete_category', function(event) {
        event.preventDefault();
        let deleteId = $(this).val()
        $('#delete_id').val(deleteId)
        $('#deleteModal').modal('toggle')

    });


    $(document).on("click", "#deleteCategory", function(e) {

        e.preventDefault();
        let categoryId = $('#delete_id').val()
        if (categoryId) {
            $.ajax({
                type: "POST",
                url: "../Src/ajax/video_sub_category_data.php",
                data: { 'categoryId': categoryId, 'delete': true },
                dataType: 'json',
                success: function(data) {
                    if (data.status == true) {
                        $('#subcategory_table').DataTable().ajax.reload();
                    } else {
                        Swal.fire({
                            title: 'Error!',
                            text: data.message ? data.message : 'Something went wrong',
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 6000,
                            customClass: {
                                container: 'bg-secondary',
                                title: 'text-danger',
                                content: 'text-danger'
                            }
                        })
                    }

                }
            });
        }

    });

});