<?php
list($rootPath, $srcPath) = ['../..', '../../Src'];
require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

use Src\Models\Ad;

$dbConnection = (new \Src\System\DatabaseConnector())->getConnection();

$adObject = new Ad($dbConnection);


if (isset($_POST['adName']) && isset($_POST['adSlogan']) && isset($_POST['companyName'])  && isset($_POST['adType']) && isset($_POST['packageName']) && isset($_FILES['adIcon']) && isset($_POST['redirectUrl'])) {
    $adName = $_POST['adName'];
    $adSlogan = $_POST['adSlogan'];
    $companyName = $_POST['companyName'];
    $adType = $_POST['adType'];
    $packageName = $_POST['packageName'];
    $adIcon = $_FILES['adIcon']['name'];
    $redirectUrl = $_POST['redirectUrl'];

    $insert = $adObject->store(compact('adName', 'adSlogan', 'companyName','adType', 'packageName', 'adIcon','redirectUrl' ));
    if ($insert) {
        echo json_encode(['status' => 1, 'message' => 'success', 'id' => $insert]);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "can't add custom ad"]);
    return;
}
else if(isset($_POST['id'])){
    $updateId = $_POST['id'];
        $ad = $adObject->getAdById($updateId);
    echo json_encode($ad);
}
else if (isset($_POST['editAdName']) && isset($_POST['editAdSlogan']) && isset($_POST['editCompanyName'])  && isset($_POST['editAdType']) && isset($_POST['editPackageName']) && isset($_FILES['editAdIcon']) && isset($_POST['editRedirectUrl'])) {

    $adName = $_POST['editAdName'];
    $adSlogan = $_POST['editAdSlogan'];
    $companyName = $_POST['editCompanyName'];
    $adType = $_POST['editAdType'];
    $packageName = $_POST['editPackageName'];
    $adIcon = $_FILES['editAdIcon']['name'];
    $redirectUrl = $_POST['editRedirectUrl'];
    $id = $_POST['categoryId'];

    $ad = $adObject->getAdById($id);
//    $oldIcon = $ad['editAdIcon'];
    if (!$ad) {
        echo json_encode(['status' => 0, 'error' => "Ad does not exist you are trying to updated", 'id' => $id]);
        return;
    }

    $data = compact('id', 'adName', 'adSlogan', 'companyName', 'adType', 'packageName', 'adIcon', 'redirectUrl');
    $updated = $adObject->update($data);
    if ($updated > 0) {

//        if ($oldIcon !== $adIcon && file_exists("../$oldIcon")) {
//            unlink("../$oldIcon");
//        }
        echo json_encode(['status' => 1, 'message' => 'success']);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "can't add audio."]);
    return;

} else {
    echo json_encode([['status' => 0, 'message' => "Invlid request data"]]);
}

//if (isset($_REQUEST['adName']) && isset($_REQUEST['adSlogan']) && isset($_REQUEST['companyName'])  && isset($_REQUEST['adType']) && isset($_REQUEST['packageName']) && isset($_REQUEST['adIcon']) && isset($_REQUEST['redirectUrl']) && isset($_REQUEST['insert'])) {
//
//    $adName = $_REQUEST['adName'];
//    $adSlogan = $_REQUEST['adSlogan'];
//    $companyName = $_REQUEST['companyName'];
//    $adType = $_REQUEST['adType'];
//    $packageName = $_REQUEST['packageName'];
//    $adIcon = $_REQUEST['adIcon'];
//    $redirectUrl = $_REQUEST['redirectUrl'];
//
//    $insert = $adObject->store(compact('adName', 'adSlogan', 'companyName','adType', 'packageName', 'adIcon','redirectUrl' ));
//    if ($insert) {
//        echo json_encode(['status' => 1, 'message' => 'success', 'id' => $insert]);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "can't add custom ad"]);
//    return;
//}
//else if (isset($_REQUEST['adName']) && isset($_REQUEST['adSlogan']) && isset($_REQUEST['companyName'])  && isset($_REQUEST['adType']) && isset($_REQUEST['packageName']) && isset($_REQUEST['adIcon']) && isset($_REQUEST['redirectUrl']) && isset($_REQUEST['adId']) && isset($_REQUEST['update'])) {
//
//
//    $id = $_REQUEST['adId'];
//    $ad = $adObject->getAdById($id);
//    $oldIcon = $ad['ad_icon'];
//    if (!$ad) {
//        echo json_encode(['status' => 0, 'error' => "Ad does not exist you are trying to updated", 'id' => $id]);
//        return;
//    }
//    $adName = $_REQUEST['adName'];
//    $adSlogan = $_REQUEST['adSlogan'];
//    $companyName = $_REQUEST['companyName'];
//    $adType = $_REQUEST['adType'];
//    $packageName = $_REQUEST['packageName'];
//    $adIcon = $_REQUEST['adIcon'];
//    $redirectUrl = $_REQUEST['redirectUrl'];
//
//    $data = compact('id', 'adName', 'adSlogan', 'companyName', 'adType', 'packageName', 'adIcon', 'redirectUrl');
//    $updated = $adObject->update($data);
//    if ($updated > 0) {
//
//        if ($oldIcon !== $adIcon && file_exists("../$oldIcon")) {
//            unlink("../$oldIcon");
//        }
//        echo json_encode(['status' => 1, 'message' => 'success']);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "can't add audio."]);
//    return;
//}
//// delete ad
//else if (isset($_REQUEST['adId']) && isset($_REQUEST['delete'])) {
//    $id = $_REQUEST['adId'];
//    $res=$adObject->delete($id);
//    if($res)
//    {
//        echo json_encode(['status' => 1, 'message' => 'success']);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "can't delete notification"]);
//    return;
//}
//// get ad
//else if (isset($_REQUEST['adId']) && isset($_REQUEST['show'])) {
//
//    $adId = $_REQUEST['adId'];
//    $results = $adObject->getAdById($adId);
//    if ($results) {
//        echo json_encode(['id' => $results['id'], 'ad_name' => $results['ad_name'], 'ad_slogan' => $results['ad_slogan'], 'company_name' => $results['company_name'], 'ad_type' => $results['ad_type'], 'package_name' => $results['package_name'], 'ad_icon' => $results['ad_icon'], 'redirect_url' => $results['redirect_url']]);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "audio with given id not exist"]);
//    return;
//}
//else if (isset($_REQUEST['adNames'])) {
//
//    $results = $adObject->getAdNames();
//    if ($results) {
//        echo json_encode(['status' => 1, 'data' => $results]);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "audio with given id not exist"]);
//    return;
//}
//else if (isset($_FILES['adIcon']) && isset($_REQUEST['upload'])) {
//    $icon = $_FILES['adIcon']['name'];
//    $icon_tmp = $_FILES['adIcon']['tmp_name'];
//
//    $uniqueName = time() . '-ad-icon-' . $icon;
//    $iconPath = "Src/storage/ad/icon/$uniqueName";
//    $moved = move_uploaded_file($icon_tmp, "$rootPath/$iconPath");
//    if ($moved) {
//        echo json_encode(['status' => 1, 'path' => $iconPath]);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "unable to upload icon"]);
//    return;
//}
//
