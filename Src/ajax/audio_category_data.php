<?php
list($rootPath, $srcPath) = ['../..', '../../Src'];
require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

use Src\Models\AudioCategory;
use Src\Models\DeleteFile;

$dbConnection = (new \Src\System\DatabaseConnector())->getConnection();

$AudioCategory = new AudioCategory($dbConnection);
$df = new DeleteFile($dbConnection);


if (isset($_POST['name']) && isset($_FILES['icon']) && $_POST['status']) {
    $name = $_POST['name'];
    $icon = $_FILES['icon']['name'];
    $status = $_POST['status'];

    $data = $AudioCategory->getAudioCategoryByName($name);
    if (count($data) > 0) {
        echo json_encode(['status' => 0, 'error' => ['category' => 'Category with given name already exist']]);
        return;
    } else {
        $icon = $_FILES['icon']['name'];

        $tragetFile = "Src/storage/category/icon/";
        $path = $tragetFile . basename($_FILES['icon']['name']);
        move_uploaded_file($_FILES['icon']['tmp_name'] , $path);

        $insert = $AudioCategory->store(['name' => $name, 'icon' => $icon, 'status' => $status]);
        if ($insert) {
            echo json_encode(['status' => 1, 'message' => 'success']);
            return;
        }
        echo json_encode(['status' => 0, 'message' => "can't add category"]);
        return;
    }
} else if(isset($_POST['updateId'])){
    $updateId = $_POST['updateId'];
    $data = $AudioCategory->getAudioCategoryById($updateId);
    echo json_encode($data);
}
else if (isset($_POST['editCategoryName']) && isset($_FILES['editIcon']) && isset($_POST['editStatus']) && isset($_REQUEST['categoryId']) ) {

    $categoryId = $_POST['categoryId'];
    $categoryName = $_POST['editCategoryName'];
    $status = $_POST['editStatus'];

    $categoryObj = $AudioCategory->getAudioCategoryById($categoryId);
    $oldIcon = $categoryObj['icon'];
    if (!$categoryObj) {
        echo json_encode(['status' => 0, 'error' => "Category does not exist you are trying to update", 'id' => $categoryId]);
        return;
    }
    $data = $AudioCategory->getAudioCategoryByCutomFilter([['name', '=', $categoryName], ['id', '!=', $categoryId]]);
    if (count($data) > 0) {
        echo json_encode(['status' => 0, 'data' => $data, 'error' => ['category' => 'Category with given name already exist']]);
        return;
    }
    $icon = $_FILES['editIcon']['name'];

    $update = $AudioCategory->update(['categoryId' => $categoryId, 'name' => $categoryName, 'icon' => $icon, 'status' => $status]);
    if ($update >= 0) {
        if ($icon !== $oldIcon) {
            if (file_exists("../$oldIcon")) {
                unlink("../$oldIcon");
            }
        }
        echo json_encode(['status' => 1, 'message' => 'success']);
        return;
    }
    echo json_encode(['status' => 0, 'error' => "can't update category"]);
    return;
} else {
    echo json_encode([['status' => 0, 'message' => "Invlid request data"]]);
}



//if (isset($_FILES['icon']) && isset($_REQUEST['uploadIcon'])) {
//    $icon = $_FILES['icon']['name'];
//    $icon_tmp = $_FILES['icon']['tmp_name'];
//
//    $uniqueName = time() . '-icon-' . $icon;
//    $path = "Src/storage/audio_category/icon/$uniqueName";
//    $moved = move_uploaded_file($icon_tmp, "$rootPath/$path");
//    if ($moved) {
//        echo json_encode(['status' => 1, 'path' => $path]);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "unable to upload icon"]);
//    return;
//} else if (isset($_REQUEST['id'])) {
//
//    $results = $AudioCategory->getAudioCategoryByCutomFilter([['status', '=', 'active']]);
//    echo json_encode($results);
//} else if (isset($_REQUEST['filePath']) && isset($_REQUEST['fieldName']) && isset($_REQUEST['deleteFile'])) {
//    $file = $_REQUEST['filePath'];
//    $subjectName = $_REQUEST['fieldName'];
//    $df->addFile($file, 'audio_category', $subjectName);
//    echo json_encode(['status' => 1]);
//    return;
//} else if (isset($_REQUEST['audioCategoryNames'])) {
//
//    $results = $AudioCategory->getAudioCategoryNames([['status', '=', 'active']]);
//    echo json_encode(['status' => 1, 'data' => $results]);
//} else if (isset($_REQUEST['audioCategoryName']) && isset($_REQUEST['byName'])) {
//    $AudioCategoryName = $_REQUEST['audioCategoryName'];
//    $results = $AudioCategory->getAudioCategoryByName($AudioCategoryName);
//
//    echo json_encode(['status' => 1, 'data' => $results]);
//    return;
//}
//// add new category
//else if (isset($_REQUEST['audioCategoryName']) && isset($_REQUEST['status']) && isset($_REQUEST['icon']) && isset($_REQUEST['insert'])) {
//
//    $AudioCategoryName = $_REQUEST['audioCategoryName'];
//    $status = $_REQUEST['status'];
//
//    $data = $AudioCategory->getAudioCategoryByName($AudioCategoryName);
//    if (count($data) > 0) {
//        echo json_encode(['status' => 0, 'error' => ['category' => 'Category with given name already exist']]);
//        return;
//    } else {
//        $icon = $_REQUEST['icon'];
//
//        $insert = $AudioCategory->store(['name' => $AudioCategoryName, 'icon' => $icon, 'status' => $status]);
//        if ($insert) {
//            echo json_encode(['status' => 1, 'message' => 'success']);
//            return;
//        }
//        echo json_encode(['status' => 0, 'message' => "can't add category"]);
//        return;
//    }
//}
//// update category
//else if (isset($_REQUEST['audioCategoryName']) && isset($_REQUEST['status']) && isset($_REQUEST['icon']) && isset($_REQUEST['audioCategoryId'])&& isset($_REQUEST['priority']) && isset($_REQUEST['update'])) {
//
//    $AudioCategoryId = $_REQUEST['audioCategoryId'];
//    $AudioCategoryObj = $AudioCategory->getAudioCategoryById($AudioCategoryId);
//    $AudioCategoryName = $_REQUEST['audioCategoryName'];
//    $status = $_REQUEST['status'];
//    $oldIcon = $AudioCategoryObj['icon'];
//    if (!$AudioCategoryObj) {
//        echo json_encode(['status' => 0, 'error' => "Category does not exist you are trying to updated", 'id' => $AudioCategoryId]);
//        return;
//    }
//    $priority = $_REQUEST['priority'];
//
//    $data = $AudioCategory->getAudioCategoryByCutomFilter([['name', '=', $AudioCategoryName], ['id', '!=', $AudioCategoryId]]);
//    if (count($data) > 0) {
//        echo json_encode(['status' => 0, 'data' => $data, 'error' => ['category' => 'Category with given name already exist']]);
//        return;
//    }
//    $icon = $_REQUEST['icon'];
//
//    $update = $AudioCategory->update(['categoryId' => $AudioCategoryId, 'name' => $AudioCategoryName, 'icon' => $icon, 'status' => $status, 'priority' => $priority]);
//    if ($update >= 0) {
//        if ($icon !== $oldIcon) {
//            if (file_exists("../$oldIcon")) {
//                unlink("../$oldIcon");
//            }
//        }
//        echo json_encode(['status' => 1, 'message' => 'success']);
//        return;
//    }
//    echo json_encode(['status' => 0, 'error' => "can't update category"]);
//    return;
//}
//// delete category
//else if (isset($_REQUEST['audioCategoryId']) && isset($_REQUEST['delete'])) {
//
//    $AudioCategoryId = $_REQUEST['audioCategoryId'];
//    $isDeleteable = $AudioCategory->isLinkedToOther($AudioCategoryId);
//    if ($isDeleteable) {
//        $AudioCategoryObj = $AudioCategory->getAudioCategoryById($AudioCategoryId);
//        if ($AudioCategoryObj) {
//            $path = $AudioCategoryObj['icon'];
//            $delete = $AudioCategory->delete($AudioCategoryId);
//            if ($delete) {
//                if (file_exists("../$path")) {
//                    unlink("../$path");
//                }
//                echo json_encode(['status' => 1, 'message' => 'deleted']);
//                return;
//            }
//            echo json_encode(['status' => 0, 'message' => "unable to delete category"]);
//            return;
//        }
//        echo json_encode(['status' => 0, 'message' => "category with given id not exist"]);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "Can't delete, catgory linked with audios"]);
//    return;
//}
//// getAudio category
//else if (isset($_REQUEST['audioCategoryId']) && isset($_REQUEST['show'])) {
//
//    $AudioCategoryId = $_REQUEST['audioCategoryId'];
//
//    $results = $AudioCategory->getAudioCategoryById($AudioCategoryId);
//    if ($results) {
//        echo json_encode(['id' => $results['id'], 'name' => $results['name'], 'icon' => $results['icon'], 'status' => $results['status']]);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "category with given id not exist"]);
//    return;
//}
//else if (isset($_REQUEST['getCategoryCount'])) {
//
//    $results = $AudioCategory->getAudioCategoryCount();
//    if ($results) {
//        echo json_encode(['count' => $results]);
//        return;
//    }
//    echo json_encode(['count' => 20]);
//    return;
//}