<?php
list($rootPath, $srcPath) = ['../..', '../../Src'];
require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

use Src\Models\Audio;
use Src\Models\DeleteFile;

$dbConnection = (new \Src\System\DatabaseConnector())->getConnection();

$AudioModel = new Audio($dbConnection);
$df = new DeleteFile($dbConnection);



if (isset($_POST['name']) && isset($_FILES['audio']) && isset($_POST['audio_category_id'])) {
    $name = $_REQUEST['name'];
    $categoryId = $_REQUEST['audio_category_id'];
    $audio = $_FILES['audio']['name'];
    $data = compact('name', 'audio', 'categoryId');
    $inserted = $AudioModel->store($data);
    if ($inserted) {
        echo json_encode(['status' => 1, 'message' => 'success']);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "can't add audio"]);
    return;
}else if (isset($_POST['id'])) {
    $updateId = $_POST['id'];
    $results = $AudioModel->getAudioById($updateId);
    echo json_encode($results);
}
else if (isset($_POST['editName']) && isset($_FILES['editAudio']) && isset($_POST['edit_audio_category_id']) && isset($_POST['categoryId'])) {
    $id = $_POST['categoryId'];
    $audioObj = $AudioModel->getAudioById($id);
    $oldAudio = $audioObj['audio'];
    if (!$audioObj) {
        echo json_encode(['status' => 0, 'error' => "Audio does not exist you are trying to updated", 'id' => $id]);
        return;
    }
    $name = $_POST['editName'];
    $audio = $_FILES['editAudio']['name'];
    $categoryId = $_POST['edit_audio_category_id'];
    $data = compact('id', 'name', 'audio', 'categoryId');
    $updated = $AudioModel->update($data);
    if ($updated > 0) {

        if ($oldAudio !== $audio && file_exists("../$oldAudio")) {
            unlink("../$oldAudio");
        }
        echo json_encode(['status' => 1, 'message' => 'success']);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "can't add audio."]);
    return;
}


 //upload audio
//if (isset($_FILES['audio']) && isset($_REQUEST['uploadAudio'])) {
//    $audio = $_FILES['audio']['name'];
//    $audio_tmp = $_FILES['audio']['tmp_name'];
//    $uniqueName = time() . '-audio-' . $audio;
//    $audioPath = "Src/storage/audio/audio/$uniqueName";
//    $moved = move_uploaded_file($audio_tmp, "$rootPath/$audioPath");
//    if ($moved) {
//        echo json_encode(['status' => 1, 'path' => $audioPath]);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "unable to upload audio"]);
//    return;
//}
//// delete file
//else if (isset($_REQUEST['filePath']) && isset($_REQUEST['fieldName']) && isset($_REQUEST['deleteFile'])) {
//    $file = $_REQUEST['filePath'];
//    $subjectName = $_REQUEST['fieldName'];
//    $df->addFile($file, 'audio', $subjectName);
//    echo json_encode(['status' => 1]);
//    return;
//}
//// get all audios
//else if (isset($_REQUEST['id'])) {
//
//    $results = $AudioModel->getAudios();
//    echo json_encode($results);
//}
//// insert record
//else if (isset($_REQUEST['name']) && isset($_REQUEST['audio']) && isset($_REQUEST['audio_category_id']) && isset($_REQUEST['insert'])) {
//    $name = $_REQUEST['name'];
//    $categoryId = $_REQUEST['audio_category_id'];
//    $audio = $_REQUEST['audio'];
//    $data = compact('name', 'audio', 'categoryId');
//    $inserted = $AudioModel->store($data);
//    if ($inserted) {
//        echo json_encode(['status' => 1, 'message' => 'success']);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "can't add audio"]);
//    return;
//}
//// update record
//else if (isset($_REQUEST['name']) && isset($_REQUEST['audio']) && isset($_REQUEST['audio_category_id'])  && isset($_REQUEST['update']) && isset($_REQUEST['audioId'])) {
//    $id = $_REQUEST['audioId'];
//    $audioObj = $AudioModel->getAudioById($id);
//    $oldAudio = $audioObj['audio'];
//    if (!$audioObj) {
//        echo json_encode(['status' => 0, 'error' => "Audio does not exist you are trying to updated", 'id' => $id]);
//        return;
//    }
//    $name = $_REQUEST['name'];
//    $audio = $_REQUEST['audio'];
//    $categoryId = $_REQUEST['audio_category_id'];
//    $data = compact('id', 'name', 'audio', 'categoryId');
//    $updated = $AudioModel->update($data);
//    if ($updated > 0) {
//
//        if ($oldAudio !== $audio && file_exists("../$oldAudio")) {
//            unlink("../$oldAudio");
//        }
//        echo json_encode(['status' => 1, 'message' => 'success']);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "can't add audio."]);
//    return;
//}
//// get audio
//else if (isset($_REQUEST['audioId']) && isset($_REQUEST['show'])) {
//
//    $audioId = $_REQUEST['audioId'];
//
//    $results = $AudioModel->getAudioById($audioId);
//    if ($results) {
//        echo json_encode(['id' => $results['id'], 'audio_category_id' => $results['audio_category_id'], 'name' => $results['name'], 'audio' => $results['audio']]);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "audio with given id not exist"]);
//    return;
//}
//
//// delete record
//else if (isset($_REQUEST['audioId']) && isset($_REQUEST['delete'])) {
//
//    $audioId = $_REQUEST['audioId'];
//
//    $audioObj = $AudioModel->getAudioById($audioId);
//    if ($audioObj) {
//        $audio = $audioObj['audio'];
//        $delete = $AudioModel->delete($audioId);
//        if ($delete) {
//            if (file_exists("../$audio")) {
//                unlink("../$audio");
//            }
//            echo json_encode(['status' => 1, 'message' => 'deleted']);
//            return;
//        }
//        echo json_encode(['status' => 0, 'message' => "unable to delete audio"]);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "audio with given id not exist"]);
//    return;
//}