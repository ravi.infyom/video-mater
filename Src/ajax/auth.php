<?php
require "../../bootstrap.php";
if (!session_id()) {
    session_start();
}

$sesssionName = $_ENV['SESSION_NAME'];
if (isset($_REQUEST['email']) && isset($_REQUEST['psw'])) {
    $email = $_REQUEST['email'];
    $password = $_REQUEST['psw'];


    $sql = "SELECT * FROM `users` where `email`='$email' and status='active' limit 1";
    $stmt =  $dbConnection->prepare($sql);
    $stmt->execute();
    $emailId = '';
    $psw = '';
    $userId = '';
    $userType = '';
    $stmt->setFetchMode(PDO::FETCH_ASSOC);
    $result = $stmt->fetchAll();

    if (count($result) > 0) {

        $emailId = $result[0]['email'];
        $hash = $result[0]['password'];
        $userId = $result[0]['id'];
        $userType = $result[0]['user_type'];
        $name = $result[0]['name'];
        $avatar = $result[0]['avatar'];


        // echo password_verify($password, $hash);
        if (password_verify($password, $hash)) {

            $user = [
                'id' => $userId,
                'type' => $userType,
                'email' => $emailId,
                'name' => $name,
                'avatar' => $avatar
            ];

            $_SESSION[$sesssionName]['user'] = $user;
            echo 1;
            return;
        }
        echo 0;
        return;
    }

    echo 0;
    return;
} else if (isset($_REQUEST['logout'])) {
    $sessionName = $_ENV['SESSION_NAME'];

    unset($_SESSION[$sessionName]);
    echo 'logout_success';
    return;
}