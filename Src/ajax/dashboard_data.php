<?php
list($rootPath, $srcPath) = ['../..', '../../Src'];
require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

use Src\Models\Audio;
use Src\Models\AudioCategory;
use Src\Models\Category;
use Src\Models\VideoEffect;
use Src\Models\VideoReadyCategory;
use Src\Models\VideoReady;
use Src\Models\Language;

$AudioModel = new Audio($dbConnection);
$AudioCategory = new AudioCategory($dbConnection);
$Category = new Category($dbConnection);
$VideoEffect = new VideoEffect($dbConnection);
$VideoReadyCategory = new VideoReadyCategory($dbConnection);
$VideoReady = new VideoReady($dbConnection);
$Language = new Language($dbConnection);

$audioCount = $AudioModel->getAudioCount();
$audioCategoryCount = $AudioCategory->getAudioCategoryCount();
$videoCategoryCount = $Category->getCategoryCount();
$videoEffectCount = $VideoEffect->getVideoEffectCount();
$videoReadyCount = $VideoReady->getVideoReadyCount();
$videoReadyCategoryCount = $VideoReadyCategory->getCategoryCount();
$languageCount = $Language->getLanguageCount();
$data = compact('audioCount', 'audioCategoryCount', 'videoCategoryCount', 'videoEffectCount', 'videoReadyCount', 'videoReadyCategoryCount', 'languageCount');
echo json_encode($data);