<?php
list($rootPath, $srcPath) = ['../..', '../../Src'];
require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

use Src\Models\Category;
use Src\Models\DeleteFile;
use Src\System\DatabaseConnector;

$dbConnection = (new DatabaseConnector())->getConnection();

$category = new Category($dbConnection);
$df = new DeleteFile($dbConnection);

if (isset($_POST['categoryName']) && isset($_POST['icon']) && isset($_POST['status'])) {

    $categoryName = $_REQUEST['categoryName'];
    $status = $_REQUEST['status'];

    $data = $category->getCategoryByName($categoryName);
    if (count($data) > 0) {
        echo json_encode(['status' => 0, 'error' => ['category' => 'Category with given name already exist']]);
        return;
    } else {
        $icon = $_REQUEST['icon'];

        $uniqueName = time() . '-icon-' . $icon;
        $pathFile = "Src/storage/category/icon/$uniqueName";
        $path = $pathFile . basename($_FILES['icon']['name']);
        move_uploaded_file($_FILES['icon']['tmp_name'] , $path);


        $insert = $category->store(['name' => $categoryName, 'icon' => $icon, 'status' => $status]);
        if ($insert) {
            echo json_encode(['status' => 1, 'message' => 'success']);
            return;
        }
        echo json_encode(['status' => 0, 'message' => "can't add category"]);
        return;
    }
}

if(isset($_POST['txtFname']) && $_POST['txtLname'] && $_POST['txtEmail'] && $_POST['txtPhone'] && $_POST['txtPassword'] && $_POST['txtConfirmpassword'] )
{
    $firstName = $_POST['txtFname'];
    $lastName = $_POST['txtLname'];
    $email = $_POST['txtEmail'];
    $phone = $_POST['txtPhone'];
    $password = $_POST['txtPassword'];
    $encryptPassword = md5($password);
    $confirmPassword = $_POST['txtConfirmpassword'];
    $isAdmin = $_POST['txtisAdmin'];
    $image = $_FILES['txtImage']['name'];

    $tags=$_POST['txtTag'];
    $tagsImplode=implode(",",$tags);

    date_default_timezone_set("Asia/Calcutta");
    $createdAt= date("Y-m-d h:i:s");

    $tragetFile="../image/";
    $path = $tragetFile . basename($_FILES['txtImage']['name']);
    move_uploaded_file($_FILES['txtImage']['tmp_name'] , $path);

    $insertQuery = "insert into user(first_name,last_name,email_id,phone,password,con_password,image,is_admin,tags,created_at) values('$firstName','$lastName','$email','$phone','$encryptPassword','$confirmPassword','$image','$isAdmin','$tagsImplode','$createdAt')";
    $query       =  mysqli_query($connection,$insertQuery);
    $inset_last_id = mysqli_insert_id($connection);


    $tags=$_POST['txtTag'];
    if(isset($_POST['txtTag']))
    {
        foreach($tags as $tagName)
        {
            $Query        =  mysqli_query($connection,"SELECT * from tags where name ='$tagName'");
            while($record =  mysqli_fetch_array($Query))
            {
                $tagsId   =  $record['id'];
                $insertQuery  =  "INSERT into user_tags(user_id,tag_id) values('$inset_last_id','$tagsId')";
                $Query        =  mysqli_query($connection,$insertQuery);
            }
        }
    }
}
?>