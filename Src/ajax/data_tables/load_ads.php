<?php
list($rootPath, $srcPath) = ['../../..', '../../../Src'];
// require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = trim($_POST['search']['value']); // Search value

## Search 
$searchQuery = " ";
if ($searchValue != '') {
    $searchQuery = " AND (`ad_name` like '%" . $searchValue . "%' ) OR (`ad_slogan` like '%" . $searchValue . "%') OR (`company_name`like '%" . $searchValue . "%') OR (`id` = '$searchValue') OR (`ad_type` like '%" . $searchValue . "%') OR (`package_name` like '%" . $searchValue . "%')";
}

## Total number of records without filtering
$sel =  $dbConnection->query("SELECT count(*) as allcount from `ad`")->fetch();
$totalRecords = $sel['allcount'];

## Total number of record with filtering
$sel =  $dbConnection->query("SELECT count(*) as allcount from `ad` WHERE 1 " . $searchQuery)->fetch();
$totalRecordwithFilter = $sel['allcount'];

## Fetch records
$query = "SELECT * from `ad` WHERE 1 " . $searchQuery . " order by " . $columnName . " " . $columnSortOrder . " limit " . $row . "," . $rowperpage;

$records = $dbConnection->query($query);
$data = array();

foreach ($dbConnection->query($query) as $row) {
   $temp = [
        "id" => $row['id'],
        "ad_name" => $row['ad_name'],
        "ad_slogan" => $row['ad_slogan'],
        "company_name" => $row['company_name'],
        "ad_type" => $row['ad_type'],
        "package_name" => $row['package_name'],
        "redirect_url" => $row['redirect_url'],
    ];
    $icon=$row['ad_icon'];
    $pos=stripos($icon,"/storage/ad/icon");
    if($pos==3)
    {
        $icon=$_ENV['APP_URL']."/".$icon;
    }
    $temp['ad_icon']=$icon;
    $data[]=$temp;

}


## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data
);

echo json_encode($response);