<?php
list($rootPath, $srcPath) = ['../../..', '../../../Src'];
require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = trim($_POST['search']['value']); // Search value

// $searchable=[];
// $columns=isset($_REQUEST['columns']) ?$_REQUEST['columns'] : ['language']; 
// array_walk($columns,function($column) use ($searchable){
//     if($column['searchable']==true)
//     {
//        $searchable[]= $column['data'];
//     }
// });

// echo json_encode($searchable);
## Search 
$searchQuery = " ";
if ($searchValue != '') {
    $searchQuery = " AND (`name` like '%" . $searchValue . "%' ) OR (`id` = '$searchValue')";
}

## Total number of records without filtering
$sel =  $dbConnection->query("SELECT count(`audio`.`id`) as allcount from `audio` join `audio_category`on `audio_category`.`id` = `audio`.`audio_category_id`")->fetch();
$totalRecords = $sel['allcount'];

## Total number of record with filtering
$sel =  $dbConnection->query("SELECT count(`audio`.`id`) as allcount from `audio` join `audio_category`on `audio_category`.`id` = `audio`.`audio_category_id` WHERE 1 " . $searchQuery)->fetch();
$totalRecordwithFilter = $sel['allcount'];

## Fetch records
$query = "SELECT `audio`.*,`audio_category`.`name` as `audio_category_name` from `audio` join `audio_category`on `audio_category`.`id` = `audio`.`audio_category_id` WHERE 1 " . $searchQuery . " order by " . $columnName . " " . $columnSortOrder . " limit " . $row . "," . $rowperpage;
$records = $dbConnection->query($query);
$data = array();

foreach ($dbConnection->query($query) as $row) {
    $data[] = [
        "id" => $row['id'],
        "audio_category_id" => $row['audio_category_id'],
        "audio_category_name" => $row['audio_category_name'],
        "name" => $row['name'],
        "audio" => $row['audio'],
    ];
}


## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data
);

echo json_encode($response);