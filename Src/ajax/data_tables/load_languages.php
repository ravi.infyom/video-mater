<?php
list($rootPath, $srcPath) = ['../../..', '../../../Src'];
require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = trim($_POST['search']['value']); // Search value

// $searchable=[];
// $columns=isset($_REQUEST['columns']) ?$_REQUEST['columns'] : ['language']; 
// array_walk($columns,function($column) use ($searchable){
//     if($column['searchable']==true)
//     {
//        $searchable[]= $column['data'];
//     }
// });

// echo json_encode($searchable);
## Search 
$searchQuery = " ";
if ($searchValue != '') {
    $searchQuery = " AND (language like '%" . $searchValue . "%' ) OR (id like '%" . $searchValue . "%')";
}

## Total number of records without filtering
$sel =  $dbConnection->query("SELECT count(*) as allcount from languages")->fetch();
$totalRecords = $sel['allcount'];

## Total number of record with filtering
$sel =  $dbConnection->query("SELECT count(*) as allcount from languages WHERE 1 " . $searchQuery)->fetch();
$totalRecordwithFilter = $sel['allcount'];

## Fetch records
$query = "SELECT * from languages WHERE 1 " . $searchQuery . " order by " . $columnName . " " . $columnSortOrder . " limit " . $row . "," . $rowperpage;
$records = $dbConnection->query($query);
$data = array();

foreach ($dbConnection->query($query) as $row) {
    $data[] = [
        "id" => $row['id'],
        "language" => $row['language'],
    ];
}


## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data
);

echo json_encode($response);