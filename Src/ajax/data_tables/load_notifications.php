<?php
list($rootPath, $srcPath) = ['../../..', '../../../Src'];
// require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = trim($_POST['search']['value']); // Search value

## Search 
$searchQuery = " ";
if ($searchValue != '') {
    $searchQuery = " AND (`title` like '%" . $searchValue . "%' ) OR (`message` like '%" . $searchValue . "%') OR (`banner_size`like '%" . $searchValue . "%') OR (`id` = '$searchValue') OR (`status` like '%" . $searchValue . "%')";
}

## Total number of records without filtering
$sel =  $dbConnection->query("SELECT count(*) as allcount from `notification`")->fetch();
$totalRecords = $sel['allcount'];

## Total number of record with filtering
$sel =  $dbConnection->query("SELECT count(*) as allcount from `notification` WHERE 1 " . $searchQuery)->fetch();
$totalRecordwithFilter = $sel['allcount'];

## Fetch records
$query = "SELECT * from `notification` WHERE 1 " . $searchQuery . " order by " . $columnName . " " . $columnSortOrder . " limit " . $row . "," . $rowperpage;

$records = $dbConnection->query($query);
$data = array();

foreach ($dbConnection->query($query) as $row) {
    $data[] = [
        "id" => $row['id'],
        "title" => $row['title'],
        "message" => $row['message'],
        "banner_size" => $row['banner_size'],
        // "banner_image" => $row['banner_image'],
        // "target_url" => $row['target_url'],
        "open_app" => $row['open_app'],
        "status" => $row['status'],
    ];
}


## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data
);

echo json_encode($response);