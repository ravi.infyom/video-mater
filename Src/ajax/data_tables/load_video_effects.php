<?php
list($rootPath, $srcPath) = ['../../..', '../../../Src'];
require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = trim($_POST['search']['value']); // Search value

// $searchable=[];
// $columns=isset($_REQUEST['columns']) ?$_REQUEST['columns'] : ['language']; 
// array_walk($columns,function($column) use ($searchable){
//     if($column['searchable']==true)
//     {
//        $searchable[]= $column['data'];
//     }
// });

// echo json_encode($searchable);
## Search 
$searchQuery = " ";
if ($searchValue != '') {
    $searchQuery = " and (`video_effects`.`id` like '%" . $searchValue . "%' or `category`.`name` like '%" . $searchValue . "%' or `video_effects`.`title` like '%" . $searchValue . "%')";
}

## Total number of records without filtering
$sel =  $dbConnection->query("SELECT count(`video_effects`.`id`) as allcount FROM `video_effects` join `category`on `category`.`id` = `video_effects`.`category_id`")->fetch();
$totalRecords = $sel['allcount'];

// WHERE  `category`.`status`='active'
## Total number of record with filtering
$sel =  $dbConnection->query("SELECT count(`video_effects`.`id`) as allcount FROM `video_effects` join `category`on `category`.`id` = `video_effects`.`category_id` WHERE  1" . $searchQuery)->fetch();
$totalRecordwithFilter = $sel['allcount'];

## Fetch records
$query = "SELECT `video_effects`.*,`category`.`name` as `category_name` FROM `video_effects` join `category`on `category`.`id` = `video_effects`.`category_id` WHERE  1" . $searchQuery . " order by " . $columnName . " " . $columnSortOrder . " limit " . $row . "," . $rowperpage;
$records = $dbConnection->query($query);
$data = array();

foreach ($dbConnection->query($query) as $row) {
    $data[] = [
        "id" => $row['id'],
        "category_name" => $row['category_name'],
        "title" => $row['title'],
        "cover_image" => $row['cover_image'],
    ];
}


## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data
);

echo json_encode($response);