<?php
list($rootPath, $srcPath) = ['../../..', '../../../Src'];
require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";


## Read value
$draw = $_POST['draw'];
$row = $_POST['start'];
$rowperpage = $_POST['length']; // Rows display per page
$columnIndex = $_POST['order'][0]['column']; // Column index
$columnName = $_POST['columns'][$columnIndex]['data']; // Column name
$columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
$searchValue = trim($_POST['search']['value']); // Search value


## Search 
$searchQuery = " ";
if ($searchValue != '') {
    $searchQuery = " AND (`name` like '%" . $searchValue . "%' ) OR (`id` = '$searchValue')";
}

## Total number of records without filtering
$sel = $dbConnection->query("SELECT count(*) as allcount from `video_sub_category`")->fetch();
$totalRecords = $sel['allcount'];

## Total number of record with filtering
$sel = $dbConnection->query("SELECT count(*) as allcount from `video_sub_category` WHERE 1" . $searchQuery)->fetch();
$totalRecordwithFilter = $sel['allcount'];

## Fetch records
$query = "SELECT `video_sub_category`.*,`category`.`name` as `category_name` FROM `video_sub_category` join `category`on `category`.`id` = `video_sub_category`.`category_id` WHERE 1" . $searchQuery . " order by " . $columnName . " " . $columnSortOrder . " limit " . $row . "," . $rowperpage;
$records = $dbConnection->query($query);
$data = array();
foreach ($dbConnection->query($query) as $row) {
    $data[] = [
        "id" => $row['id'],
        "name" => $row['name'],
        "icon" => $row['icon'],
        "category_name" => $row['category_name'],
    ];
}


## Response
$response = array(
    "draw" => intval($draw),
    "iTotalRecords" => $totalRecords,
    "iTotalDisplayRecords" => $totalRecordwithFilter,
    "aaData" => $data
);

echo json_encode($response);