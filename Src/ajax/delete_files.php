<?php
list($rootPath, $srcPath) = ['../..', '../../Src'];
require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

use Src\Models\DeleteFile;

$deleteFile = new DeleteFile($dbConnection);

$getFiles = $deleteFile->getAllFiles();


if (is_array($getFiles) && count($getFiles) > 0) {
    $finalFiles = [];
    foreach ($getFiles as $file) {
        $id = $file['id'];
        $filePath = $file['file_path'];
        $table = $file['subject'];
        $field = $file['subject_name'];
        if ($deleteFile->canDelete($filePath, $table, $field)) {
            $fullPath = "$rootPath/$filePath";
            if (file_exists($fullPath)) {

                unlink($fullPath);
            }
            $finalFiles[] = $id;
        }
    }

    $deleteFile->deleteFiles($finalFiles);
}

echo json_encode(['status' => 1]);
return;
// print_r($finalFiles);