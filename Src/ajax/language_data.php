<?php

require "../../bootstrap.php";

use Src\Models\Language;

$language = new Language($dbConnection);
if (isset($_REQUEST['id'])) {
	$results = $language->getLanguages();
	// $results=[];
	// $sql = "SELECT * FROM `languages`";
	// foreach ( $dbh->query($sql) as $row)
	// {
	// 	$data['id']=$row['id'];
	// 	$data['language']=$row['language'];


	// 	array_push($results,$data);		
	// }
	echo json_encode($results);
} else if (isset($_REQUEST['languageId']) && isset($_REQUEST['show'])) {
	$results = $language->getLanguageById($_REQUEST['languageId']);
	echo json_encode(['id' => $results['id'], 'language' => $results['language']]);
} else if (isset($_REQUEST['language'])  && isset($_REQUEST['insert'])) {

	$lan = $_REQUEST['language'];
	$data = $language->getLanguageByName($lan);
	if (count($data) > 0) {
		echo json_encode('duplicate');
		return;
	} else {
		$insert = $language->store($lan);
		echo json_encode($insert);
		return;
	}
} else if (isset($_REQUEST['languageId']) && isset($_REQUEST['delete'])) {

	$languageId = $_REQUEST['languageId'];

	$delete = $language->delete($languageId);
	echo json_encode($delete);
	return;
} else if (isset($_REQUEST['languageId']) && isset($_REQUEST['language']) && isset($_REQUEST['update'])) {

	$lan = $_REQUEST['language'];
	$data = $language->getLanguageByName($lan);
	if (count($data) > 0) {
		echo json_encode('duplicate');
		return;
	} else {
		$languageId = $_REQUEST['languageId'];
		$languageName = $_REQUEST['language'];

		$update = $language->update(['languageId' => $languageId, 'language' => $languageName]);
		echo json_encode($update);
		return;
	}
}