<?php
list($rootPath, $srcPath) = ['../..', '../../Src'];
require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

use Src\Models\Notification;

$notification = new Notification($dbConnection);

if (isset($_REQUEST['title']) && isset($_REQUEST['message']) && isset($_REQUEST['banner_image'])  && isset($_REQUEST['banner_size']) && isset($_REQUEST['target_url']) && isset($_REQUEST['open_app']) && isset($_REQUEST['insert'])) {

    $title = $_REQUEST['title'];
    $message = $_REQUEST['message'];
    $banner_image = $_REQUEST['banner_image'];
    $banner_size = $_REQUEST['banner_size'];
    $target_url = $_REQUEST['target_url'];
    $open_app = $_REQUEST['open_app'];
    $status = 'PENDING';

    $insert = $notification->store(compact('title', 'message', 'banner_image','banner_size', 'target_url', 'open_app', 'status'));
    if ($insert) {
        echo json_encode(['status' => 1, 'message' => 'success', 'id' => $insert]);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "can't add category"]);
    return;
}
// delete notification
else if (isset($_REQUEST['notificationId']) && isset($_REQUEST['delete'])) {
    $id = $_REQUEST['notificationId'];
    $res=$notification->delete($id);
    if($res)
    {
        echo json_encode(['status' => 1, 'message' => 'success']);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "can't delete notification"]);
    return;
}
else if (isset($_REQUEST['notificationId']) && isset($_REQUEST['send'])) {
    $id = $_REQUEST['notificationId'];
    $res=$notification->sendNotiFication($id);
    if($res)
    {
        $notification->updateStatus($id,'COMPLETED');
        echo json_encode(['status' => 1, 'message' => 'success']);
        return;
    }
    else
    {
        $notification->updateStatus($id,'FAILED');

    }
    echo json_encode(['status' => 0, 'message' => "can't send notification"]);
    return;
}

else if (isset($_FILES['bannerImage']) && isset($_REQUEST['upload'])) {
    $banner = $_FILES['bannerImage']['name'];
    $banner_tmp = $_FILES['bannerImage']['tmp_name'];

    $uniqueName = time() . '-banner-' . $banner;
    $bannerPath = "Src/storage/notification/banner_image/$uniqueName";
    $moved = move_uploaded_file($banner_tmp, "$rootPath/$bannerPath");
    if ($moved) {
        echo json_encode(['status' => 1, 'path' => $bannerPath]);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "unable to upload banner"]);
    return;
}

