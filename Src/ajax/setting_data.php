<?php
list($rootPath, $srcPath) = ['../..', '../../Src'];
require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

use Src\Models\DeleteFile;
use Src\Models\User;

$userModel = new User($dbConnection);
$df = new DeleteFile($dbConnection);
$sn = $_ENV['SESSION_NAME'];
$sessionId = $_SESSION[$sn] && $_SESSION[$sn]['user'] && $_SESSION[$sn]['user']['id'] ? $_SESSION[$sn]['user']['id'] : null;
// upload avatar
if (isset($_FILES['avatar']) && isset($_REQUEST['uploadFile'])) {
    $avatar = $_FILES['avatar']['name'];
    $avatar_tmp = $_FILES['avatar']['tmp_name'];

    $uniqueName = time() . '-avatar-' . $avatar;
    $avatarPath = "Src/storage/user/avatar/$uniqueName";
    $moved = move_uploaded_file($avatar_tmp, "$rootPath/$avatarPath");
    if ($moved) {
        echo json_encode(['status' => 1, 'path' => $avatarPath]);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "unable to upload avatar"]);
    return;
}
// upload icon
else if (isset($_FILES['icon']) && isset($_REQUEST['uploadIcon'])) {
    $icon = $_FILES['icon']['name'];
    $icon_tmp = $_FILES['icon']['tmp_name'];

    $uniqueName = time() . '-icon-' . $icon;
    $iconPath = "Src/storage/user/country/icon/$uniqueName";
    $moved = move_uploaded_file($icon_tmp, "$rootPath/$iconPath");
    if ($moved) {
        echo json_encode(['status' => 1, 'path' => $iconPath]);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "unable to upload icon"]);
    return;
}
// delete file
else if (isset($_REQUEST['filePath']) && isset($_REQUEST['fieldName']) && isset($_REQUEST['deleteFile'])) {
    $file = $_REQUEST['filePath'];
    $subjectName = $_REQUEST['fieldName'];
    $df->addFile($file, 'audio', $subjectName);
    echo json_encode(['status' => 1]);
    return;
}
// load settings
else if (isset($_REQUEST['settings'])) {

    if ($sessionId) {
        $userData = $userModel->getUserById($sessionId);
        if (count($userData) > 0) {
            echo json_encode([
                'status' => true,
                'data' => $userData,
            ]);
            return;
        } else {
            echo json_encode([
                'status' => false,
                'message' => 'User data not found',
            ]);
            return;
        }
    } else {
        echo json_encode([
            'status' => false,
            'message' => 'User id not found',
        ]);
        return;
    }
} else if (isset($_REQUEST['name']) && isset($_REQUEST['avatar']) && isset($_REQUEST['email']) && isset($_REQUEST['updateProfile'])) {
    if ($sessionId) {
        $name = $_REQUEST['name'];
        $avatar = $_REQUEST['avatar'];
        $email = $_REQUEST['email'];
        $userData = $userModel->getUserById($sessionId);
        if (count($userData) > 0) {
            $oldPath = $userData['avatar'];
            $updated = $userModel->updateProfile($sessionId, $name, $avatar, $email);
            if ($updated && $oldPath != $avatar) {
                $df->addFile($oldPath, 'users', 'avatar');
            }
            echo json_encode([
                'status' => true,
            ]);
            return;
        } else {
            echo json_encode([
                'status' => false,
                'message' => 'User data not found',
            ]);
            return;
        }
    } else {
        echo json_encode([
            'status' => false,
            'message' => 'User id not found',
        ]);
        return;
    }
} else if (isset($_REQUEST['fbAd']) && isset($_REQUEST['fbIntreastial']) && isset($_REQUEST['fbBanner']) && isset($_REQUEST['fbNative']) && isset($_REQUEST['fbNativeBanner']) && isset($_REQUEST['adMobAd']) && isset($_REQUEST['adMobIntreastial']) && isset($_REQUEST['adMobBanner']) && isset($_REQUEST['adMobNative']) && isset($_REQUEST['adMobRv']) && isset($_REQUEST['pageClicks']) &&  isset($_REQUEST['bannerView']) && isset($_REQUEST['nativeView']) && isset($_REQUEST['updateNetwork'])) {

    

    if ($sessionId) {
        $fbAd = $_REQUEST['fbAd'];
        $fbIntreastial = $_REQUEST['fbIntreastial'];
        $fbBanner = $_REQUEST['fbBanner'];
        $fbNative = $_REQUEST['fbNative'];
        $fbNativeBanner = $_REQUEST['fbNativeBanner'];
        $adMobAd = $_REQUEST['adMobAd'];
        $adMobIntreastial = $_REQUEST['adMobIntreastial'];
        $adMobBanner = $_REQUEST['adMobBanner'];
        $adMobNative = $_REQUEST['adMobNative'];
        $adMobRv = $_REQUEST['adMobRv'];
        $pageClicks = $_REQUEST['pageClicks'];
        $bannerView = $_REQUEST['bannerView'];
        $nativeView = $_REQUEST['nativeView'];
        $userData = $userModel->getUserById($sessionId);
        if (count($userData) > 0) {
            $data = (array) $userData['data'];

            $data['network']['fbAd'] = $fbAd;
            $data['network']['fbIntreastial'] = $fbIntreastial;
            $data['network']['fbBanner'] = $fbBanner;
            $data['network']['fbNative'] = $fbNative;
            $data['network']['fbNativeBanner'] = $fbNativeBanner;
            $data['network']['adMobAd'] = $adMobAd;
            $data['network']['adMobIntreastial'] = $adMobIntreastial;
            $data['network']['adMobBanner'] = $adMobBanner;
            $data['network']['adMobNative'] = $adMobNative;
            $data['network']['adMobRv'] = $adMobRv;
            $data['network']['pageClicks'] = $pageClicks;
            $data['network']['bannerView'] = $bannerView;
            $data['network']['nativeView'] = $nativeView;
            $updated = $userModel->updateJsonData($sessionId, $data);

            echo json_encode([
                'status' => true,
            ]);
            return;
        } else {
            echo json_encode([
                'status' => false,
                'message' => 'User data not found',
            ]);
            return;
        }
    } else {
        echo json_encode([
            'status' => false,
            'message' => 'User id not found',
        ]);
        return;
    }
} else if (isset($_REQUEST['token']) && isset($_REQUEST['updateToken'])) {
    if ($sessionId) {
        $token = $_REQUEST['token'];
        $userData = $userModel->getUserById($sessionId);
        if (count($userData) > 0) {
            $data = (array) $userData['data'];
            $data['token'] = $token;
            $updated = $userModel->updateJsonData($sessionId, $data);
            echo json_encode([
                'status' => true,
            ]);
            return;
        } else {
            echo json_encode([
                'status' => false,
                'message' => 'User data not found',
            ]);
            return;
        }
    } else {
        echo json_encode([
            'status' => false,
            'message' => 'User id not found',
        ]);
        return;
    }
} else if (isset($_REQUEST['password']) && isset($_REQUEST['updatePassword'])) {
    if ($sessionId) {
        $password = $_REQUEST['password'];
        $userData = $userModel->getUserById($sessionId);
        if (count($userData) > 0) {
            $updated = $userModel->updatePassword($sessionId, $password);
            echo json_encode([
                'status' => true,
            ]);
            return;
        } else {
            echo json_encode([
                'status' => false,
                'message' => 'User data not found',
            ]);
            return;
        }
    } else {
        echo json_encode([
            'status' => false,
            'message' => 'User id not found',
        ]);
        return;
    }
} else if (isset($_REQUEST['updateSession'])) {

    $sesssionName = $_ENV['SESSION_NAME'];
    $userData = $userModel->getUserById($sessionId);
    if (count($userData) > 0) {
        $_SESSION[$sesssionName]['user']['name'] = $userData['name'];
        $_SESSION[$sesssionName]['user']['email'] = $userData['email'];
        $_SESSION[$sesssionName]['user']['avatar'] = $userData['avatar'];
        echo json_encode([
            'status' => true,
        ]);
        return;
    } else {
        echo json_encode([
            'status' => false,
            'message' => 'User data not found',
        ]);
        return;
    }
}
else if (isset($_REQUEST['updateStatus']) && isset($_REQUEST['versionCode']) && isset($_REQUEST['whatsNew']) && isset($_REQUEST['updateDetails'])) {
    if ($sessionId) {
        $updateStatus = $_REQUEST['updateStatus'];
        $versionCode = $_REQUEST['versionCode'];
        $whatsNew = $_REQUEST['whatsNew'];

        $userData = $userModel->getUserById($sessionId);
        if (count($userData) > 0) {
            $data = (array) $userData['data'];

            $data['update']['updateStatus'] = $updateStatus;
            $data['update']['versionCode'] = $versionCode;
            $data['update']['whatsNew'] = $whatsNew;
            $updated = $userModel->updateJsonData($sessionId, $data);

            echo json_encode([
                'status' => true,
            ]);
            return;
        } else {
            echo json_encode([
                'status' => false,
                'message' => 'User data not found',
            ]);
            return;
        }
    } else {
        echo json_encode([
            'status' => false,
            'message' => 'User id not found',
        ]);
        return;
    }
}
// update country
else if (isset($_REQUEST['makerName']) && isset($_REQUEST['icon']) && isset($_REQUEST['countryStatus'])&& isset($_REQUEST['countryMessage']) && isset($_REQUEST['updateCountry'])) {
    if ($sessionId) {
        $makerName = $_REQUEST['makerName'];
        $icon = $_REQUEST['icon'];
        $countryMessage = $_REQUEST['countryMessage'];
        $countryStatus = $_REQUEST['countryStatus'];
        $userData = $userModel->getUserById($sessionId);
        if (count($userData) > 0) {
            $data = (array) $userData['data'];

            $data['country']['makerName'] = $makerName;
            $data['country']['countryMessage'] = $countryMessage;
            $data['country']['countryStatus'] = $countryStatus;
            $data['country']['icon'] = $icon;
            $updated = $userModel->updateJsonData($sessionId, $data);

            echo json_encode([
                'status' => true,
            ]);
            return;
        } else {
            echo json_encode([
                'status' => false,
                'message' => 'User data not found',
            ]);
            return;
        }
    } else {
        echo json_encode([
            'status' => false,
            'message' => 'User id not found',
        ]);
        return;
    }
}
// update ad
else if (isset($_REQUEST['ads']) && isset($_REQUEST['showAd']) && isset($_REQUEST['numberOfClick']) && isset($_REQUEST['updateAd'])) {
    if ($sessionId) {
        $ads = $_REQUEST['ads'];
        $showAd = $_REQUEST['showAd'];
        $numberOfClick = $_REQUEST['numberOfClick'];
        $userData = $userModel->getUserById($sessionId);
        if (count($userData) > 0) {
            $data = (array) $userData['data'];

            $data['ad']['ads'] = $ads;
            $data['ad']['numberOfClick'] = $numberOfClick;
            $data['ad']['showAd'] = $showAd;
            $updated = $userModel->updateJsonData($sessionId, $data);

            echo json_encode([
                'status' => true,
            ]);
            return;
        } else {
            echo json_encode([
                'status' => false,
                'message' => 'User data not found',
            ]);
            return;
        }
    } else {
        echo json_encode([
            'status' => false,
            'message' => 'User id not found',
        ]);
        return;
    }
}
// update record
// else if (isset($_REQUEST['name']) && isset($_REQUEST['audio']) && isset($_REQUEST['audio_category_id'])  && isset($_REQUEST['update']) && isset($_REQUEST['audioId'])) {
//     $id = $_REQUEST['audioId'];
//     $audioObj = $AudioModel->getAudioById($id);
//     $oldAudio = $audioObj['audio'];
//     if (!$audioObj) {
//         echo json_encode(['status' => 0, 'error' => "Audio does not exist you are trying to updated", 'id' => $id]);
//         return;
//     }
//     $name = $_REQUEST['name'];
//     $audio = $_REQUEST['audio'];
//     $categoryId = $_REQUEST['audio_category_id'];
//     $data = compact('id', 'name', 'audio', 'categoryId');
//     $updated = $AudioModel->update($data);
//     if ($updated > 0) {

//         if ($oldAudio !== $audio && file_exists("../$oldAudio")) {
//             unlink("../$oldAudio");
//         }
//         echo json_encode(['status' => 1, 'message' => 'success']);
//         return;
//     }
//     echo json_encode(['status' => 0, 'message' => "can't add audio."]);
//     return;
// }
