<?php
list($rootPath, $srcPath) = ['../..', '../../Src'];
require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

use Src\Models\DeleteFile;
use Src\Models\VideoEffect;

$dbConnection = (new DatabaseConnector())->getConnection();


$VideoEffect = new VideoEffect($dbConnection);
$df = new DeleteFile($dbConnection);


if (isset($_REQUEST['category']) && isset($_REQUEST['title']) && isset($_FILES['video']) && isset($_FILES['cover_image']) && isset($_FILES['zip']) && isset($_REQUEST['insert'])) {
    $categoryId = $_REQUEST['category'];
    $title = $_REQUEST['title'];
    $video = $_REQUEST['video'];
    $coverImage = $_REQUEST['cover_image'];
    $zip = $_REQUEST['zip'];
    $data = compact('categoryId', 'title', 'video', 'coverImage', 'zip');
    $inserted = $VideoEffect->store($data);
    if ($inserted) {
        echo json_encode(['status' => 1, 'message' => 'success']);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "can't add video effect"]);
    return;
}




//if (isset($_FILES['coverImage']) && isset($_REQUEST['uploadCoverImage'])) {
//    $image = $_FILES['coverImage']['name'];
//    $image_tmp = $_FILES['coverImage']['tmp_name'];
//
//    $uniqueName = time() . '-video-effect-cover-image-' . $image;
//    $path = "Src/storage/video_effect/cover_image/$uniqueName";
//    $moved = move_uploaded_file($image_tmp, "$rootPath/$path");
//    if ($moved) {
//        echo json_encode(['status' => 1, 'path' => $path]);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "unable to upload video"]);
//    return;
//} else if (isset($_FILES['video']) && isset($_REQUEST['uploadVideo'])) {
//    $video = $_FILES['video']['name'];
//    $video_tmp = $_FILES['video']['tmp_name'];
//
//    $uniqueName = time() . '-video-effect-video-' . $video;
//    $videoPath = "Src/storage/video_effect/video/$uniqueName";
//    $moved = move_uploaded_file($video_tmp, "$rootPath/$videoPath");
//    if ($moved) {
//        echo json_encode(['status' => 1, 'path' => $videoPath]);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "unable to upload video"]);
//    return;
//} else if (isset($_FILES['zip']) && isset($_REQUEST['uploadZip'])) {
//    $zip = $_FILES['zip']['name'];
//    $zip_tmp = $_FILES['zip']['tmp_name'];
//
//    $uniqueName = time() . '-video-effect-zip-' . $zip;
//    $zipPath = "Src/storage/video_effect/zip/$uniqueName";
//    $moved = move_uploaded_file($zip_tmp, "$rootPath/$zipPath");
//    if ($moved) {
//        echo json_encode(['status' => 1, 'path' => $zipPath]);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "unable to upload video"]);
//    return;
//} else if (isset($_REQUEST['filePath']) && isset($_REQUEST['fieldName']) && isset($_REQUEST['deleteFile'])) {
//    $file = $_REQUEST['filePath'];
//    $subjectName = $_REQUEST['fieldName'];
//    $df->addFile($file, 'video_effects', $subjectName);
//    echo json_encode(['status' => 1]);
//    return;
////} else if (isset($_REQUEST['category']) && isset($_REQUEST['title']) && isset($_REQUEST['video']) && isset($_REQUEST['cover_image']) && isset($_REQUEST['zip']) && isset($_REQUEST['insert'])) {
//} else if (isset($_REQUEST['category']) && isset($_REQUEST['title']) && isset($_REQUEST['video']) && isset($_REQUEST['cover_image']) && isset($_REQUEST['zip']) && isset($_REQUEST['insert'])) {
//    $categoryId = $_REQUEST['category'];
//    $title = $_REQUEST['title'];
//    $video = $_REQUEST['video'];
//    $coverImage = $_REQUEST['cover_image'];
//    $zip = $_REQUEST['zip'];
//    $data = compact('categoryId', 'title', 'video', 'coverImage', 'zip');
//    $inserted = $VideoEffect->store($data);
//    if ($inserted) {
//        echo json_encode(['status' => 1, 'message' => 'success']);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "can't add video effect"]);
//    return;
//} else if (isset($_REQUEST['category']) && isset($_REQUEST['title']) && isset($_REQUEST['video']) && isset($_REQUEST['cover_image']) && isset($_REQUEST['zip']) && isset($_REQUEST['update']) && isset($_REQUEST['videoEffectId'])) {
//    $id = $_REQUEST['videoEffectId'];
//    $videoEffectObj = $VideoEffect->getVideoEffectById($id);
//    $oldCoverImage = $videoEffectObj['cover_image'];
//    $oldVideo = $videoEffectObj['video'];
//    $oldZip = $videoEffectObj['zip'];
//    if (!$videoEffectObj) {
//        echo json_encode(['status' => 0, 'error' => "Video effect does not exist you are trying to updated", 'id' => $videoEffectId]);
//        return;
//    }
//    $categoryId = $_REQUEST['category'];
//    $title = $_REQUEST['title'];
//    $video = $_REQUEST['video'];
//    $coverImage = $_REQUEST['cover_image'];
//    $zip = $_REQUEST['zip'];
//    $data = compact('id', 'categoryId', 'title', 'video', 'coverImage', 'zip');
//    $updated = $VideoEffect->update($data);
//    if ($updated > 0) {
//        if ($oldCoverImage !== $coverImage && file_exists("../$oldCoverImage")) {
//            unlink("../$oldCoverImage");
//        }
//        if ($oldVideo !== $video && file_exists("../$oldVideo")) {
//            unlink("../$oldVideo");
//        }
//        if ($oldZip !== $zip && file_exists("../$oldZip")) {
//            unlink("../$oldZip");
//        }
//        echo json_encode(['status' => 1, 'message' => 'success']);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "can't add video effect"]);
//    return;
//}
//// get video effect
//else if (isset($_REQUEST['videoEffectId']) && isset($_REQUEST['show'])) {
//
//    $videoEffectId = $_REQUEST['videoEffectId'];
//
//    $results = $VideoEffect->getVideoEffectById($videoEffectId);
//    if ($results) {
//        echo json_encode(['id' => $results['id'], 'category_id' => $results['category_id'], 'title' => $results['title'], 'cover_image' => $results['cover_image'], 'video' => $results['video'], 'zip' => $results['zip']]);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "video effect with given id not exist"]);
//    return;
//}
//// delete video effect
//else if (isset($_REQUEST['videoEffectId']) && isset($_REQUEST['delete'])) {
//
//    $videoEffectId = $_REQUEST['videoEffectId'];
//
//    $VideoEffectObj = $VideoEffect->getVideoEffectById($videoEffectId);
//    if ($VideoEffectObj) {
//        $coverImage = $VideoEffectObj['cover_image'];
//        $video = $VideoEffectObj['video'];
//        $zip = $VideoEffectObj['zip'];
//        $delete = $VideoEffect->delete($videoEffectId);
//        if ($delete) {
//            if (file_exists("../$coverImage")) {
//                unlink("../$coverImage");
//            }
//            if (file_exists("../$video")) {
//                unlink("../$video");
//            }
//            if (file_exists("../$zip")) {
//                unlink("../$zip");
//            }
//            echo json_encode(['status' => 1, 'message' => 'deleted']);
//            return;
//        }
//        echo json_encode(['status' => 0, 'message' => "unable to delete video effect"]);
//        return;
//    }
//    echo json_encode(['status' => 0, 'message' => "video effect with given id not exist"]);
//    return;
//}
