<?php
list($rootPath, $srcPath) = ['../..', '../../Src'];
require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

use Src\Models\VideoReady;
use Src\Models\DeleteFile;

$VideoReady = new VideoReady($dbConnection);
$df = new DeleteFile($dbConnection);

// upload cover image
if (isset($_FILES['coverImage']) && isset($_REQUEST['uploadCoverImage'])) {
    $image = $_FILES['coverImage']['name'];
    $image_tmp = $_FILES['coverImage']['tmp_name'];

    $uniqueName = time() . '-video-ready-cover-image-' . $image;
    $path = "Src/storage/video_ready/cover_image/$uniqueName";
    $moved = move_uploaded_file($image_tmp, "$rootPath/$path");
    if ($moved) {
        echo json_encode(['status' => 1, 'path' => $path]);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "unable to upload video"]);
    return;
}
// upload video
else if (isset($_FILES['video']) && isset($_REQUEST['uploadVideo'])) {
    $video = $_FILES['video']['name'];
    $video_tmp = $_FILES['video']['tmp_name'];

    $uniqueName = time() . '-video-ready-video-' . $video;
    $videoPath = "Src/storage/video_ready/video/$uniqueName";
    $moved = move_uploaded_file($video_tmp, "$rootPath/$videoPath");
    if ($moved) {
        echo json_encode(['status' => 1, 'path' => $videoPath]);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "unable to upload video"]);
    return;
}
// delete file 
else if (isset($_REQUEST['filePath']) && isset($_REQUEST['fieldName']) && isset($_REQUEST['deleteFile'])) {
    $file = $_REQUEST['filePath'];
    $subjectName = $_REQUEST['fieldName'];
    $df->addFile($file, 'video_ready', $subjectName);
    echo json_encode(['status' => 1]);
    return;
}
// insert record
else if (isset($_REQUEST['category']) && isset($_REQUEST['title']) && isset($_REQUEST['video']) && isset($_REQUEST['cover_image']) && isset($_REQUEST['insert'])) {
    $categoryId = $_REQUEST['category'];
    $title = $_REQUEST['title'];
    $video = $_REQUEST['video'];
    $coverImage = $_REQUEST['cover_image'];
    $data = compact('categoryId', 'title', 'video', 'coverImage');
    $inserted = $VideoReady->store($data);
    if ($inserted) {
        echo json_encode(['status' => 1, 'message' => 'success']);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "can't add video"]);
    return;
}
// update record
else if (isset($_REQUEST['category']) && isset($_REQUEST['title']) && isset($_REQUEST['video']) && isset($_REQUEST['cover_image']) && isset($_REQUEST['update']) && isset($_REQUEST['videoReadyId'])) {
    $id = $_REQUEST['videoReadyId'];
    $videoReadyObj = $VideoReady->getVideoReadyById($id);
    $oldCoverImage = $videoReadyObj['cover_image'];
    $oldVideo = $videoReadyObj['video'];
    if (!$videoReadyObj) {
        echo json_encode(['status' => 0, 'error' => "Video does not exist you are trying to updated", 'id' => $videoReadyId]);
        return;
    }
    $categoryId = $_REQUEST['category'];
    $title = $_REQUEST['title'];
    $video = $_REQUEST['video'];
    $coverImage = $_REQUEST['cover_image'];
    $data = compact('id', 'categoryId', 'title', 'video', 'coverImage');
    $updated = $VideoReady->update($data);
    if ($updated > 0) {
        if ($oldCoverImage !== $coverImage && file_exists("../$oldCoverImage")) {
            unlink("../$oldCoverImage");
        }
        if ($oldVideo !== $video && file_exists("../$oldVideo")) {
            unlink("../$oldVideo");
        }
        echo json_encode(['status' => 1, 'message' => 'success']);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "can't add video."]);
    return;
}
// get video  
else if (isset($_REQUEST['videoReadyId']) && isset($_REQUEST['show'])) {

    $videoReadyId = $_REQUEST['videoReadyId'];

    $results = $VideoReady->getVideoReadyById($videoReadyId);
    if ($results) {
        echo json_encode(['id' => $results['id'], 'category_id' => $results['category_id'], 'title' => $results['title'], 'cover_image' => $results['cover_image'], 'video' => $results['video']]);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "video with given id not exist"]);
    return;
}

// delete record
else if (isset($_REQUEST['videoReadyId']) && isset($_REQUEST['delete'])) {

    $videoReadyId = $_REQUEST['videoReadyId'];

    $VideoReadyObj = $VideoReady->getVideoReadyById($videoReadyId);
    if ($VideoReadyObj) {
        $coverImage = $VideoReadyObj['cover_image'];
        $video = $VideoReadyObj['video'];
        $zip = $VideoReadyObj['zip'];
        $delete = $VideoReady->delete($videoReadyId);
        if ($delete) {
            if (file_exists("../$coverImage")) {
                unlink("../$coverImage");
            }
            if (file_exists("../$video")) {
                unlink("../$video");
            }
            echo json_encode(['status' => 1, 'message' => 'deleted']);
            return;
        }
        echo json_encode(['status' => 0, 'message' => "unable to delete video"]);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "video with given id not exist"]);
    return;
}