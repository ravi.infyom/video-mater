<?php
list($rootPath, $srcPath) = ['../..', '../../Src'];
require "$srcPath/middleware/authenticated.php";
require "$rootPath/bootstrap.php";

use Src\Models\VideoSubCategory;
use Src\Models\DeleteFile;

$category = new VideoSubCategory($dbConnection);
$df = new DeleteFile($dbConnection);

if (isset($_FILES['icon']) && isset($_REQUEST['uploadIcon'])) {
    $icon = $_FILES['icon']['name'];
    $icon_tmp = $_FILES['icon']['tmp_name'];

    $uniqueName = time() . '-icon-' . $icon;
    $path = "Src/storage/video_ready_category/icon/$uniqueName";
    $moved = move_uploaded_file($icon_tmp, "$rootPath/$path");
    if ($moved) {
        echo json_encode(['status' => 1, 'path' => $path]);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "unable to upload icon"]);
    return;
} else if (isset($_REQUEST['id'])) {

    $results = $category->getSubCategories();
    echo json_encode($results);
} else if (isset($_REQUEST['filePath']) && isset($_REQUEST['fieldName']) && isset($_REQUEST['deleteFile'])) {
    $file = $_REQUEST['filePath'];
    $subjectName = $_REQUEST['fieldName'];
    $df->addFile($file, 'video_ready_category', $subjectName);
    echo json_encode(['status' => 1]);
    return;
} else if (isset($_REQUEST['categoryNames'])) {

    $results = $category->getSubCategories([['status', '=', 'active']]);
    echo json_encode(['status' => 1, 'data' => $results]);
    return;
} else if (isset($_REQUEST['categoryName']) && isset($_REQUEST['byName'])) {
    $categoryName = $_REQUEST['categoryName'];
    $results = $category->getCategoryByName($categoryName);

    echo json_encode(['status' => 1, 'data' => $results]);
    return;
}
// add new category 
else if (isset($_REQUEST['subcategoryName']) && isset($_REQUEST['icon']) && isset($_REQUEST['category_id']) && isset($_REQUEST['insert'])) {

    $subcategoryName = $_REQUEST['subcategoryName'];
    $category_id = $_REQUEST['category_id'];

    $data = $category->getCategoryByName($subcategoryName);

    if (count($data) > 0) {
        echo json_encode(['status' => 0, 'error' => ['category' => 'Category with given name already exist']]);
        return;
    } else {
        $icon = $_REQUEST['icon'];

        $insert = $category->store(['name' => $subcategoryName, 'icon' => $icon, 'category_id' => $category_id]);
        if ($insert) {
            echo json_encode(['status' => 1, 'message' => 'success']);
            return;
        }
        echo json_encode(['status' => 0, 'message' => "can't add category"]);
        return;
    }
}
// update category
else if (isset($_REQUEST['subcategoryName']) && isset($_REQUEST['icon']) && isset($_REQUEST['category_id']) && isset($_REQUEST['subcategoryId'])  && isset($_REQUEST['update'])) {

    $subcategoryId = $_REQUEST['subcategoryId'];
    $subcategoryName = $_REQUEST['subcategoryName'];
    $category_id = $_REQUEST['category_id'];


    $categoryObj = $category->getCategoryById($subcategoryId);
    $oldIcon = $categoryObj['icon'];
    if (!$categoryObj) {
        echo json_encode(['status' => 0, 'error' => "Category does not exist you are trying to update", 'id' => $categoryId]);
        return;
    }

    $icon = $_REQUEST['icon'];

    $update = $category->update(['categoryId' => $subcategoryId, 'name' => $subcategoryName, 'icon' => $icon, 'category_id' => $category_id]);
    if ($update >= 0) {
        if ($icon !== $oldIcon) {
            if (file_exists("../$oldIcon")) {
                unlink("../$oldIcon");
            }
        }
        echo json_encode(['status' => 1, 'message' => 'success']);
        return;
    }
    echo json_encode(['status' => 0, 'error' => "can't update category"]);
    return true;
}
// delete category 
else if (isset($_REQUEST['categoryId']) && isset($_REQUEST['delete'])) {
    $categoryId = $_REQUEST['categoryId'];

    $isDeleteable = $category->isLinkedToOther($categoryId);
    if ($isDeleteable) {
        $categoryObj = $category->getCategoryById($categoryId);
        if ($categoryObj) {
            $path = $categoryObj['icon'];
            $delete = $category->delete($categoryId);
            if ($delete) {
                if (file_exists("../$path")) {
                    unlink("../$path");
                }
                echo json_encode(['status' => 1, 'message' => 'deleted']);
                return;
            }
            echo json_encode(['status' => 0, 'message' => "unable to delete category"]);
            return;
        }
        echo json_encode(['status' => 0, 'message' => "category not exist"]);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "Can't delete, catgory linked with videos"]);
    return;
}
// get category 
else if (isset($_REQUEST['categoryId']) && isset($_REQUEST['show'])) {

    $categoryId = $_REQUEST['categoryId'];

    $results = $category->getCategoryById($categoryId);
    if ($results) {
        echo json_encode(['id' => $results['id'], 'name' => $results['name'], 'icon' => $results['icon'], 'category_id' => $results['category_id']]);
        return;
    }
    echo json_encode(['status' => 0, 'message' => "category with given id not exist"]);
    return;
} 
else if (isset($_REQUEST['getCategoryCount'])) {

    $results = $category->getSubCategoryCount();
    if ($results) {
        echo json_encode(['count' => $results]);
        return;
    }
    echo json_encode(['count' => 20]);
    return;
} else {
    echo json_encode([['status' => 0, 'message' => "Invlid request data"]]);
}