<?php $sessionName = $_ENV['SESSION_NAME'] ?>
<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-success elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo $rootPath . '/index' ?>" class="brand-link">
        <img src="<?php echo $srcPath . '/dist/img/AdminLTELogo.png' ?>" alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3" style="opacity: .8">
        <span class="brand-text font-weight-light">Status App</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <?php if (isset($_SESSION[$sessionName]['user']) && isset($_SESSION[$sessionName]['user']['avatar'])) {
                ?>
<!--                <img src="--><?php //echo $rootPath . '/' . $_SESSION[$sessionName]['user']['avatar'] ?><!--"-->
<!--                    class="img-circle elevation-2" alt="User Image">-->
                <?php
                } else {
                ?>
                <img src="<?php echo $srcPath . '/dist/img/avatar3.png' ?>" class="img-circle elevation-2"
                    alt="User Image">
                <?php
                } ?>

            </div>
            <div class="info">
                <a href="<?php echo $rootPath . '/Views/settings' ?>"
                    class="d-block"><?php echo $_SESSION[$sessionName]['user'] ? $_SESSION[$sessionName]['user']['name'] : '--'; ?></a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

                <li class="nav-item">
                    <a href="<?php echo $rootPath . '/index' ?>" class="nav-link 
                      <?php if ($active == 'dashboard') {
                            echo 'active';
                        } ?>">
                        <i class="fa fa-tachometer-alt nav-icon"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <?php if(filter_var($_ENV['ENABLE_LANGUAGES'], FILTER_VALIDATE_BOOLEAN)){  ?>
                <li class="nav-item">
                    <a href="<?php echo $rootPath . '/Views/language' ?>" class="nav-link 
                      <?php if ($active == 'language') {
                            echo 'active';
                        } ?>">
                        <i class="fa fa-language nav-icon"></i>
                        <p>Language</p>
                    </a>
                </li>
                <?php } ?>
                <li class="nav-item">
                    <a href="<?php echo $rootPath . '/Views/category' ?>" class="nav-link 
                      <?php if ($active == 'video_category') {
                            echo 'active';
                        } ?>">
                        <i class="fa fa-cubes nav-icon"></i>
                        <p>Video Category</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo $rootPath . '/Views/video_sub_category' ?>" class="nav-link 
                      <?php if ($active == 'video_sub_category') {
                            echo 'active';
                        } ?>">
                        <i class="fas fa-list nav-icon"></i>
                        <p>Video Sub Category</p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="<?php echo $rootPath . '/Views/video_effect' ?>" class="nav-link 
                      <?php if ($active == 'video_effect') {
                            echo 'active';
                        } ?>">
                        <i class="fa fa-video-camera  nav-icon"></i>
                        <p>Video Effect</p>
                    </a>
                </li>
                <?php 
                   if(filter_var($_ENV['ENABLE_VIDEO_READY'], FILTER_VALIDATE_BOOLEAN)){
                ?>
                <li class="nav-item">
                    <a href="<?php echo $rootPath . '/Views/video_ready_category' ?>" class="nav-link 
                      <?php if ($active == 'video_ready_category') {
                            echo 'active';
                        } ?>">
                        <i class="fa fa-cubes nav-icon"></i>
                        <p>Video Ready Category</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo $rootPath . '/Views/video_ready' ?>" class="nav-link 
                      <?php if ($active == 'video_ready') {
                            echo 'active';
                        } ?>">
                        <i class="fa fa-video-camera  nav-icon"></i>
                        <p>Video Ready</p>
                    </a>
                </li>
                <?php }?>
                <li class="nav-item">
                    <a href="<?php echo $rootPath . '/Views/audio_category' ?>" class="nav-link 
                      <?php if ($active == 'audio_category') {
                            echo 'active';
                        } ?>">
                        <i class="fa fa-cubes  nav-icon"></i>
                        <p>Audio Category</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo $rootPath . '/Views/audio' ?>" class="nav-link 
                      <?php if ($active == 'audio') {
                            echo 'active';
                        } ?>">
                        <i class="fa fa-music  nav-icon"></i>
                        <p>Audio</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo $rootPath . '/Views/ad' ?>" class="nav-link 
                      <?php if ($active == 'ad') {
                            echo 'active';
                        } ?>">
                        <i class="fa fa-ad  nav-icon"></i>
                        <p>Custom Ad</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo $rootPath . '/Views/notification' ?>" class="nav-link 
                      <?php if ($active == 'notification') {
                            echo 'active';
                        } ?>">
                        <i class="fa fa-bell  nav-icon"></i>
                        <p>Notification</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="<?php echo $rootPath . '/Views/settings' ?>" class="nav-link 
                      <?php if ($active == 'settings') {
                            echo 'active';
                        } ?>">
                        <i class="fa fa-tools  nav-icon"></i>
                        <p>Settings</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>