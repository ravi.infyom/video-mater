<style>
    .required {
        color: red;
    }
    
    .hideMe {
        display: none !important;
    }
</style>

<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>

    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        
        <li class="nav-item">
            <a class="nav-link" onclick="logout()" role="button" title="logout"><i class="fas fa-sign-out-alt"></i></a>
        </li>
    </ul>
</nav>
<!-- /.navbar -->
<script type="text/javascript">
    function logout() {
        // alert(email+psw);
        let app_url = "<?php echo $_ENV['APP_URL'] ?>"


        Swal.fire({
            title: 'Are you sure?',
            text: "You will be logged out after this action",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Logout',
            cancelButtonText:'No',
            customClass: {
                content: 'text-danger'
            },
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "post",
                    url: "<?php echo $srcPath . '/ajax/auth.php' ?>",
                    data: {
                        logout: true
                    },
                    success: function(data) {
                        if (data == 'logout_success') {
                            location.replace(app_url + '/login.php');
                        }
                    }
                });
            }
        })
    }
</script>