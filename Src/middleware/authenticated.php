<?php
require $rootPath . '/bootstrap.php';

if (!session_id()) {
    session_start();
}

$sessionName = $_ENV['SESSION_NAME'];
if (isset($_SESSION[$sessionName]) && isset($_SESSION[$sessionName]['user']['id'])) {
} else {
    session_destroy();
    header("Location: $rootPath/login.php");
}