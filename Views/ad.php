<?php
list($rootPath, $srcPath) = ['..', '../Src'];
require_once($srcPath . '/middleware/authenticated.php');
require "../bootstrap.php";
$page = 'Custom Ad';
$active = 'ad';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $_ENV['APP_NAME']; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo $rootPath . '/favicon.png' ?>" type="image/png" sizes="16x16">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/fontawesome-free/css/all.min.css' ?>">
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/adminlte.min.css' ?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/overlayScrollbars/css/OverlayScrollbars.min.css' ?>">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css' ?>">
    <link href="<?php echo $srcPath . '/plugins/datatables-responsive/css/responsive.bootstrap4.min.css' ?>"
        rel="stylesheet">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <?php
        require_once($srcPath . '/components/navbar.php')
        ?>

        <!-- Main Sidebar Container -->
        <?php
        require_once($srcPath . '/components/main_sidebar.php')
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <h1 class="ml-2 text-dark"><?php echo $page; ?></h1>

                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div >
                        <div class="card">
                            <div class="card-header">
                                <button type="button"  class="btn btn-success" style="width:95px;" data-toggle="modal" data-target="#insertmyModal">
                                    Add
                                </button>
                                <div class="row">
                                    <div class="col-md-6">
                                        Ad List
                                    </div>
                                    <div class="col-md-6">
                                        <a role="button" href="manage_ad.php"
                                            class="btn btn-sm btn-success float-right text-white">
                                            <i class="fa fa-plus"> Add</i>
                                        </a>
                                    </div>

                                    <!-- /.col -->
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="ad_table" class="table  table-bordered table-hover"
                                            cellspacing="0" width="100%">
                                            <thead class="thead-dark">
                                                <tr>
                                                    <th>Id</th>
                                                    <th>Ad Name</th>
                                                    <th>Ad Slogan</th>
                                                    <th>Company Name</th>
                                                    <th>Ad Type</th>
                                                    <th>Package Name</th>
                                                    <th>Icon</th>
                                                    <th>Redirect Url</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody id="ad_table_body">

                                            </tbody>
                                        </table>

                                    </div>

                                    <!-- /.col -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <div>
            <div class="modal" id="deleteModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body text-center mb-5">
                            <img src="<?php echo $srcPath . '/dist/img/right.png' ?>" class="img-responsive">
                            <h1>Are You Sure?</h1>
                            <p>Do you really want to delete these ad? This process cannot be undone.</p>
                            <div class="btn-group">
                                <input type="hidden" value="" id="delete_id">
                                <button type="button" class="btn btn-secondary btn-lg mr-2 rounded-lg"
                                    data-dismiss="modal">Cancel</button>
                                <button type="button" id="deleteAd" data-dismiss="modal"
                                    class="btn btn-danger btn-lg rounded-lg">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



            <div class="modal" id="insertmyModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h4 class="modal-title">Add Ad</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <form name="frminsert" id="frminsert" enctype="multipart/form-data" method="post">
                            <div class="modal-body">
                                <label for="">Ad Name</label>
                                <input type="text" name="adName" required id="adName" class="form-control" />
                                <span id="fname-error" class="text-danger"></span></br>

                                <label for="">Ad Slogan</label>
                                <input type="text" name="adSlogan" required id="adSlogan" class="form-control" />
                                <span id="fname-error" class="text-danger"></span></br>

                                <label for="">Company Name</label>
                                <input type="text" name="companyName" required id="companyName" class="form-control" />
                                <span id="fname-error" class="text-danger"></span></br>

                                <label for="">Ad Type</label>
                                <input type="radio" name="adType" id="adType" required value="Web" class="form-control" />Web
                                <input type="radio" name="adType" id="adType" required  value="Play Store" class="form-control" />Play Store
                                <span id="fname-error" class="text-danger"></span></br>

                                <label for="">Package Name</label>
                                <input type="text" name="packageName" id="packageName" required class="form-control" />
                                <span id="fname-error" class="text-danger"></span></br>

                                <label for="">Add Icon</label>
                                <input type="file" name="adIcon" id="adIcon" class="form-control" />
                                <img id="txtImageshow" src="#" width="120px">
                                <span id="image-error" class="text-danger"></span></br>

                                <label for="">Redirect Url</label>
                                <input type="text" name="redirectUrl" id="redirectUrl" required class="form-control" />
                                <span id="tag_error" class="text-danger"></span></br>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" id="insert" name="insert" value="Save" class="btn btn-success" />
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h4 class="modal-title">Update Ad</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <form name="frmupdate" id="frmUpdate" enctype="multipart/form-data" method="post">

                            <div class="modal-body">
                                <label for="">Ad Name</label>
                                <input type="text" name="editAdName" required id="editAdName" class="form-control" />
                                <span id="fname-error" class="text-danger"></span></br>

                                <label for="">Ad Slogan</label>
                                <input type="text" name="editAdSlogan" required id="editAdSlogan" class="form-control" />
                                <span id="fname-error" class="text-danger"></span></br>

                                <label for="">Company Name</label>
                                <input type="text" name="editCompanyName" required id="editCompanyName" class="form-control" />
                                <span id="fname-error" class="text-danger"></span></br>

                                <label for="">Ad Type</label>
                                <input type="radio" name="editAdType" id="editAdType" required value="Web" class="form-control" />Web
                                <input type="radio" name="editAdType" id="editAdType" required  value="Play Store" class="form-control" />Play Store
                                <span id="fname-error" class="text-danger"></span></br>

                                <label for="">Package Name</label>
                                <input type="text" name="editPackageName" id="editPackageName" required class="form-control" />
                                <span id="fname-error" class="text-danger"></span></br>

                                <label for="">Add Icon</label>
                                <input type="file" name="editAdIcon" id="editAdIcon" class="form-control" />
                                <img id="txtImageshow" src="#" width="120px">
                                <span id="image-error" class="text-danger"></span></br>

                                <label for="">Redirect Url</label>
                                <input type="text" name="editRedirectUrl" id="editRedirectUrl" required class="form-control" />
                                <span id="tag_error" class="text-danger"></span></br>
                                <input type="hidden" name="categoryId"  id="edit_hidden_update_id" />
                            </div>

                            <div class="modal-footer">
                                <button type="submit" value="Update" id="btnmodalupdate" name="update" class="btn btn-success" >Update</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>

                    </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <!-- ./wrapper -->
    <!-- jQuery -->
    <script src="<?php echo $srcPath . '/plugins/jquery/jquery.min.js' ?>"></script>

    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo $srcPath . '/plugins/jquery-ui/jquery-ui.min.js' ?>"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo $srcPath . '/plugins/bootstrap/js/bootstrap.bundle.min.js' ?>"></script>
    <!-- overlayScrollbars -->
    <script src="<?php echo $srcPath . '/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $srcPath . '/dist/js/adminlte.js' ?>"></script>
    <script src="<?php echo $srcPath . '/dist/js/demo.js' ?>"></script>
    <script src="https://kit.fontawesome.com/a044a561b3.js" crossorigin="anonymous"></script>
    
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.css' ?>">
    <script src=" <?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.js' ?>"> </script>

    <!-- DataTables -->
    <script src="<?php echo $srcPath . '/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/datatables-responsive/js/dataTables.responsive.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/datatables-responsive/js/responsive.bootstrap4.min.js' ?>"></script>
    
    <script src=" <?php echo $srcPath . '/actions/ad.js' ?>"></script>
    <script>
    $(document).ready(function() {
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })
    });
    </script>

</body>

</html>