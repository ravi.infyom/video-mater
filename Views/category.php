<?php
list($rootPath, $srcPath) = ['..', '../Src'];
require_once($srcPath . '/middleware/authenticated.php');

require "../bootstrap.php";
$page = 'Video Category';
$active = 'video_category';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $_ENV['APP_NAME']; ?>| Category</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo $rootPath . '/favicon.png' ?>" type="image/png" sizes="16x16">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/fontawesome-free/css/all.min.css' ?>">
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/adminlte.min.css' ?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/overlayScrollbars/css/OverlayScrollbars.min.css' ?>">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css' ?>">
    <link href="<?php echo $srcPath . '/plugins/datatables-responsive/css/responsive.bootstrap4.min.css' ?>" rel="stylesheet">

</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <?php
        require_once($srcPath . '/components/navbar.php')
        ?>

        <!-- Main Sidebar Container -->
        <?php
        require_once($srcPath . '/components/main_sidebar.php')
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <h1 class="ml-2 text-dark"><?php echo $page; ?></h1>

                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">

                    <div class="card">
                        <div class="card-header">
                            <div class="row justify-content-end">
                            <button type="button"  class="btn btn-success" style="width:95px;" data-toggle="modal" data-target="#insertmyModal">
                                Add
                            </button>
                            </div>
                            <div class="row justify-content-center">
                                <ul class="nav nav-tabs nav-pills" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="datatable-tab" data-toggle="tab" href="#datatable" role="tab" aria-controls="datatable" aria-selected="false">Video Category List</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link px-4" id="add-tab" data-toggle="tab" href="#add" role="tab" aria-controls="add" aria-selected="true">Add</a>
                                    </li>
                                </ul>
                                <!-- /.col -->
                            </div>
                        </div>
                        <div class="">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">

                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="datatable" role="tabpanel" aria-labelledby="datatable-tab">
                                            <table id="category_table" class="table table-striped  table-bordered table-hover" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Name</th>
                                                        <th>Icon</th>
                                                        <th>Priority</th>
                                                        <th>Status</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="category_table_body" class="category_form">

                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane fade " id="add" role="tabpanel" aria-labelledby="add-tab">
                                            <div class="row justify-content-center">
                                                <div class="col-md-8 col-sm-12">
                                                    <form id="categoryForm" class="form-horizontal">
                                                        <div>
                                                            <div class="form-group row">
                                                                <div class="col-2 ">
                                                                    <label for="category" class="col-form-label">Category<span class="required">*</span></label>
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <input type="text" name="category" id="category" class="form-control" value="">
                                                                    <label id="category-error" class="text-danger" for="category"></label>

                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-2 d-flex align-items-center ">
                                                                    <label for="icon" class="col-form-label">Select Icon</label>

                                                                </div>
                                                                <div class="col-10 d-flex">
                                                                    <div class="order-1">
                                                                        <img src="<?php echo $srcPath . '/dist/img/add-plus-2.png' ?>" class="form-control" id="icon_preview" style="max-height: 280px; height: 130px; width: 130px; border-radius: 50%" data-toggle="tooltip" data-placement="right" title="upload icon" style="cursor: pointer; max-height: 280px; height: 200px; width: 200px;">
                                                                        <input type="file" id="icon" name="icon" accept="image/*" class="hideMe">
                                                                        <label id="icon-error" class="text-danger" for="icon"></label>
                                                                        <label id="image-error" class="text-danger"></label>
                                                                    </div>
                                                                    <div class="order-2 d-flex align-items-center">
                                                                        <span class="mx-3 remove_icon hideMe"><button id="remove_icon" class="fa fa-minus-circle btn btn-danger btn-circle" data-toggle="tooltip" data-placement="right" title="remove icone"></button></span>

                                                                        <input type="hidden" name="icon_hidden" id="icon_hidden" value="">
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-2 ">
                                                                    <label for="status" class="col-form-label">Status</label>
                                                                </div>
                                                                <div class="col-10">

                                                                    <select class="form-control" style="width: auto;margin-bottom:2px;" name="status" id="status">
                                                                        <option value="active" selected>Active</option>
                                                                        <option value="deactive">Deactive</option>
                                                                    </select>
                                                                    <label id="status-error" class="text-danger" for="status"></label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <div class="col-2 ">
                                                                    <label for="priority" class="col-form-label">Priority</label>
                                                                </div>
                                                                <div class="col-10">

                                                                    <select class="form-control" style="width: auto;margin-bottom:2px;" name="priority" id="priority" disabled>
                                                                        <option value=''>-- Choose Priority --</option>
                                                                    </select>
                                                                    <label id="priority-error" class="text-danger" for="priority"></label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <p id="common-error" class="text-danger"></p>
                                                            </div>
                                                            <div class="form-group ">
                                                                <div class="col-12  text-center">
                                                                    <button class="btn btn-success" id="addCategory" value="add">Add</button>
                                                                    <a role="button" href="category.php" class="btn btn-danger  text-white">
                                                                        Back
                                                                    </a>
                                                                    <input type="hidden" id="masterId" value="" /><br />

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    </div>
                                </div>

                                <!-- /.col -->
                            </div>
                        </div>
                    </div>


                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <div>
            <div class="modal" id="deleteModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body text-center mb-5">
                            <img src="<?php echo $srcPath . '/dist/img/right.png' ?>" class="img-responsive">
                            <h1>Are You Sure?</h1>
                            <p>Do you really want to delete these category? This process cannot be undone.</p>
                            <div class="btn-group">
                                <input type="hidden" value="" id="delete_id">
                                <button type="button" class="btn btn-secondary btn-lg mr-2 rounded-lg" data-dismiss="modal">Cancel</button>
                                <button type="button" id="deleteCategory" data-dismiss="modal" class="btn btn-danger btn-lg rounded-lg">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




            <div class="modal" id="insertmyModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h4 class="modal-title">Add Video Category</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <form name="frminsert" id="frminsert" enctype="multipart/form-data" method="post">
                            <div class="modal-body">
                                <label for="">Category</label>
                                <input type="text" name="categoryName" required id="txt-name" class="form-control" />
                                <span id="fname-error" class="text-danger"></span></br>

                                <label for="">Image</label>
                                <input type="file" name="icon" id="txt-Image" class="form-control" />
                                <img id="txtImageshow" src="#" width="120px">
                                <span id="image-error" class="text-danger"></span></br>

                                <label for="">Status</label>
                                <select class="form-control" id="status" name="status">
                                    <option id="1" value="active" >Active</option>
                                    <option id="2" value="deactive" >Deactive</option>
                                </select>
                                <span id="tag_error" class="text-danger"></span></br>
                            </div>

                            <div class="modal-footer">
                                <input type="submit" id="insert" name="insert" value="Save" class="btn btn-success" />
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>                            </div>
                        </form>
                    </div>
                </div>
            </div>



            <div class="modal" id="myModal">
                <div class="modal-dialog">
                    <div class="modal-content">

                        <div class="modal-header">
                            <h4 class="modal-title">Update Video Category</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <form name="frmupdate" id="frmUpdate" enctype="multipart/form-data" method="post">


                            <div class="modal-body">
                                <label for="">Category</label>
                                <input type="text" name="editCategoryName" required id="edit-txt-name" class="form-control" />
                                <span id="fname-error" class="text-danger"></span></br>

                                <label for="">Image</label>
                                <input type="file" name="editIcon" id="edit-txt-Image" class="form-control" />
                                <img id="txtImageshow" src="#" width="120px">
                                <span id="image-error" class="text-danger"></span></br>

                                <label for="">Status</label>
                                <select class="form-control"  name="editStatus">
                                    <option id="1" value="active" id="edit-status">Active</option>
                                    <option id="2" value="deactive" id="edit-status">Deactive</option>
                                </select>
                                <span id="tag_error" class="text-danger"></span></br>
                                <input type="hidden" name="categoryId"  id="edit_hidden_update_id" />
                            </div>

                            <div class="modal-footer">
                                <button type="submit" value="Update" id="btnmodalupdate" name="update" class="btn btn-success" >Update</button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                            </div>

                        </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <!-- ./wrapper -->
    <!-- jQuery -->
    <script src="<?php echo $srcPath . '/plugins/jquery/jquery.min.js' ?>"></script>

    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo $srcPath . '/plugins/jquery-ui/jquery-ui.min.js' ?>"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo $srcPath . '/plugins/bootstrap/js/bootstrap.bundle.min.js' ?>"></script>
    <!-- overlayScrollbars -->
    <script src="<?php echo $srcPath . '/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $srcPath . '/dist/js/adminlte.js' ?>"></script>
    <script src="<?php echo $srcPath . '/dist/js/demo.js' ?>"></script>
    <script src="https://kit.fontawesome.com/a044a561b3.js" crossorigin="anonymous"></script>
    <script src="<?php echo $srcPath . '/plugins/select2/js/select2.full.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/actions/manage_category.js' ?>"></script>

    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.css' ?>">
    <script src=" <?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.js' ?>"> </script>

    <!-- DataTables -->
    <script src="<?php echo $srcPath . '/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/datatables-responsive/js/dataTables.responsive.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/datatables-responsive/js/responsive.bootstrap4.min.js' ?>"></script>
    <script src=" <?php echo $srcPath . '/actions/category.js' ?>"></script>
    <script>
        $(document).ready(function() {
            $(function() {
                $('[data-toggle="tooltip"]').tooltip()
            })
        });
    </script>

</body>

</html>