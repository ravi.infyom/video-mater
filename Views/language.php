<?php
list($rootPath, $srcPath) = ['..', '../Src'];
require_once($srcPath . '/middleware/authenticated.php');
require "../bootstrap.php";
$page = 'Language';
$active = 'language';
// currently language tab is not required so, redirect to dashboard
if (!filter_var($_ENV['ENABLE_LANGUAGES'], FILTER_VALIDATE_BOOLEAN)) {
    header("Location: $rootPath/index");
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo  $_ENV['APP_NAME']; ?>| Language</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo $rootPath . '/favicon.png' ?>" type="image/png" sizes="16x16">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/fontawesome-free/css/all.min.css' ?>">
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/adminlte.min.css' ?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/overlayScrollbars/css/OverlayScrollbars.min.css' ?>">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css' ?>">
    <link rel="stylesheet"
        href="<?php echo $srcPath . '/plugins/datatables-responsive/css/responsive.bootstrap4.min.css' ?>">


    <style>

    </style>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <?php
        require_once($srcPath . '/components/navbar.php')
        ?>

        <!-- Main Sidebar Container -->
        <?php
        require_once($srcPath . '/components/main_sidebar.php')
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <h1 class="ml-2 text-dark"><?php echo $page; ?></h1>

                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div id="language_form">
                        <div class="card" hide="true">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-6">

                                        Manage Language

                                    </div>
                                    <div class="col-md-6">

                                        <button class="btn btn-sm btn-success float-right" id="open_language"><i
                                                class="fas fa-plus"></i></button>
                                        <button class="btn btn-sm btn-danger float-right" id="close_language"
                                            style="display:none;"><i class="fas fa-times"></i></button>

                                    </div>

                                    <!-- /.col -->
                                </div>

                            </div>
                            <div class="card-body" id="language_body" style="display:none;">
                                <div class="container">
                                    <form role="form" id="language_form">
                                        <div class="row">

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="language_name">Language</label>
                                                    <input type="text" class="form-control" id="language_name" required>
                                                </div>
                                                <label id='ln' class='text-danger'></label><br />
                                                <button class="btn btn-success" id="submit_language"
                                                    value="add">Submit</button>
                                                <input type="hidden" id="masterId" value="" />
                                            </div>
                                        </div><!-- row -->

                                    </form>
                                </div><!-- container -->

                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-header">
                            <h3> Language List</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="language_table" class="table table-striped table-bordered table-hover"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Language</th>
                                                <th>Action</th>

                                            </tr>
                                        </thead>
                                        <tbody id="language_table_body">

                                        </tbody>
                                    </table>
                                    <!-- <table id="language_table" class="display" width="100%"></table> -->
                                </div>

                                <!-- /.col -->
                            </div>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php
        // require_once('components/footer.php')
        ?>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->
    <!-- jQuery -->
    <script src="<?php echo $srcPath . '/plugins/jquery/jquery.min.js' ?>"></script>

    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo $srcPath . '/plugins/jquery-ui/jquery-ui.min.js' ?>"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo $srcPath . '/plugins/bootstrap/js/bootstrap.bundle.min.js' ?>"></script>
    <!-- overlayScrollbars -->
    <script src="<?php echo $srcPath . '/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $srcPath . '/dist/js/adminlte.js' ?>"></script>
    <script src="<?php echo $srcPath . '/dist/js/demo.js' ?>"></script>
    <script src="https://kit.fontawesome.com/a044a561b3.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.css' ?>">
    <script src=" <?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.js' ?>"> </script>
    
    <!-- DataTables -->
    <script src="<?php echo $srcPath . '/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/datatables-responsive/js/dataTables.responsive.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"' ?>""></script>

    <script src=" <?php echo $srcPath . '/actions/language.js' ?>"></script>
    <script>

    </script>
</body>

</html>