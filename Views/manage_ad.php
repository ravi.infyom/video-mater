<?php
list($rootPath, $srcPath) = ['..', '../Src'];
require_once $srcPath . '/middleware/authenticated.php';
require "../bootstrap.php";
$page = 'Manage Ad';
$active = '';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        <?php echo $_ENV['APP_NAME'] ?>
    </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo $rootPath . '/favicon.png' ?>" type="image/png" sizes="16x16">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/fontawesome-free/css/all.min.css' ?>">
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/adminlte.min.css' ?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/overlayScrollbars/css/OverlayScrollbars.min.css' ?>">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/custom/ad.css' ?>">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">
    <?php require_once $srcPath . '/components/navbar.php' ?>
    <!-- Main Sidebar Container -->
    <?php require_once $srcPath . '/components/main_sidebar.php' ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">

            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div id="ad_form" class="" style="padding: 10px 16%">
                    <!-- Horizontal Form -->
                    <div class="card ad-card">
                        <div class="card-header">
                            <h3 id="ad_header">
                                <?php echo $page ?>
                            </h3>
                            <!-- </div> -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" id="ad_body">
                            <!-- form start -->
                            <form class="form-horizontal ad-form disableForm" id="adForm">
                                <div class="form-group row">
                                    <div class="col-2 ">
                                        <label for="ad_name" class="col-form-label">Ad Name<span
                                                    class="required">*</span></label>
                                    </div>
                                    <div class="col-10">
                                        <input type="text" class="form-control" name="ad_name" id="ad_name">
                                        <label id="ad_name-error" class="text-danger" for="ad_name"></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-2 ">
                                        <label for="ad_slogan" class="col-form-label">Ad Slogan</label>
                                    </div>
                                    <div class="col-10">
                                        <input type="text" class="form-control" rows="5" id="ad_slogan">
                                        <label id="ad_slogan-error" class="text-danger" for="ad_slogan"></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-2 ">
                                        <label for="company_name" class="col-form-label">Company Name</label>
                                    </div>
                                    <div class="col-10">
                                        <input type="text" class="form-control" rows="5" id="company_name">
                                        <label id="company_name-error" class="text-danger"
                                               for="company_name"></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-2 ">
                                        <label for="company_name" class="col-form-label">Ad Type</label>
                                    </div>
                                    <div class="col-10">
                                        <div class="row mb-2">
                                            <div class="col-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" class="custom-control-input" value="Web"
                                                           id="Web" name="ad_type" checked>
                                                    <label class="custom-control-label" for="Web">Web</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" class="custom-control-input" value="play_store"
                                                           id="play_store" name="ad_type">
                                                    <label class="custom-control-label" for="play_store">Play
                                                        Store</label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-2 ">
                                        <label for="package_name" class="col-form-label">Package Name</label>
                                    </div>
                                    <div class="col-10">
                                        <input type="text" class="form-control" rows="5" id="package_name">
                                        <label id="package_name-error" class="text-danger"
                                               for="package_name"></label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-2 ">
                                        <label class="col-form-label">Add Icon<span
                                                    class="required">*</span></label>
                                    </div>
                                    <div class="col-10">
                                        <div class="row mb-2">
                                            <div class="col-12">
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" class="custom-control-input" value="url"
                                                           id="url" name="ad_icon_type" checked>
                                                    <label class="custom-control-label" for="url">Url</label>
                                                </div>
                                                <div class="custom-control custom-radio custom-control-inline">
                                                    <input type="radio" class="custom-control-input" value="upload"
                                                           id="upload" name="ad_icon_type">
                                                    <label class="custom-control-label" for="upload">Upload</label>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="row mb-2" id="url_div">
                                            <div class="col-12">
                                                <input type="text" class="form-control" name="ad_icon_url"
                                                       id="ad_icon_url">
                                                <label id="ad_icon_url-error" class="text-danger"
                                                       for="ad_icon_url"></label>
                                            </div>
                                        </div>
                                        <div class="row hideMe" id="upload_div">
                                            <div class="col-12">
                                                <img src="<?php echo $srcPath . '/dist/img/add-plus-2.png' ?>"
                                                     class="form-control" id="icon_preview"
                                                     style="max-height: 280px; height: 130px; width: 130px; border-radius: 50%"
                                                     data-toggle="tooltip" data-placement="right"
                                                     title="upload icon"
                                                     style="cursor: pointer; max-height: 280px; height: 200px; width: 200px;">
                                                <input type="file" id="ad_icon_image" name="ad_icon_image"
                                                       accept="image/*" class="hideMe">
                                                <label id="ad_icon_image-error" class="text-danger"
                                                       for="ad_icon_image"></label>
                                                <input type="hidden" name="ad_icon_hidden" id="ad_icon_hidden"
                                                       value="">

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-2 ">
                                        <label for="redirect_url" class="col-form-label">Redirect Url<span
                                                    class="required">*</span></label>
                                    </div>
                                    <div class="col-10">
                                        <input type="text" class="form-control" name="redirect_url"
                                               id="redirect_url">
                                        <label id="redirect_url-error" class="text-danger"
                                               for="redirect_url"></label>
                                    </div>
                                </div>
                                <div class="form-group ">
                                    <div class="col-12  text-center">
                                        <button type="button" id="submitAd" value="add"
                                                class="btn btn-primary">Submit
                                        </button>
                                        <a role="button" href="ad.php" class="btn btn-success text-white">Back</a>
                                        <input type="hidden" id="masterId" value=""/><br/>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </section>
    </div>

</div>
<!-- /.content-wrapper -->
<?php
// require_once('components/footer.php')
?>

<!-- Control Sidebar -->
<aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
</aside>
<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-- jQuery -->
<script src="<?php echo $srcPath . '/plugins/jquery/jquery.min.js' ?>"></script>

<!-- jQuery UI 1.11.4 -->
<script src="<?php echo $srcPath . '/plugins/jquery-ui/jquery-ui.min.js' ?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?php echo $srcPath . '/plugins/bootstrap/js/bootstrap.bundle.min.js' ?>"></script>
<!-- overlayScrollbars -->
<script src="<?php echo $srcPath . '/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js' ?>"></script>


<link rel="stylesheet" href="<?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.css' ?>">
<script src=" <?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.js' ?>">
    <
    !--AdminLTE
    App-- >
    <
    script
    src = "<?php echo $srcPath . '/dist/js/adminlte.js' ?>" >
</script>
<script src="https://kit.fontawesome.com/a044a561b3.js" crossorigin="anonymous"></script>
<script src="<?php echo $srcPath . '/actions/manage_ad.js' ?>"></script>
</body>

</html>