<?php
list($rootPath, $srcPath) = ['..', '../Src'];
require_once($srcPath . '/middleware/authenticated.php');
require "../bootstrap.php";
$page = 'Manage Audio';
$active = '';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $_ENV['APP_NAME'] ?>| Audio</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo $rootPath . '/favicon.png' ?>" type="image/png" sizes="16x16">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/fontawesome-free/css/all.min.css' ?>">
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/adminlte.min.css' ?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/overlayScrollbars/css/OverlayScrollbars.min.css' ?>">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/select2/css/select2.min.css' ?>" />
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/ekko-lightbox/ekko-lightbox.css' ?>" />
    <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/custom/audio.css' ?>">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <?php
        require_once($srcPath . '/components/navbar.php')
        ?>

        <!-- Main Sidebar Container -->
        <?php
        require_once($srcPath . '/components/main_sidebar.php')
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <!-- <div class="row mb-2">
                        <h1 class="ml-2 text-dark"><?php //echo $page; 
                                                    ?></h1>
                    </div> -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div id="audio_form" class="" style="padding: 10px 16%">
                        <!-- Horizontal Form -->
                        <div class="card audio-card">
                            <div class="card-header">
                                <h3 id="audio_header"><?php echo $page ?></h3> <!-- </div> -->
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body" id="audio_body">
                                <!-- form start -->
                                <form class="form-horizontal audio-form" id="audioForm">
                                    <div class="form-group row">
                                        <div class="col-2 ">
                                            <label for="audio_category" class="col-form-label">Audio Category<span
                                                    class="required">*</span></label>
                                        </div>
                                        <div class="col-10">
                                            <select class="select2" style="width: 100%;margin-bottom:2px;"
                                                name="audio_category" id="audio_category">
                                            </select>
                                            <label id="audio_category-error" class="text-danger"
                                                for="audio_category"></label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-2 ">
                                            <label for="name" class="col-form-label">Name<span
                                                    class="required">*</span></label>
                                        </div>
                                        <div class="col-10">
                                            <input type="text" class="form-control" name="name" id="name">
                                            <label id="name-error" class="text-danger" for="name"></label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-2 d-flex align-items-center ">
                                            <label for="audio" class="col-form-label">Audio
                                                <span class="required">*</span>
                                            </label>
                                        </div>
                                        <div class="col-10 d-flex">
                                            <div class="order-1">
                                                <img src="<?php echo $srcPath . '/dist/img/mp3-4.jpg' ?>"
                                                    class="add_audio" id="audio_image" data-toggle="tooltip"
                                                    title="upload audio" data-placement="right"
                                                    style="cursor: pointer; max-height: 280px; height: 130px; width: 130px; border-radius: 50%"
                                                    data-s3="1">
                                                <input type="file" id="audio" name="audio" accept="audio/*"
                                                    class="hideMe">
                                                <label id="audio-error" class="text-danger" for="audio"></label>
                                            </div>
                                            <div class="order-2 d-flex align-items-center">
                                                <span class="mx-3 remove_audio hideMe"><button id="remove_audio"
                                                        class="fa fa-minus-circle btn btn-danger btn-circle"
                                                        data-toggle="tooltip" data-placement="right"
                                                        title="remove audio"></button></span>
                                                <input type="hidden" id="audio_hidden" value="">
                                                <div class="audio-spinner hideMe text-center">
                                                    <span> <i class="fa fa-spinner fa-2x fa-spin "></i></span><br />
                                                    <span class="text-success">Uploading...</span>
                                                </div>

                                                <div>
                                                    <audio controls src="" id="audio_preview" class="hideMe"
                                                        style="height:30px">
                                                        Your browser does not support the
                                                        <code>audio</code> element.
                                                    </audio>
                                                </div>
                                                <!-- <audio controls>
                                                    <source
                                                        src="../storage/audio/audio/1592761058-audio-file_example_MP3_2MG.mp3">
                                                    Your browser does not support the audio element.
                                                </audio> -->
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <div class="col-12  text-center">
                                            <button type="button" id="submitAudio" value="add"
                                                class="btn btn-primary">Submit</button>
                                            <a role="button" href="audio.php" class="btn btn-success text-white">
                                                Back
                                            </a>
                                            <input type="hidden" id="masterId" value="" /><br />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </section>
        </div>
        <!-- /.content -->
        <div>
            <div class="modal" id="deleteFileModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body text-center mb-5">
                            <img src="<?php echo $srcPath . '/dist/img/right.png' ?>" class="img-responsive">
                            <h1>Are You Sure?</h1>
                            <p>Do you really want to delete these file? This process cannot be undone.</p>
                            <div class="btn-group">
                                <input type="hidden" value="" id="delete_file_id">
                                <button type="button" class="btn btn-secondary btn-lg mr-2 rounded-lg"
                                    data-dismiss="modal">Cancel</button>
                                <button type="button" id="delete_file" data-dismiss="modal"
                                    class="btn btn-danger btn-lg rounded-lg">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- /.content-wrapper -->
    <?php
    // require_once('components/footer.php')
    ?>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->
    <!-- jQuery -->
    <script src="<?php echo $srcPath . '/plugins/jquery/jquery.min.js' ?>"></script>

    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo $srcPath . '/plugins/jquery-ui/jquery-ui.min.js' ?>"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <script src="<?php echo $srcPath . '/plugins/select2/js/select2.full.min.js' ?>"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo $srcPath . '/plugins/bootstrap/js/bootstrap.bundle.min.js' ?>"></script>
    <!-- overlayScrollbars -->
    <script src="<?php echo $srcPath . '/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js' ?>"></script>

    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.css' ?>">
    <script src=" <?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.js' ?>"> </script>

    <!-- AdminLTE App -->
    <script src="<?php echo $srcPath . '/dist/js/adminlte.js' ?>"></script>
    <script src="https://kit.fontawesome.com/a044a561b3.js" crossorigin="anonymous"></script>
    <script src="<?php echo $srcPath . '/plugins/ekko-lightbox/ekko-lightbox.js' ?>"></script>
    <script>
        $(document).ready(function () {
            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
            $('#audio_category').select2();
        });
    </script>
    <script src="<?php echo $srcPath . '/actions/manage_audio.js' ?>"></script>
</body>

</html>