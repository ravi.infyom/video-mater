<?php
list($rootPath, $srcPath) = ['..', '../Src'];
require_once $srcPath . '/middleware/authenticated.php';
require "../bootstrap.php";
$page = 'Manage Notification';
$active = '';
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>
            <?php echo $_ENV['APP_NAME'] ?>
        </title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="<?php echo $rootPath . '/favicon.png' ?>" type="image/png" sizes="16x16">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/fontawesome-free/css/all.min.css' ?>">
        <!-- Ionicons -->
        <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->

        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/adminlte.min.css' ?>">
        <!-- overlayScrollbars -->
        <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/overlayScrollbars/css/OverlayScrollbars.min.css' ?>">

        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/custom/audio.css' ?>">
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">

            <?php
require_once $srcPath . '/components/navbar.php'
?>

                <!-- Main Sidebar Container -->
                <?php
require_once $srcPath . '/components/main_sidebar.php'
?>

                    <!-- Content Wrapper. Contains page content -->
                    <div class="content-wrapper">
                        <!-- Content Header (Page header) -->
                        <div class="content-header">
                            <div class="container-fluid">
                                <!-- <div class="row mb-2">
                        <h1 class="ml-2 text-dark"><?php //echo $page;
?></h1>
                    </div> -->
                            </div>
                            <!-- /.container-fluid -->
                        </div>
                        <!-- /.content-header -->

                        <!-- Main content -->
                        <section class="content">
                            <div class="container-fluid">
                                <div id="audio_form" class="" style="padding: 10px 16%">
                                    <!-- Horizontal Form -->
                                    <div class="card audio-card">
                                        <div class="card-header">
                                            <h3 id="audio_header">
                                                <?php echo $page ?>
                                            </h3>
                                            <!-- </div> -->
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body" id="audio_body">
                                            <!-- form start -->
                                            <form class="form-horizontal audio-form" id="audioForm">
                                                <div class="form-group row">
                                                    <div class="col-2 ">
                                                        <label for="title" class="col-form-label">Title<span
                                                    class="required">*</span></label>
                                                    </div>
                                                    <div class="col-10">
                                                        <input type="text" class="form-control" name="title" id="title">
                                                        <label id="title-error" class="text-danger" for="title"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-2 ">
                                                        <label for="message" class="col-form-label">Message<span
                                                    class="required">*</span></label>
                                                    </div>
                                                    <div class="col-10">
                                                        <textarea class="form-control" rows="5" id="message"></textarea>
                                                        <label id="message-error" class="text-danger" for="message"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-2 ">
                                                        <label for="banner_image" class="col-form-label">Banner Iamge</label>
                                                    </div>
                                                    <div class="col-10">
                                                        <div class="row mb-2">
                                                            <div class="col-12">
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" class="custom-control-input" value="url" id="url" name="banner_type" checked>
                                                                    <label class="custom-control-label" for="url">Url</label>
                                                                </div>
                                                                <div class="custom-control custom-radio custom-control-inline">
                                                                    <input type="radio" class="custom-control-input" value="upload" id="upload" name="banner_type">
                                                                    <label class="custom-control-label" for="upload">Upload</label>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="row mb-2" id="url_div">
                                                            <div class="col-12">
                                                                <input type="text" class="form-control" name="banner_url" id="banner_url">
                                                                <label id="banner_url-error" class="text-danger" for="banner_url"></label>
                                                            </div>
                                                        </div>
                                                        <div class="row hideMe" id="upload_div">
                                                            <div class="col-12">
                                                                <input type="file" id="banner_image" class="form-control" name="banner_image" accept="image/*">
                                                                <label id="banner_image-error" class="text-danger" for="banner_image"></label>
                                                                <input type="hidden" name="banner_hidden" id="banner_hidden" value="">

                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-2 ">
                                                        <label for="target_url" class="col-form-label">Target Url</label>
                                                    </div>
                                                    <div class="col-10">
                                                        <input type="text" class="form-control" name="target_url" id="target_url">
                                                        <label id="target_url-error" class="text-danger" for="target_url"></label>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-3 ">
                                                        <label for="open_app" class="col-form-label">Open App</label>

                                                        <div class="custom-control custom-switch ">
                                                            <input type="checkbox" class="custom-control-input form-contorl" id="open_app" name="open_app" checked>
                                                            <label class="custom-control-label" for="open_app"></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-9">
                                                        <div>
                                                            <label for="open_app" class="col-form-label" style="margin-right:5px">Banner Size </label>
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" class="custom-control-input" value="small" id="small" name="banner_size" checked>
                                                                <label class="custom-control-label" for="small">Small</label>
                                                            </div>
                                                            <div class="custom-control custom-radio custom-control-inline">
                                                                <input type="radio" class="custom-control-input" value="large" id="large" name="banner_size">
                                                                <label class="custom-control-label" for="large">Large</label>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group ">
                                                    <div class="col-12  text-center">
                                                        <button type="button" id="submitNotification" value="add" class="btn btn-primary">Submit</button>
                                                        <a role="button" href="notification.php" class="btn btn-success text-white">Back</a>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </section>
                    </div>
                   
        </div>
        <!-- /.content-wrapper -->
        <?php
            // require_once('components/footer.php')
        ?>

            <!-- Control Sidebar -->
            <aside class="control-sidebar control-sidebar-dark">
                <!-- Control sidebar content goes here -->
            </aside>
            <!-- /.control-sidebar -->
            </div>
            <!-- ./wrapper -->
            <!-- jQuery -->
            <script src="<?php echo $srcPath . '/plugins/jquery/jquery.min.js' ?>"></script>

            <!-- jQuery UI 1.11.4 -->
            <script src="<?php echo $srcPath . '/plugins/jquery-ui/jquery-ui.min.js' ?>"></script>
            <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
            <script>
                $.widget.bridge('uibutton', $.ui.button)
            </script>
            <!-- Bootstrap 4 -->
            <script src="<?php echo $srcPath . '/plugins/bootstrap/js/bootstrap.bundle.min.js' ?>"></script>
            <!-- overlayScrollbars -->
            <script src="<?php echo $srcPath . '/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js' ?>"></script>


            <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.css' ?>">
            <script src=" <?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.js' ?>">
                < !--AdminLTE App-- >
                <
                script src = "<?php echo $srcPath . '/dist/js/adminlte.js' ?>" >
            </script>
            <script src="https://kit.fontawesome.com/a044a561b3.js" crossorigin="anonymous"></script>
            <script src="<?php echo $srcPath . '/actions/manage_notification.js' ?>"></script>
    </body>

    </html>