<?php
list($rootPath, $srcPath) = ['..', '../Src'];
require_once($srcPath . '/middleware/authenticated.php');
require "../bootstrap.php";
$page = 'Manage Video Ready';
$active = '';
if (!filter_var($_ENV['ENABLE_VIDEO_READY'], FILTER_VALIDATE_BOOLEAN)) {
    header("Location: $rootPath/index");
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $_ENV['APP_NAME'] ?>| Video Ready</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo $rootPath . '/favicon.png' ?>" type="image/png" sizes="16x16">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/fontawesome-free/css/all.min.css' ?>">
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/adminlte.min.css' ?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/overlayScrollbars/css/OverlayScrollbars.min.css' ?>">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/select2/css/select2.min.css' ?>" />
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/ekko-lightbox/ekko-lightbox.css' ?>" />
    <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/custom/video_ready.css' ?>">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <?php
        require_once($srcPath . '/components/navbar.php')
        ?>

        <!-- Main Sidebar Container -->
        <?php
        require_once($srcPath . '/components/main_sidebar.php')
        ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <!-- <div class="row mb-2">
                        <h1 class="ml-2 text-dark"><?php //echo $page; 
                                                    ?></h1>
                    </div> -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <div id="video_ready_form" class="disableForm" style="padding: 10px 20%">
                        <!-- Horizontal Form -->
                        <div class="card video-card">
                            <div class="card-header">
                                <h3 id="video_ready_header"><?php echo $page ?></h3> <!-- </div> -->
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body" id="category_body">
                                <!-- form start -->
                                <form class="form-horizontal video-form" id="videoReadyForm">
                                    <div class="form-group row">
                                        <div class="col-2 ">
                                            <label for="category" class="col-form-label">Category<span
                                                    class="required">*</span></label>
                                        </div>
                                        <div class="col-10">
                                            <!-- <select class=" form-control select2" style="max-width: 100%;"
                                                    name="category" id="category">

                                                </select> -->
                                            <select class="select2 " style="width: 100%;margin-bottom:2px;"
                                                name="category" id="category">
                                            </select>
                                            <label id="category-error" class="text-danger" for="category"></label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-2 ">
                                            <label for="title" class="col-form-label">Title<span
                                                    class="required">*</span></label>
                                        </div>
                                        <div class="col-sm-10">
                                            <input type="text" name="title" id="title" class="form-control" value="">
                                            <label id="title-error" class="text-danger" for="title"></label>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-2 d-flex align-items-center ">
                                            <label for="cover_image" class="col-form-label">Cover Image<span
                                                    class="required">*</span></label>

                                        </div>
                                        <div class="col-10 d-flex">
                                            <div class="order-1">
                                                <img src="<?php echo $srcPath . '/dist/img/add-plus-2.png' ?>"
                                                    class="form-control" id="image_preview"
                                                    style="max-height: 280px; height: 130px; width: 130px; border-radius: 50%"
                                                    data-toggle="tooltip" data-placement="right"
                                                    title="upload cover image"
                                                    style="cursor: pointer; max-height: 280px; height: 200px; width: 200px;">
                                                <input type="file" id="cover_image" name="cover_image" accept="image/*"
                                                    class="hideMe">
                                                <label id="cover_image-error" class="text-danger"
                                                    for="cover_image"></label>

                                            </div>
                                            <div class="order-2 d-flex align-items-center">
                                                <span class="mx-3 remove_cover_image hideMe"><button
                                                        id="remove_cover_image"
                                                        class="fa fa-minus-circle btn btn-danger btn-circle"
                                                        data-toggle="tooltip" data-placement="right"
                                                        title="remove cover image"></button></span>

                                                <input type="hidden" name="cover_image_hidden" id="cover_image_hidden"
                                                    value="">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-2 d-flex align-items-center ">
                                            <label for="video" class="col-form-label">Video
                                                <span class="required">*</span>
                                            </label>
                                        </div>
                                        <div class="col-sm-10 d-flex">
                                            <div class="order-1">
                                                <img src="<?php echo $srcPath . '/dist/img/no-image-3.png' ?>"
                                                    class="add_video" id="video_image" data-toggle="tooltip"
                                                    title="upload video" data-placement="right"
                                                    style="cursor: pointer; max-height: 280px; height: 130px; width: 130px; border-radius: 50%"
                                                    data-s3="1">
                                                <input type="file" id="video" name="video" accept="video/*"
                                                    class="hideMe">
                                                <label id="video-error" class="text-danger" for="video"></label>
                                            </div>
                                            
                                            <div class="order-2 d-flex align-items-center">
                                            
                                                <span class="mx-3 remove_video hideMe"><button id="remove_video"
                                                        class="fa fa-minus-circle btn btn-danger btn-circle"
                                                        data-toggle="tooltip" data-placement="right"
                                                        title="remove video"></button></span>
                                                <input type="hidden" id="video_hidden" value="">
                                                <div class="video-spinner hideMe text-center">
                                                    <span> <i class="fa fa-spinner fa-2x fa-spin "></i></span><br/>
                                                    <span  class="text-success">Uploading...</span>
                                                </div>
                                                <a class="lightbox btn btn-link video hideMe"
                                                    data-toggle="lightboxVideo" data-type="video" href="">
                                                    Play
                                                </a>
                                                <video id="video_preview" class="hideMe">
                                                    <source id="source" src=""></video>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <div class="col-12  text-center">
                                            <button type="button" id="submitVideoReady" value="add"
                                                class="btn btn-primary">Submit</button>
                                            <a role="button" href="video_ready.php" class="btn btn-success text-white">
                                                Back
                                            </a>
                                            <input type="hidden" id="masterId" value="" /><br />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                </div>
            </section>
        </div>
        <!-- /.content -->
        <div>
            <div class="modal" id="deleteFileModal">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <!-- Modal body -->
                        <div class="modal-body text-center mb-5">
                            <img src="<?php echo $srcPath . '/dist/img/right.png' ?>" class="img-responsive">
                            <h1>Are You Sure?</h1>
                            <p>Do you really want to delete these file? This process cannot be undone.</p>
                            <div class="btn-group">
                                <input type="hidden" value="" id="delete_file_id">
                                <button type="button" class="btn btn-secondary btn-lg mr-2 rounded-lg"
                                    data-dismiss="modal">Cancel</button>
                                <button type="button" id="delete_file" data-dismiss="modal"
                                    class="btn btn-danger btn-lg rounded-lg">Delete</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- /.content-wrapper -->
    <?php
    // require_once('components/footer.php')
    ?>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->
    <!-- jQuery -->
    <script src="<?php echo $srcPath . '/plugins/jquery/jquery.min.js' ?>"></script>

    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo $srcPath . '/plugins/jquery-ui/jquery-ui.min.js' ?>"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button)
    </script>
    <script src="<?php echo $srcPath . '/plugins/select2/js/select2.full.min.js' ?>"></script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo $srcPath . '/plugins/bootstrap/js/bootstrap.bundle.min.js' ?>"></script>
    <!-- overlayScrollbars -->
    <script src="<?php echo $srcPath . '/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $srcPath . '/dist/js/adminlte.js' ?>"></script>
    <script src="https://kit.fontawesome.com/a044a561b3.js" crossorigin="anonymous"></script>
    <script src="<?php echo $srcPath . '/plugins/ekko-lightbox/ekko-lightbox.js' ?>"></script>
    
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.css' ?>">
    <script src=" <?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.js' ?>"> </script>
    <script>
    $(document).ready(function() {
        $(function() {
            $('[data-toggle="tooltip"]').tooltip()
        })
        $('#category').select2();
    });
    </script>
    <script src="<?php echo $srcPath . '/actions/video_ready-uploads.js' ?>"></script>
    <script src="<?php echo $srcPath . '/actions/manage_video_ready.js' ?>"></script>
</body>

</html>