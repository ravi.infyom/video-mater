<?php
list($rootPath, $srcPath) = ['..', '../Src'];
require_once $srcPath . '/middleware/authenticated.php';
require "../bootstrap.php";
$page = 'Settings';
$active = 'settings';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        <?php echo $_ENV['APP_NAME']; ?>
    </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo $rootPath . '/favicon.png' ?>" type="image/png" sizes="16x16">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/fontawesome-free/css/all.min.css' ?>">
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/adminlte.min.css' ?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/overlayScrollbars/css/OverlayScrollbars.min.css' ?>">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/select2/css/select2.min.css' ?>" />

    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css' ?>">
    <link href="<?php echo $srcPath . '/plugins/datatables-responsive/css/responsive.bootstrap4.min.css' ?>"
        rel="stylesheet">

    <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/custom/settings.css' ?>">
</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <?php
require_once $srcPath . '/components/navbar.php'
?>

        <!-- Main Sidebar Container -->
        <?php
require_once $srcPath . '/components/main_sidebar.php'
?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <h1 class="ml-2 " style="color:#6c757d">
                            <?php echo $page; ?>
                        </h1>

                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <!-- <div id="main-content" class="disableAll"> -->
                    <div class="row">
                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <!-- Profile Image -->
                            <div class="card card-secondary card-outline">
                                <div class="card-header text-center">
                                    <h3 class="card-title" style="color:#6c757d"><b>Update Profile</b></h3>
                                </div>
                                <div class="card-body box-profile">
                                    <div class="text-center ">
                                        <img class="avatar form-control" src="../Src/dist/img/avatar5.png"
                                            alt="User profile picture" id="avatar_preview">

                                        <!-- <span class="edit-avatar"><i class="fas fa-edit"></i></span> -->
                                        <figcaption><small><i>*Click above image to update profile</i></small>
                                        </figcaption>
                                        <label id="avatar-error" class="text-danger" for="avatar"></label>
                                        <input type="file" id="avatar" name="avatar" accept="image/*" class="hideMe">
                                        <input type="hidden" name="avatar_hidden" id="avatar_hidden" value="">
                                    </div>
                                    <div class="form-group row">

                                        <input type="email" class="form-control form-control-sm" name="email" id="email"
                                            autocomplete="false" placeholder="Enter Full Name">
                                        <label id="email-error" class="text-danger" for="email"></label>
                                    </div>
                                    <div class="form-group row">

                                        <input type="text" class="form-control form-control-sm" name="full_name"
                                            id="full_name" autocomplete="false" placeholder="Enter Full Name">
                                        <label id="full_name-error" class="text-danger" for="full_name"></label>
                                    </div>

                                    <button class="btn btn-secondary btn-block text-bold"
                                        id="update_profile">Update</button>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                            <!-- /.card -->
                        </div>
                        <div class="col-lg-3  col-sm-6 col-xs-12">
                            <div class="card card-secondary card-outline">
                                <div class="card-header text-center">
                                    <h3 class="card-title" style="color:#6c757d"><b>Update Password</b></h3>
                                </div>
                                <div class="card-body box-profile">
                                    <div class="form-group row">

                                        <input type="password" autocomplete="new-password"
                                            class="form-control form-control-sm" name="password" id="password"
                                            placeholder="Password">
                                        <label id="password-error" class="text-danger" for="password"></label>
                                        <!-- </div> -->
                                    </div>
                                    <div class="form-group row">

                                        <input type="password" autocomplete="new-password"
                                            class="form-control form-control-sm" name="confirm_password"
                                            id="confirm_password" placeholder="Confirm Password">
                                        <label id="confirm_password-error" class="text-danger"
                                            for="confirm_password"></label>
                                        <!-- </div> -->
                                    </div>
                                    <div class="row">
                                        <button class="btn  btn-secondary btn-block" id="update_password"><b>Update
                                                Password</b></button>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>

                        </div>
                        <div class="col-lg-5 col-sm-6 col-xs-12">
                            <div class="card card-secondary card-outline">
                                <div class="card-header text-center">
                                    <h3 class="card-title" style="color:#6c757d"><b>Notification</b></h3>
                                </div>
                                <div class="card-body box-profile">
                                    <div class="form-group row">
                                        <textarea class="form-control" rows="5" id="server_token"
                                            placeholder="Enter Server Token..."></textarea>
                                        <label id="server_token-error" class="text-danger" for="server_token"></label>
                                        <!-- </div> -->
                                    </div>
                                    <div class="row">
                                        <button class="btn  btn-secondary btn-block" id="update_token"><b>Update Server
                                                Key</b></button>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <!-- Profile Image -->
                            <div class="card card-secondary card-outline">
                                <div class="card-header text-center">
                                    <h3 class="card-title" style="color:#6c757d"><b>Country Market</b></h3>
                                </div>
                                <div class="card-body box-profile">
                                    <div class="col-sm-12">
                                        <div class="text-center ">
                                            <img class="icon form-control" src="../Src/dist/img/add_plus.png"
                                                alt="Add Icon" id="icon_preview">

                                            <!-- <span class="edit-icon"><i class="fas fa-edit"></i></span> -->
                                            <figcaption><small><i>*Click above image to add/update icon</i></small>
                                            </figcaption>
                                            <label id="icon-error" class="text-danger" for="icon"></label>
                                            <input type="file" id="icon" name="icon" accept="image/*" class="hideMe">
                                            <input type="hidden" name="icon_hidden" id="icon_hidden" value="">
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group ">
                                            <label>Maker Name</label>
                                            <input type="text" class="form-control form-control-sm" name="maker_name"
                                                id="maker_name" placeholder="Enter Maker Name">
                                            <label id="maker_name-error" class="text-danger hideMe"
                                                for="maker_name"></label>
                                        </div>
                                    </div>
                                    <!-- radio -->
                                    <div class="col-12">
                                        <div class="form-group">
                                            <div class="custom-control custom-switch ">
                                                <input type="checkbox" class="custom-control-input" id="country_status"
                                                    name="country_status">
                                                <label class="custom-control-label" for="country_status">Status</label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <!-- textarea -->
                                        <div class="form-group">
                                            <label>Country Message</label>
                                            <textarea id="country_msg" class="form-control" rows="3"
                                                placeholder="Enter ..."></textarea>
                                        </div>
                                    </div>

                                    <button class="btn btn-secondary btn-block text-bold"
                                        id="update_country">Update</button>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                            <!-- /.card -->
                        </div>
                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="card card-secondary card-outline">
                                <div class="card-header">
                                    <h3 class="card-title" style="color:#6c757d"><b>Update</b></h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <!-- radio -->
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="custom-control custom-switch ">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="update_status" name="update_status">
                                                    <label class="custom-control-label" for="update_status">Update
                                                        Status</label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Version Code</label>
                                                <input type="text" id="version_code" class="form-control"
                                                    placeholder="Enter ...">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <!-- textarea -->
                                            <div class="form-group">
                                                <label>What's New</label>
                                                <textarea id="whats_new" class="form-control" rows="3"
                                                    placeholder="Enter ..."></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <button class="btn  btn-secondary btn-block"
                                            id="update_details"><b>Update</b></button>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <div class="card card-secondary card-outline">
                                <div class="card-header">
                                    <h3 class="card-title" style="color:#6c757d"><b>Ad Dialog</b></h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body">
                                    <div class="row">
                                        <!-- radio -->
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <div class="custom-control custom-switch ">
                                                    <input type="checkbox" class="custom-control-input form-contorl"
                                                        id="show_ad" name="show_ad">
                                                    <label class="custom-control-label" for="show_ad">Ad</label>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="">
                                                <label for="ads" class="col-form-label">Select Ad<span
                                                        class="required">*</span></label>
                                                <select class="select2" style="width: 100%;margin-bottom:2px;"
                                                    name="ads" id="ads" multiple>
                                                </select>
                                                <label id="ads-error" class="text-danger" for="ads"></label>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Number of click</label>
                                                <input type="number" min=1 id="number_of_click" class="form-control"
                                                    placeholder="Enter ...">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <button class="btn  btn-secondary btn-block"
                                            id="update_ad"><b>Update</b></button>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-sm-6 col-xs-12">
                            <!-- /.card -->
                            <div class="card card-secondary card-outline ">
                                <div class="card-header text-center">
                                    <h3 class="card-title" style="color:#6c757d"><b>Ad Network</b></h3>
                                </div>
                                <div class="card-body box-profile">
                                    <div class="row">
                                        <div class="">
                                            <div class="custom-control custom-switch custom-switch-inline">
                                                <input type="checkbox" class="custom-control-input form-contorl"
                                                    id="fbAd" name="fbAd">
                                                <label class="custom-control-label" for="fbAd">Facebook Ad</label>
                                            </div>
                                            <div class="custom-control custom-switch custom-switch-inline">
                                                <input type="checkbox" class="custom-control-input" id="fbIntreastial"
                                                    name="fbIntreastial">
                                                <label class="custom-control-label" for="fbIntreastial">Facebook
                                                    Intreastial</label>
                                            </div>
                                            <div class="custom-control custom-switch custom-switch-inline">
                                                <input type="checkbox" class="custom-control-input" id="fbNative"
                                                    name="fbNative">
                                                <label class="custom-control-label" for="fbNative">Facebook
                                                    Native</label>
                                            </div>
                                            <div class="custom-control custom-switch custom-switch-inline">
                                                <input type="checkbox" class="custom-control-input" id="fbBanner"
                                                    name="fbBanner">
                                                <label class="custom-control-label" for="fbBanner">Facebook
                                                    Banner</label>
                                            </div>
                                            <div class="custom-control custom-switch custom-switch-inline">
                                                <input type="checkbox" class="custom-control-input" id="fbNativeBanner"
                                                    name="fbNativeBanner">
                                                <label class="custom-control-label" for="fbNativeBanner">Facebook Native
                                                    Banner</label>
                                            </div>
                                            <div class="custom-control custom-switch custom-switch-inline">
                                                <input type="checkbox" class="custom-control-input form-contorl"
                                                    id="adMobAd" name="adMobAd">
                                                <label class="custom-control-label" for="adMobAd">AdMob Ad</label>
                                            </div>
                                            <div class="custom-control custom-switch custom-switch-inline">
                                                <input type="checkbox" class="custom-control-input"
                                                    id="adMobIntreastial" name="adMobIntreastial">
                                                <label class="custom-control-label" for="adMobIntreastial">AdMob
                                                    Intreastial</label>
                                            </div>
                                            <div class="custom-control custom-switch custom-switch-inline">
                                                <input type="checkbox" class="custom-control-input" id="adMobBanner"
                                                    name="adMobBanner">
                                                <label class="custom-control-label" for="adMobBanner">AdMob
                                                    Banner</label>
                                            </div>
                                            <div class="custom-control custom-switch custom-switch-inline">
                                                <input type="checkbox" class="custom-control-input" id="adMobNative"
                                                    name="adMobNative">
                                                <label class="custom-control-label" for="adMobNative">AdMob
                                                    Native</label>
                                            </div>
                                            <div class="custom-control custom-switch custom-switch-inline">
                                                <input type="checkbox" class="custom-control-input" id="adMobRv"
                                                    name="adMobRv">
                                                <label class="custom-control-label" for="adMobRv">AdMob RV</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label>Page clicks</label>
                                            <input type="number" min=1 id="pageClicks" class="form-control"
                                                placeholder="Enter ...">
                                        </div>


                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label>Banner View</label>
                                            <input type="number" min=1 id="bannerView" class="form-control"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <label>Native View</label>
                                            <input type="number" min=1 id="nativeView" class="form-control"
                                                placeholder="Enter ...">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <button class="btn btn-block btn-secondary"
                                            id="update_network"><b>Update</b></button>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <!-- /.card -->
                        </div>
                    </div>
                    <!-- </div> -->
                    <!-- <div id="spinner" class="show"></div> -->
                </div>
                <!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </div>
    <!-- ./wrapper -->
    <!-- jQuery -->
    <script src="<?php echo $srcPath . '/plugins/jquery/jquery.min.js' ?>"></script>

    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo $srcPath . '/plugins/jquery-ui/jquery-ui.min.js' ?>"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo $srcPath . '/plugins/bootstrap/js/bootstrap.bundle.min.js' ?>"></script>
    <!-- overlayScrollbars -->
    <script src="<?php echo $srcPath . '/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $srcPath . '/dist/js/adminlte.js' ?>"></script>
    <script src="<?php echo $srcPath . '/dist/js/demo.js' ?>"></script>
    <script src="https://kit.fontawesome.com/a044a561b3.js" crossorigin="anonymous"></script>

    <script src="<?php echo $srcPath . '/plugins/select2/js/select2.full.min.js' ?>"></script>
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.css' ?>">
    <script src=" <?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.js' ?>">
    </script>

    <!-- DataTables -->
    <script src="<?php echo $srcPath . '/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/datatables-responsive/js/dataTables.responsive.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/datatables-responsive/js/responsive.bootstrap4.min.js" ' ?>"" ></script>

    <script>
            $(document).ready(function () {
                $('#ads ').select2({
                    placeholder: " Select Ads",
                });
            }); </script>

    <script src=" <?php echo $srcPath . '/actions/setting.js ' ?>"></script>


</body>

</html>