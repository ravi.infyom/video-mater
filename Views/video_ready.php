<?php
list($rootPath, $srcPath) = ['..', '../Src'];
require_once $srcPath . '/middleware/authenticated.php';
require "../bootstrap.php";
$page = 'Video Ready';
$active = 'video_ready';
if (!filter_var($_ENV['ENABLE_VIDEO_READY'], FILTER_VALIDATE_BOOLEAN)) {
    header("Location: $rootPath/index");
}
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $_ENV['APP_NAME'] ?>| Video Ready</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo $rootPath . '/favicon.png' ?>" type="image/png" sizes="16x16">
    <!-- Font Awesome -->
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/adminlte.min.css' ?>">

    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/select2/css/select2.min.css' ?>" />
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/nprogress/nprogress.css' ?>">
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/ekko-lightbox/ekko-lightbox.css' ?>" />
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/overlayScrollbars/css/OverlayScrollbars.min.css' ?>">
    <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/custom/video_ready.css' ?>">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css' ?>">
    <link rel="stylesheet"
        href="<?php echo $srcPath . '/plugins/datatables-responsive/css/responsive.bootstrap4.min.css' ?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <?php
require_once $srcPath . '/components/navbar.php'
?>

        <!-- Main Sidebar Container -->
        <?php
require_once $srcPath . '/components/main_sidebar.php'
?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <h1 class="ml-2 text-dark"><?php echo $page; ?></h1>
                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">

                    <div class="card">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-6">
                                    Video Ready List
                                </div>
                                <div class="col-md-6">
                                    <a role="button" href="manage_video_ready.php"
                                        class="btn btn-sm btn-success float-right text-white">
                                        <i class="fa fa-plus"> Add</i>
                                    </a>
                                </div>

                                <!-- /.col -->
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="video_ready_table" class="table table-striped table-bordered table-hover"
                                        cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Category</th>
                                                <th>Title</th>
                                                <th>Cover Iamge</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody id="video_ready_table_body">
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.col -->
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
    <div>
        <div class="modal" id="deleteModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <!-- Modal Header -->
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <!-- Modal body -->
                    <div class="modal-body text-center mb-5">
                        <img src="<?php echo $srcPath . '/dist/img/right.png' ?>" class="img-responsive">
                        <h1>Are You Sure?</h1>
                        <p>Do you really want to delete these video ? This process cannot be undone.</p>
                        <div class="btn-group">
                            <input type="hidden" value="" id="delete_id">
                            <button type="button" class="btn btn-secondary btn-lg mr-2 rounded-lg"
                                data-dismiss="modal">Cancel</button>
                            <button type="button" id="deleteVideoReady" data-dismiss="modal"
                                class="btn btn-danger btn-lg rounded-lg">Delete</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->
    <!-- jQuery -->
    <script src="<?php echo $srcPath . '/plugins/jquery/jquery.min.js' ?>"></script>

    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo $srcPath . '/plugins/jquery-ui/jquery-ui.min.js' ?>"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script src="https://kit.fontawesome.com/a044a561b3.js" crossorigin="anonymous"></script>

    <script>
    $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo $srcPath . '/plugins/bootstrap/js/bootstrap.bundle.min.js' ?>"></script>
    <!-- overlayScrollbars -->
    <script src="<?php echo $srcPath . '/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js' ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo $srcPath . '/dist/js/adminlte.js' ?>"></script>
    <script src="<?php echo $srcPath . '/dist/js/demo.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/ekko-lightbox/ekko-lightbox.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/nprogress/nprogress.js' ?>"></script>
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.css' ?>">
    <script src=" <?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.js' ?>"> </script>

    <!-- DataTables -->
    <script src="<?php echo $srcPath . '/plugins/datatables/jquery.dataTables.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/datatables-responsive/js/dataTables.responsive.min.js' ?>"></script>
    <script src="<?php echo $srcPath . '/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"' ?>""></script>

    <script>
    $(document).ready(function() {

    });
    </script>
    <script src=" <?php echo $srcPath . '/actions/video_ready.js' ?>"></script>
</body>

</html>