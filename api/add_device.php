<?php
require "../bootstrap.php";
require "./Parts/headers.php";

use Src\Controller\DeviceController;
use Src\Service\JwtHelper;

$headers = getAuthorizationHeader();
$token = getBearerToken($headers);
$jwt = new JwtHelper($dbConnection);

if ($token && $jwt->verify_token($token)) {
    $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $method = $_SERVER['REQUEST_METHOD'];

    $uri = explode('api/', $uri);
    if ($method == 'POST' && $uri[1] == 'add_device') {

        $id = '';
        if (isset($_POST['device_id'])) {
            $id = $_POST['device_id'];
        } else {
            $data = json_decode(file_get_contents("php://input"));
            $id = $data->device_id ?? '';
        }

        if (!empty($id)) {

            $controller = new DeviceController($dbConnection, $method);
            $result = $controller->addDevice($id);

            header('HTTP/1.1 200 OK');
            echo json_encode(['success' => true]);
        } else {

            // set response code - 400 bad request
            http_response_code(422);

            // tell the user
            echo json_encode(array("success" => false, "errors" => ["device_id" => "device_id field is required."], "message" => "Invalid data."));
        }
    } else {
        header("HTTP/1.1 404 Not Found");
        echo json_encode(array("success" => false, "errors" => "Only POST request allowed", "message" => "Invalid request type."));
        return false;
    }

} else {
    http_response_code(401);
    echo json_encode(['status' => 'FAILED', 'message' => 'Unauthorized']);
    return false;
}
