<?php
require "../bootstrap.php";
require "./Parts/headers.php";

use Src\Controller\AudioCategoryController;
use Src\Controller\AudioController;
use Src\Service\JwtHelper;

$headers = getAuthorizationHeader();
$token = getBearerToken($headers);
$jwt = new JwtHelper($dbConnection);

if ($token && $jwt->verify_token($token)) {
    $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $method = $_SERVER['REQUEST_METHOD'];

    $uri = explode('api/', $uri);
    if ($method == 'GET' && $uri[1] == 'audio') {
        $queryParams = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
        parse_str($queryParams, $params);

        $response = [
            'status' => true,
            'total_category' => 0,
            'total_audio' => 0,
            'msg' => 'audio',
            'audio_category' => [],
            'audios' => [],
        ];

        $controller = new AudioController($dbConnection, $method, $params);
        $result = $controller->processRequest();

        if (array_key_exists('audios', $result) && count($result['audios']) > 0) {
            $response['audios'] = $result['audios'];
        }
        if (array_key_exists('audio_count', $result) && $result['audio_count'] > 0) {
            $response['total_audio'] = $result['audio_count'];
        }
        $catController = new AudioCategoryController($dbConnection, $method, $params);
        $catResult = $catController->processRequest();
        if (array_key_exists('audio_category', $catResult) && count($catResult['audio_category']) > 0) {
            $response['audio_category'] = $catResult['audio_category'];
        }
        if (array_key_exists('total_category', $catResult) && $catResult['total_category'] > 0) {
            $response['total_category'] = $catResult['total_category'];
        }
        header('HTTP/1.1 200 OK');
        echo json_encode($response);
    } else {
        header("HTTP/1.1 404 Not Found");
        return false;
    }

} else {
    http_response_code(401);
    echo json_encode(['status' => 'FAILED', 'message' => 'Unauthorized']);
    return false;
}
