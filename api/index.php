<?php
require "../bootstrap.php";
require "./Parts/headers.php";

use  Src\Service\JwtHelper;

$headers = getAuthorizationHeader();
$token= getBearerToken($headers);
$jwt=new JwtHelper($dbConnection);

if ($token && $jwt->verify_token($token)) {
    http_response_code(200);
    echo json_encode(['status' => 'SUCCESS', 'message' => 'API server running']);
    return true;
} else {
    http_response_code(401);
    echo json_encode(['status' => 'FAILED', 'message' => 'Unauthorized']);
    return false;
}
