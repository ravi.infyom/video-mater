<?php
require "../bootstrap.php";

use Src\Service\JwtHelper;

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

if (!isset($_POST['email'])) {
    http_response_code(422);
    // tell the user
    echo json_encode(array("success" => false, "errors" => ["email" => "email field is required."], "message" => "Invalid data."));
    return false;
}

if (!isset($_POST['password'])) {
    http_response_code(422);
    // tell the user
    echo json_encode(array("success" => false, "errors" => ["password" => "password field is required."], "message" => "Invalid data."));
    return false;
}

$email = $_POST['email'];
$password = $_POST['password'];

$jwt = new JwtHelper($dbConnection);
$jwt->authenticate($email, $password);
$token = $jwt->getToken();

if ($token) {
    http_response_code(200);
    echo json_encode(['status' => 'SUCCESS', 'message' => 'login successfull', 'token' => $token]);
    return true;
} else {
    http_response_code(401);
    echo json_encode(['status' => 'FAILED', 'message' => 'Unauthorized']);
    return true;
}
