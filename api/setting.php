<?php
require "../bootstrap.php";
require "./Parts/headers.php";

use Src\Controller\AudioCategoryController;
use Src\Controller\UserController;
use Src\Service\JwtHelper;

$headers = getAuthorizationHeader();
$token = getBearerToken($headers);
$jwt = new JwtHelper($dbConnection);

//
if ($token && $jwt->verify_token($token)) {
    $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $method = $_SERVER['REQUEST_METHOD'];

    $uri = explode('api/', $uri);
    if ($method == 'GET' && $uri[1] == 'setting') {
        // $queryParams = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
        // parse_str($queryParams, $params);

        $response = [
            "success" => true,
            "message" => "ad perfectly work",
            "update" => [
                "status" => null,
                "version_code" => 0,
                "whats_news" => "",
            ],
            "ad_network" => [
                "fb_ad" => null,
                "admob_ad" => null,
                "page_click" => null,
            ],
            "country_market" => [
                "status" => null,
                "maker_name" => "",
                "msg" => "",
                "flag_icon" => "",
            ],
            "custom_ad_status" => null,
            "custom_ad_click" => null,
            "custom_ad" => [

            ],
        ];

        $controller = new UserController($dbConnection, $method);
        $result = $controller->processRequest();

        if (array_key_exists('data', $result) && count($result['data']) > 0) {
            $response = $result['data'];
        }
        
       
        header('HTTP/1.1 200 OK');
        echo json_encode($response);
    } else {
        header("HTTP/1.1 404 Not Found");
        return false;
    }

} else {
    http_response_code(401);
    echo json_encode(['status' => 'FAILED', 'message' => 'Unauthorized']);
    return false;
}
