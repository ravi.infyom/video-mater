<?php
require "../bootstrap.php";
require "./Parts/headers.php";

use Src\Controller\VideoCategoryController;
use Src\Controller\VideoEffectController;
use Src\Service\JwtHelper;

$headers = getAuthorizationHeader();
$token = getBearerToken($headers);
$jwt = new JwtHelper($dbConnection);

if ($token && $jwt->verify_token($token)) {
    $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $method = $_SERVER['REQUEST_METHOD'];

    $uri = explode('api/', $uri);
    if ($method == 'GET' && $uri[1] == 'video_effect') {
        $queryParams = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
        parse_str($queryParams, $params);

        $response = [
            'status' => true,
            'total_category' => 0,
            'total_effect' => 0,
            'msg' => 'video stutus effect',
            'effect_category' => [],
            'effects' => [],
        ];

        $controller = new VideoEffectController($dbConnection, $method, $params);
        $videoEffectResult = $controller->processRequest();

        if (array_key_exists('effects', $videoEffectResult) && count($videoEffectResult['effects']) > 0) {
            $response['effects'] = $videoEffectResult['effects'];
        }
        if (array_key_exists('effect_count', $videoEffectResult) && $videoEffectResult['effect_count'] > 0) {
            $response['total_effect'] = $videoEffectResult['effect_count'];
        }
        $catController = new VideoCategoryController($dbConnection, $method, $params);
        $videoEffectResult = $catController->processRequest();
        if (array_key_exists('effect_category', $videoEffectResult) && count($videoEffectResult['effect_category']) > 0) {
            $response['effect_category'] = $videoEffectResult['effect_category'];
        }
        if (array_key_exists('total_category', $videoEffectResult) && $videoEffectResult['total_category'] > 0) {
            $response['total_category'] = $videoEffectResult['total_category'];
        }
        header('HTTP/1.1 200 OK');
        echo json_encode($response);
    } else {
        header("HTTP/1.1 404 Not Found");
        return false;
    }
} else {
    http_response_code(401);
    echo json_encode(['status' => 'FAILED', 'message' => 'Unauthorized']);
    return false;
}
