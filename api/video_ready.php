<?php
require "../bootstrap.php";
require "./Parts/headers.php";

use Src\Controller\VideoReadyCategoryController;
use Src\Controller\VideoReadyController;
use Src\Service\JwtHelper;

$headers = getAuthorizationHeader();
$token = getBearerToken($headers);
$jwt = new JwtHelper($dbConnection);

if ($token && $jwt->verify_token($token)) {
    $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $method = $_SERVER['REQUEST_METHOD'];

    $uri = explode('api/', $uri);
    if ($method == 'GET' && $uri[1] == 'video_ready') {
        $queryParams = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
        parse_str($queryParams, $params);

        $response = [
            'status' => true,
            'total_category' => 0,
            'total_effect' => 0,
            'msg' => 'ready videos',
            'video_category' => [],
            'videos' => [],
        ];

        $controller = new VideoReadyController($dbConnection, $method, $params);
        $videoReadyResult = $controller->processRequest();

        if (array_key_exists('videos', $videoReadyResult) && count($videoReadyResult['videos']) > 0) {
            $response['videos'] = $videoReadyResult['videos'];
        }
        if (array_key_exists('effect_count', $videoReadyResult) && $videoReadyResult['effect_count'] > 0) {
            $response['total_effect'] = $videoReadyResult['effect_count'];
        }
        $catController = new VideoReadyCategoryController($dbConnection, $method, $params);
        $result = $catController->processRequest();
        if (array_key_exists('video_category', $result) && count($result['video_category']) > 0) {
            $response['video_category'] = $result['video_category'];
        }
        if (array_key_exists('total_category', $result) && $result['total_category'] > 0) {
            $response['total_category'] = $result['total_category'];
        }
        header('HTTP/1.1 200 OK');
        echo json_encode($response);
    } else {
        header("HTTP/1.1 404 Not Found");
        return false;
    }
} else {
    http_response_code(401);
    echo json_encode(['status' => 'FAILED', 'message' => 'Unauthorized']);
    return false;
}
