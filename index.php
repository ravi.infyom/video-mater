<?php
list($rootPath, $srcPath) = ['.', './Src'];
require "./bootstrap.php";
require_once './Src/middleware/authenticated.php';
$page = 'Dashboard';
$active = 'dashboard';
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $_ENV['APP_NAME'] ?> | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="<?php echo $rootPath . '/favicon.png' ?>" type="image/png" sizes="16x16">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/fontawesome-free/css/all.min.css' ?>">
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->

    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/dist/css/adminlte.min.css' ?>">

    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/overlayScrollbars/css/OverlayScrollbars.min.css' ?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">


</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <?php
require_once $srcPath . '/components/navbar.php'
?>

        <!-- Main Sidebar Container -->
        <?php
require_once $srcPath . '/components/main_sidebar.php'
?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <h1 class="ml-2 text-dark"><?php echo $page; ?></h1>

                    </div><!-- /.row -->
                </div><!-- /.container-fluid -->
            </div>
            <!-- /.content-header -->

            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">
                    <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-4 col-4">
                            <!-- small box -->
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3 id="video_category_count">--</h3>

                                    <p>Video Categories</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-cubes"></i>
                                </div>
                                <a href="<?php echo $rootPath . '/Views/category' ?>" class="small-box-footer">More info
                                    <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-4 col-4">
                            <!-- small box -->
                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h3 id="video_effect_count">--</h3>

                                    <p>Video Effects</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-video-camera"></i>
                                </div>
                                <a href="<?php echo $rootPath . '/Views/video_effect' ?>" class="small-box-footer">More
                                    info <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <?php 
                        if(filter_var($_ENV['ENABLE_VIDEO_READY'], FILTER_VALIDATE_BOOLEAN)){
                        ?>
                        <div class="col-lg-4 col-4">
                            <!-- small box -->
                            <div class="small-box bg-secondary">
                                <div class="inner">
                                    <h3 id="video_ready_category_count">--</h3>

                                    <p>Video Ready Categories</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-cubes"></i>
                                </div>
                                <a href="<?php echo $rootPath . '/Views/video_ready_category' ?>"
                                    class="small-box-footer">More
                                    info <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>

                      
                        <!-- ./col -->
                        <div class="col-lg-4 col-4">
                            <!-- small box -->
                            <div class="small-box bg-info">
                                <div class="inner">
                                    <h3 id="video_ready_count">--</h3>

                                    <p>Ready Videos</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-video-camera"></i>
                                </div>
                                <a href="<?php echo $rootPath . '/Views/video_ready' ?>" class="small-box-footer">More
                                    info <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <?php }?>
                        <div class="col-lg-4 col-4">
                            <!-- small box -->
                            <div class="small-box bg-success">
                                <div class="inner">
                                    <h3 id="audio_category_count">--</h3>

                                    <p>Audio Categories</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-cubes"></i>
                                </div>
                                <a href="<?php echo $rootPath . 'Views/audio_category.php' ?>"
                                    class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <!-- ./col -->
                        <div class="col-lg-4 col-4">
                            <!-- small box -->
                            <div class="small-box bg-secondary">
                                <div class="inner">
                                    <h3 id="audio_count">--</h3>

                                    <p>Audios</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-music"></i>
                                </div>
                                <a href="<?php echo $rootPath . '/Views/audio' ?>" class="small-box-footer">More info <i
                                        class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>

                        <?php if(filter_var($_ENV['ENABLE_LANGUAGES'], FILTER_VALIDATE_BOOLEAN)){?>

                        <div class="col-lg-4 col-4">
                            <!-- small box -->
                            <div class="small-box bg-secondary">
                                <div class="inner">
                                    <h3 id="language_count">--</h3>

                                    <p>Languages</p>
                                </div>
                                <div class="icon">
                                    <i class="fa fa-language"></i>
                                </div>
                                <a href="<?php echo $rootPath . 'Views/language' ?>" class="small-box-footer">More info
                                    <i class="fas fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                        <?php }?>

                    </div>
                </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php
// require_once('components/footer.php')
?>

        <!-- Control Sidebar -->
        <aside class="control-sidebar control-sidebar-dark">
            <!-- Control sidebar content goes here -->
        </aside>
        <!-- /.control-sidebar -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery -->
    <script src="<?php echo $srcPath . '/plugins/jquery/jquery.min.js' ?>"></script>

    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo $srcPath . '/plugins/jquery-ui/jquery-ui.min.js' ?>"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
        $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="<?php echo $srcPath . '/plugins/bootstrap/js/bootstrap.bundle.min.js' ?>"></script>
    <!-- overlayScrollbars -->
    <script src="<?php echo $srcPath . '/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js' ?>"></script>

    <link rel="stylesheet" href="<?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.css' ?>">
    <script src=" <?php echo $srcPath . '/plugins/sweetalert2/sweetalert2.min.js' ?>"> </script>

    <!-- AdminLTE App -->
    <script src="<?php echo $srcPath . '/dist/js/adminlte.js' ?>"></script>
    <script src="https://kit.fontawesome.com/a044a561b3.js" crossorigin="anonymous"></script>
    <script src="<?php echo $srcPath . '/actions/dashboard.js' ?>"></script>
    <script>

    </script>
</body>

</html>